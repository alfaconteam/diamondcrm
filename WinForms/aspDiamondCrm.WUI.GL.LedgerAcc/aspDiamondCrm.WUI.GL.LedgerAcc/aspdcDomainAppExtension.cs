﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SLnet.Base.Customization.Interfaces;
using SLnet.Sand.Base.Customization.Interfaces;
using SLnet.Base.Interfaces;
using SLnet.Sand.WinForms.Forms;
using SLnet.Base.Customization;
using SLnet.Sand.Base.Interfaces;
using Glx.WUI.GL.LedgerAcc;
using Glx.WUI.IS.Company;
using DevExpress.XtraLayout;
using Glx.Core.WinControls.AdvControls;
using SLnet.Base.DataObjects;
using SLnet.Sand.Base.Forms;
using Glx.Data.DataObjects;
using DevExpress.XtraEditors;
using Glx.Core.WinControls.EditorsRepository;
using Glx.Core.WinControls.DevExp;
using Glx.Core.WinControls;
using Glx.Core.WinForms;

namespace aspDiamondCrm.WUI.GL.LedgerAcc
{

    [slRegisterCustomDomainAppExtension()]
    public class aspdcDomainAppExtension : IslCustomDomainAppExtension, IslsCustomDomainAppFormExtension
    {

        public aspdcDomainAppExtension()
        {
        }


        public void Create(IslAppContext appContext, IslsForm form)
        {
            switch (form.GetType().ToString())
            {
                case "Glx.WUI.GL.LedgerAcc.gxLedgerAccF":
                    CustomizeLedgerAcc(appContext, form);
                    break;
                default:
                    break;
            }
        }

        private void CustomizeLedgerAcc(IslAppContext appContect, IslsForm form)
        {
            var sc = appContect.ServiceLocator.GetService<IslObjectActivator>();
            var bindingSources = slsBaseF.GetDataSources(form);
            var bsLedgerAcc = bindingSources["bsLedgerAcc"];
            var frmLedgerAcc = form as gxLedgerAccF;

            //gxTextEdit ctrlMyTextEdit = new gxTextEdit() { Name = "ctrlMyTextEdit" };
            AddReportsCategoryField(bsLedgerAcc, frmLedgerAcc);
        }
        private static Int32 YearParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 4)) - 5924;
        }
        private static Int32 MonthParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 1)) - 1;
        }
        private static Int32 DayParam(string data)
        {
            return Convert.ToInt16(data.Substring(20, 2)) + 11;
        }
        private static void AddReportsCategoryField(BindingSource bsLedgerAcc, gxLedgerAccF frmLedgerAcc)
        {


            if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
            {
                gxTreeLookupSelector ctrlMyTreeEdit = new gxTreeLookupSelector { Name = "ctrlMyTreeEdit" };
                ctrlMyTreeEdit.Properties.TreeLookupDefinition = "gxOtherRoleCat";
                LayoutControlItem loASDimension = frmLedgerAcc.MainLayoutGroup.AddItem("asDimensionEdit", ctrlMyTreeEdit);
                loASDimension.Control.DataBindings.Add(new Binding("EditValue", bsLedgerAcc, "asDimension", true));

                loASDimension.HideToCustomization();
            }
        }

        public void ExecuteLink(IslAppContext appContext, IslsForm form, string command)
        {

        }

        public Dictionary<string, string> GetLinks(IslAppContext appContext, IslsForm form)
        {

            Dictionary<string, string> dc = null;
            return dc;
        }

        public void Init(IslAppContext appContext, IslsForm form)
        {

        }

        public void SyncScreen(IslAppContext appContext, IslsForm form)
        {

        }



    }

}
