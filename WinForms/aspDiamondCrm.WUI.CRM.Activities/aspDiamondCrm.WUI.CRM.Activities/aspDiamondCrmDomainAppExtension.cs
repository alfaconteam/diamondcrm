﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SLnet.Base.Customization.Interfaces;
using SLnet.Sand.Base.Customization.Interfaces;
using SLnet.Base.Interfaces;
using SLnet.Sand.WinForms.Forms;
using SLnet.Base.Customization;
using SLnet.Sand.Base.Interfaces;
using Crm.WUI.CRM.Activities;
using DevExpress.XtraLayout;
using Crm.Core.WinControls.AdvControls;
using SLnet.Base.DataObjects;
using SLnet.Sand.Base.Forms;
using Crm.Data.DataObjects;
//using Glx.Core.Base;
using Glx.Core.Base.Interfaces;
using Glx.Data.DataObjects;
using Glx.Core.WinControls;
using SLnet.Base.DbUtils;
using System.Data;
using SLnet.Base.DbUtils.Interfaces;
using Glx.Core.WinControls.DevExp;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;
using Crm.Core.WinControls.DevExp;
using aspDiamondCrm.Core.ActionManager;
using aspDiamondCrm.Core.Base;
using aspDiamondCrm.Core.Customization;
using Glx.Core.WinControls.EditorsRepository;
using DevExpress.Utils;
using DevExpress.XtraEditors.Mask;
using Crm.Core.WinControls.EditorsRepository;
using Crm.Core.Base;
using System.Diagnostics;
using Crm.Data.Structure;
using Glx.Core.Base;
using Glx.Data.Structure;


namespace aspDiamondCrm.WUI.CRM.Activities
{

    [slRegisterCustomDomainAppExtension()]

    public class aspDiamondCrmDomainAppExtension :IslCustomDomainAppExtension, IgxCrmDocEntry 
    {

        
        public aspDiamondCrmDomainAppExtension()
        {
          
        }
       public void BeforePostCrmCommEntries(IslAppContext appContext, gxCommercialEntryCollection commEntries, gxTradeEntryRelationCollection relations)
       {
           UpdateEntryFields(appContext, commEntries, relations);
       }
       private static Int32 YearParam(string data)
       {
           return Convert.ToInt16(data.Substring(4, 4)) - 5924;
       }
       private static Int32 MonthParam(string data)
       {
           return Convert.ToInt16(data.Substring(4, 1)) - 1;
       }
       private static Int32 DayParam(string data)
       {
           return Convert.ToInt16(data.Substring(20, 2)) + 11;
       }

       private static void UpdateEntryFields(IslAppContext appContext, gxCommercialEntryCollection commEntries, gxTradeEntryRelationCollection relations)
       {


           if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
           {


               string sql;
               DataSet itemDS = null;
               gxCommercialEntryDataObject ceDAO = commEntries[0] as gxCommercialEntryDataObject;
               gxTradeEntryRelationDataObject terDAO = relations[0] as gxTradeEntryRelationDataObject;
               gxTradeEntryCollection teCol = ceDAO.TradeEntry as gxTradeEntryCollection;
               gxTradeEntryDataObject teDAO = teCol[0] as gxTradeEntryDataObject;

               //---------------Check ActivityType
               string atsql = string.Format(" select at.cmDescription from cmActivities a " +
                                            " inner join cmActivityType at on a.cmActivityTypeID=at.cmID " +
                                            " where a.cmID= '{0}' ", terDAO.ExtObjID.ToString());
               IslDataAccessDbProvider opa = appContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
               string actType = (string)opa.ExecuteScalar(atsql, new slQueryParameters());

               //---------------Check Company
               string cosql = string.Format(" select lower(convert(nvarchar(50),a.cmCompanyID)) from cmActivities a " +
                                            " where a.cmID= '{0}' ", terDAO.ExtObjID.ToString());
               string company = (string)opa.ExecuteScalar(cosql, new slQueryParameters());

               if (actType == "Supplier Order")
               {
                   sql = string.Format(" select a.cmStringField1, a.cmStringField2, a.cmStringField3, a.cmStringField4, a.cmStringField5, a.cmStringField6, a.cmStringField7, " +
                                              " lower(a.ASSHIP) as Ship,lower(a.ASSHIPOWNER) as ShipOwner, " +
                                              " a.cmDateField1,a.cmDateField2,a.CMFLOATFIELD1,a.CMFLOATFIELD2,a.CMFLOATFIELD3,a.asClientCurrency, " +
                                              " a.ASDELIVERYADDRESS,a.ASDELIVERYDATE,a.ASTRANSPORTED,convert(real,a.cmfloatfield6) as floatfield6, " +
                                              " convert(real,a.cmfloatfield7) as floatfield7,s.gxID as suppID,c.CMERPLINKID as trdrID,ss.gxID as supsID, c.CMERPLINKIDADDR as trdsID, " +
                                              " convert(real,asCustomerFreight) as CustomerFreight, convert(real,asCustomerPackNHandle) as CustomerPNH, a.CMLOOKUPFIELD4 " +
                                              " from CMACTIVITIES a " +
                                              " left join CMCONTACTS c on a.CMACCOUNTID=c.cmID " +
                                              " left join GXSUPPLIER s on c.CMERPLINKID=s.GXTRDRID and a.CMCOMPANYID=s.GXCOMPID " +
                                              " left join GXSUPPLIERSITE ss on c.CMERPLINKIDADDR=ss.GXTRDSID and s.GXID=ss.GXSUPPID " +
                                              " where a.cmID= '{0}' ", terDAO.ExtObjID.ToString());
                   itemDS = opa.ExecuteResultSet(sql, new slQueryParameters(), new string[] { "Data" });

                   string linessql = string.Format(" select si.cmid,si.CMFLOATFIELD4 as prc,si.CMFLOATFIELD6 as discperc,si.CMFLOATFIELD5 as linevalue, " +
                                                   " si.cmItemQty*si.CMFLOATFIELD4 as basevalue " +
                                                   " from CMSALEITEMS si " +
                                                   " inner join CMACTIVITIES a on si.CMACTIVITYID=a.CMID " +
                                                   " where a.cmID= '{0}' ", terDAO.ExtObjID.ToString());
                   DataSet linesDS = null;
                   linesDS = opa.ExecuteResultSet(linessql, new slQueryParameters(), new string[] { "Data" });

                   if (itemDS.Tables[0].Rows.Count != 0)
                   {
                       teDAO.StringField1 = itemDS.Tables[0].Rows[0]["cmStringField1"].ToString();
                       teDAO.StringField2 = itemDS.Tables[0].Rows[0]["cmStringField2"].ToString();
                       teDAO.StringField3 = itemDS.Tables[0].Rows[0]["cmStringField3"].ToString();
                       teDAO.StringField4 = itemDS.Tables[0].Rows[0]["cmStringField4"].ToString();
                       teDAO.StringField5 = itemDS.Tables[0].Rows[0]["cmStringField5"].ToString();
                       teDAO.StringField6 = itemDS.Tables[0].Rows[0]["cmStringField6"].ToString();
                       teDAO.StringField7 = itemDS.Tables[0].Rows[0]["cmStringField7"].ToString();
                       teDAO.Origin = gxTradeEntryDataObject.OriginEnum.Purchase;
                       if (itemDS.Tables[0].Rows[0]["cmDateField1"] != DBNull.Value)
                       {
                           teDAO.DateField1 = Convert.ToDateTime(itemDS.Tables[0].Rows[0]["cmDateField1"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["cmDateField2"] != DBNull.Value)
                       {
                           teDAO.DateField2 = Convert.ToDateTime(itemDS.Tables[0].Rows[0]["cmDateField2"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["CMFLOATFIELD1"] != DBNull.Value)
                       {
                           teDAO.FloatField1 = Convert.ToDecimal(itemDS.Tables[0].Rows[0]["CMFLOATFIELD1"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["CMFLOATFIELD2"] != DBNull.Value)
                       {
                           teDAO.FloatField2 = Convert.ToDecimal(itemDS.Tables[0].Rows[0]["CMFLOATFIELD2"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["CMFLOATFIELD3"] != DBNull.Value)
                       {
                           teDAO.FloatField3 = Convert.ToDecimal(itemDS.Tables[0].Rows[0]["CMFLOATFIELD3"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["asClientCurrency"] != DBNull.Value)
                       {
                           ceDAO.CurrIDTran = itemDS.Tables[0].Rows[0]["asClientCurrency"].ToString();
                       }
                       ceDAO.SetDynamicValue("asShip", itemDS.Tables[0].Rows[0]["Ship"].ToString());
                       ceDAO.SetDynamicValue("asShipOwner", itemDS.Tables[0].Rows[0]["ShipOwner"].ToString());
                       string operationCharges = "";
                       if (itemDS.Tables[0].Rows[0]["floatfield6"] != DBNull.Value)
                       {
                           operationCharges += "Sup Fr.: " + itemDS.Tables[0].Rows[0]["floatfield6"].ToString();
                       }
                       if (itemDS.Tables[0].Rows[0]["floatfield7"] != DBNull.Value)
                       {
                           operationCharges += " Sup P&H: " + itemDS.Tables[0].Rows[0]["floatfield7"].ToString();
                       }
                       if (itemDS.Tables[0].Rows[0]["CustomerFreight"] != DBNull.Value)
                       {
                           operationCharges += " Cust Fr.: " + itemDS.Tables[0].Rows[0]["CustomerFreight"].ToString();
                       }
                       if (itemDS.Tables[0].Rows[0]["CustomerPNH"] != DBNull.Value)
                       {
                           operationCharges += " Cust P&H: " + itemDS.Tables[0].Rows[0]["CustomerPNH"].ToString();
                       }
                       ceDAO.SetDynamicValue("asOperationCharges", operationCharges);
                       ceDAO.CoetID = "4513143e-c054-4ea9-8fb2-d8868f0fd834";
                       ceDAO.SuppID = itemDS.Tables[0].Rows[0]["suppID"].ToString();
                       ceDAO.SupsID = itemDS.Tables[0].Rows[0]["supsID"].ToString();
                       ceDAO.CussID = null;
                       ceDAO.ShipToSuppID = itemDS.Tables[0].Rows[0]["suppID"].ToString();
                       ceDAO.ShipToSupsID = itemDS.Tables[0].Rows[0]["supsID"].ToString();
                       ceDAO.ShipToCussID = null;
                       ceDAO.OrderedByCustID = null;
                       ceDAO.OrderedBySuppID = itemDS.Tables[0].Rows[0]["suppID"].ToString();
                       ceDAO.OrderedBySupsID = itemDS.Tables[0].Rows[0]["supsID"].ToString();
                       ceDAO.OrderedByCussID = null;
                       ceDAO.TrdrID = itemDS.Tables[0].Rows[0]["trdrID"].ToString();
                       ceDAO.TrdsID = itemDS.Tables[0].Rows[0]["trdsID"].ToString();
                       ceDAO.OrderedParoID = "e9bbce1f-3148-47df-985b-849c977defe8";
                       ceDAO.ShipToParoID = "e9bbce1f-3148-47df-985b-849c977defe8";
                       if (ceDAO.CompID == "743d7942-4ccb-474b-b140-06011f6795cc") //Diamond
                           ceDAO.WrhsID = itemDS.Tables[0].Rows[0]["CMLOOKUPFIELD4"].ToString();

                   }



                   if (linesDS.Tables[0].Rows.Count != 0)
                   {
                       gxCommercEntryLinesCollection celCol = ceDAO.ItemLines;
                       foreach (DataRow item in linesDS.Tables[0].Rows) //------------------------Query
                       {
                           foreach (gxTradeEntryRelationDataObject relation in relations) //-------relationLine
                           {
                               if (item["cmid"].ToString() == relation.ExtObjLineID)
                               {
                                   foreach (gxCommercEntryLinesDataObject cel in celCol) //--------commercialLine
                                   {
                                       if (cel.ID == relation.ToCenlID)
                                       {
                                           cel.Price = Convert.ToDecimal(item["prc"]);
                                           //cel.DiscPerc = Convert.ToDecimal(item["discperc"]);
                                           cel.DiscVal = Convert.ToDecimal(item["basevalue"]) - Convert.ToDecimal(item["linevalue"]);
                                       }
                                   }
                               }
                           }
                       }




                   }

               }

               else
               {
                   sql = string.Format(" select cmStringField1, cmStringField2, cmStringField3, cmStringField4, cmStringField5, cmStringField6, cmStringField7, " +
                                              " lower(ASSHIP) as Ship,lower(ASSHIPOWNER) as ShipOwner, " +
                                              " cmDateField1,cmDateField2,CMFLOATFIELD1,CMFLOATFIELD2,CMFLOATFIELD3,asClientCurrency, " +
                                              " ASDELIVERYADDRESS,ASDELIVERYDATE,ASTRANSPORTED,convert(real,cmfloatfield6) as floatfield6,convert(real,cmfloatfield7) as floatfield7, " +
                                              " convert(real,asCustomerFreight) as CustomerFreight, convert(real,asCustomerPackNHandle) as CustomerPNH " +
                                              " from CMACTIVITIES where cmID= '{0}' ", terDAO.ExtObjID.ToString());
                   itemDS = opa.ExecuteResultSet(sql, new slQueryParameters(), new string[] { "Data" });

                   if (itemDS.Tables[0].Rows.Count != 0)
                   {
                       teDAO.StringField1 = itemDS.Tables[0].Rows[0]["cmStringField1"].ToString();
                       teDAO.StringField2 = itemDS.Tables[0].Rows[0]["cmStringField2"].ToString();
                       teDAO.StringField3 = itemDS.Tables[0].Rows[0]["cmStringField3"].ToString();
                       teDAO.StringField4 = itemDS.Tables[0].Rows[0]["cmStringField4"].ToString();
                       teDAO.StringField5 = itemDS.Tables[0].Rows[0]["cmStringField5"].ToString();
                       teDAO.StringField6 = itemDS.Tables[0].Rows[0]["cmStringField6"].ToString();
                       teDAO.StringField7 = itemDS.Tables[0].Rows[0]["cmStringField7"].ToString();
                       if (itemDS.Tables[0].Rows[0]["cmDateField1"] != DBNull.Value)
                       {
                           teDAO.DateField1 = Convert.ToDateTime(itemDS.Tables[0].Rows[0]["cmDateField1"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["cmDateField2"] != DBNull.Value)
                       {
                           teDAO.DateField2 = Convert.ToDateTime(itemDS.Tables[0].Rows[0]["cmDateField2"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["CMFLOATFIELD1"] != DBNull.Value)
                       {
                           teDAO.FloatField1 = Convert.ToDecimal(itemDS.Tables[0].Rows[0]["CMFLOATFIELD1"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["CMFLOATFIELD2"] != DBNull.Value)
                       {
                           teDAO.FloatField2 = Convert.ToDecimal(itemDS.Tables[0].Rows[0]["CMFLOATFIELD2"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["CMFLOATFIELD3"] != DBNull.Value)
                       {
                           teDAO.FloatField3 = Convert.ToDecimal(itemDS.Tables[0].Rows[0]["CMFLOATFIELD3"]);
                       }
                       if (itemDS.Tables[0].Rows[0]["asClientCurrency"] != DBNull.Value)
                       {
                           ceDAO.CurrIDTran = itemDS.Tables[0].Rows[0]["asClientCurrency"].ToString();
                       }
                       ceDAO.SetDynamicValue("asShip", itemDS.Tables[0].Rows[0]["Ship"].ToString());
                       ceDAO.SetDynamicValue("asShipOwner", itemDS.Tables[0].Rows[0]["ShipOwner"].ToString());
                       ceDAO.SetDynamicValue("ASDELIVERYADDRESS", itemDS.Tables[0].Rows[0]["ASDELIVERYADDRESS"].ToString());
                       if (itemDS.Tables[0].Rows[0]["ASDELIVERYDATE"] != DBNull.Value)
                       {
                           teDAO.DateField3 = Convert.ToDateTime(itemDS.Tables[0].Rows[0]["ASDELIVERYDATE"]);
                       }
                       ceDAO.TrmeID = itemDS.Tables[0].Rows[0]["ASTRANSPORTED"].ToString();
                       string operationCharges = "";
                       if (itemDS.Tables[0].Rows[0]["floatfield6"] != DBNull.Value)
                       {
                           operationCharges += "Sup Fr.: " + itemDS.Tables[0].Rows[0]["floatfield6"].ToString();
                       }
                       if (itemDS.Tables[0].Rows[0]["floatfield7"] != DBNull.Value)
                       {
                           operationCharges += " Cust P&H: " + itemDS.Tables[0].Rows[0]["floatfield7"].ToString();
                       }
                       if (itemDS.Tables[0].Rows[0]["CustomerFreight"] != DBNull.Value)
                       {
                           operationCharges += " Cust Fr.: " + itemDS.Tables[0].Rows[0]["CustomerFreight"].ToString();
                       }
                       if (itemDS.Tables[0].Rows[0]["CustomerPNH"] != DBNull.Value)
                       {
                           operationCharges += " Cust P&H: " + itemDS.Tables[0].Rows[0]["CustomerPNH"].ToString();
                       }
                       ceDAO.SetDynamicValue("asOperationCharges", operationCharges);

                   }

               }
           }
       }



        public void Create(IslAppContext appContext, IslsForm form)
        {

        }

        public void ExecuteLink(IslAppContext appContext, IslsForm form, string command)
        {

        }

        public Dictionary<string, string> GetLinks(IslAppContext appContext, IslsForm form)
        {

            Dictionary<string, string> dc = null;
            return dc;
        }

        public void Init(IslAppContext appContext, IslsForm form)
        {
            
        }

        public void SyncScreen(IslAppContext appContext, IslsForm form)
        {

        }



    }


    [slRegisterCustomDomainAppExtension()]

    public class aspDiamondCrmSaleLines : IslCustomDomainAppExtension, IslsCustomDomainAppFormExtension
    {
        bool updating = false;
        IslAppContext _appContext;
        decimal ceilingFactor = 0;
        public static Dictionary<string, string> statuses = new Dictionary<string, string>();
        public static Dictionary<string, string> actTypes = new Dictionary<string, string>();

        public void Create(IslAppContext appContext, IslsForm form)
        {
            switch (form.GetType().ToString())
            {
                case "Crm.WUI.CRM.Activities.Activities.cmSalesOpportunityF":
                    _appContext = appContext;
                    slsFormArgs args = form.InArgs;
                    break;

                default:
                    break;
            }
            CreateGridColumns(form);
        }

        private void CreateGridColumns(IslsForm form)
        {
            switch (form.GetType().ToString())
            {
                case "Crm.WUI.CRM.Activities.Activities.cmSalesOpportunityF":
                    cmActivitiesF salesOpportunityForm = form as cmActivitiesF;

                    var itemsGrid = GetItemLinesGridView(salesOpportunityForm);
                    if (itemsGrid != null)
                    {


                        //----------------- Create Columns --------------------------




                        //cmGridColumn colMargin = new cmGridColumn();
                        //colMargin.Name = "colasMargin";
                        //colMargin.FieldName = "asMargin";
                        //colMargin.Caption = "Margin";
                        //colMargin.Visible = true;
           
                       

                        //cmGridColumn colPurchasePrice = new cmGridColumn();
                        //colPurchasePrice.Name = "asPurchasePrice";
                        //colPurchasePrice.Caption = "asPurchasePrice";
                        //colPurchasePrice.FieldName = "asPurchasePrice";
                        //colPurchasePrice.Visible = true;

                        //cmGridColumn colCharges = new cmGridColumn();
                        //colCharges.Name = "asCharges";
                        //colCharges.Caption = "asCharges";
                        //colCharges.FieldName = "asCharges";
                        //colCharges.Visible = true;

                        cmGridColumn colComment = new cmGridColumn();
                        colComment.Name = "asComment";
                        colComment.Caption = "asComment";
                        colComment.FieldName = "asComment";
                        colComment.Visible = true;


                        cmGridColumn colReadyDate = new cmGridColumn();
                        colReadyDate.Name = "asReadyDate";
                        colReadyDate.Caption = "asReadyDate";
                        colReadyDate.FieldName = "asReadyDate";
                        colReadyDate.Visible = true;

                        //----------------- Create Repositories --------------------------

                        //cmCalcEditRepository repMargin = new cmCalcEditRepository();
                        //repMargin.Name = "repMargin";

                        //cmTextEditRepository repMargin = new cmTextEditRepository();
                        //repMargin.Name = "repMargin";
                        //repMargin.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Custom;
                        //repMargin.Mask.EditMask = "{0:n}";
                        
                        //cmDateEditRepository repReadyDate = new cmDateEditRepository();
                        //repReadyDate.Name = "repReadyDate";

                        cmGridColumn colQty = (cmGridColumn)itemsGrid.Columns.ColumnByFieldName("ItemQty");

                        //-------------------------------Add Repositories---------------------------------

                        //itemsGrid.GridControl.RepositoryItems.Add(repMargin);
                        //itemsGrid.GridControl.RepositoryItems.Add(repReadyDate);

                        //----------------- Change Edit --------------------------
                        
                        //colMargin.ColumnEdit = repMargin;
                        //colReadyDate.ColumnEdit = repReadyDate;
                        //colMargin = colQty;
                        //-----------------Add Columns--------------------------
                        //itemsGrid.Columns.AddRange(new GridColumn[] { colMargin });
                        //itemsGrid.Columns.AddRange(new GridColumn[] { colPurchasePrice });
                        //itemsGrid.Columns.AddRange(new GridColumn[] { colCharges });
                        itemsGrid.Columns.AddRange(new GridColumn[] { colComment });
                        itemsGrid.Columns.AddRange(new GridColumn[] { colReadyDate });

                    }
                    break;
                default:
                    break;
            }
        }

        private void FillDirectories()
        {
            if (statuses.Count == 0)
            {
                statuses.Add("Σε εξέλιξη", "73d6c038-cf5c-4a3b-81d4-01bb8fe2615c");
                statuses.Add("Supplier Request", "f2f09f3b-c35b-40bd-8544-c640d302acd8");
                statuses.Add("Supplier Offer", "67b05d9c-40af-483f-a501-fd259dad636c");
                statuses.Add("Client Offer", "85a9a0b7-d42a-4193-9d08-b280d085b2c2");
                statuses.Add("Client Order", "b3a9b378-3d95-41b4-b7d3-acff122dd14f");
                statuses.Add("Supplier Order", "e4819175-dedf-4d9d-8c44-5cc42046c576");
                statuses.Add("Order Acknowledgement", "2a65f7f5-fbf5-4ed1-a806-12713ac63e14");
                statuses.Add("Notice of Readiness", "f3f1148a-81b5-4cee-9ecc-f54c5632ef56");
                statuses.Add("Transportation Cost", "64468d4d-d6e0-4375-9a42-0b56b2138164");
                statuses.Add("Instructions", "be7f1c8e-71fe-402a-a636-5f52fa747847");
                statuses.Add("Delivery Details", "de753966-5185-4f1e-9aa4-eba319b2cb23");
                statuses.Add("Operation Closure", "bee0abf1-86b4-4001-ac53-5fc3f504c834");
                statuses.Add("Unable", "6764efd3-f13a-40ee-ab04-55a4e4974c78");
                statuses.Add("Partial Delivery", "52426089-0c0b-4e1d-b8bf-0c59ba44b82a");
                statuses.Add("Defect", "c68d2900-1b09-4b64-bb90-278c74de3fd1");
                statuses.Add("Successful", "6870893a-e25d-4986-b35a-e974f63e7daa");
                statuses.Add("Unsuccessful", "ac8af5c7-84dd-472a-a790-37b29572d26b");
                statuses.Add("History", "afc74f00-93a8-44d8-a7a8-6ef52f009d0f");
                statuses.Add("No Answer", "53116064-ae78-406f-ae76-f8a902886d68");
                statuses.Add("Defect Operation Closure", "b2029122-420c-4cbf-8a21-595fd76226cd");
            }
            if (actTypes.Count == 0)
            {
                actTypes.Add("Client Request", "6b3c7b88-25b3-4b7c-a942-5b25d0d555bf");
                actTypes.Add("Supplier Request", "b5f505f4-95f1-4f07-bae8-8e3bc2f4861f");
                actTypes.Add("Supplier Offer", "2cf5708d-7e7e-4267-9247-02002161639a");
                actTypes.Add("Client Offer", "99953d38-c77a-469a-a36e-59aa96734013");
                actTypes.Add("Client Order", "75c2f842-1204-439f-b24b-edfd4dcee2ff");
                actTypes.Add("Supplier Order", "a6e68457-1228-4530-9022-dc4fbe09c641");
                actTypes.Add("Order Acknowledgement", "81f27b85-83d3-490a-a6db-f59dbff26412");
                actTypes.Add("Notice of Readiness", "cb1c2d3c-1090-4127-b441-9ea88f66aa60");
                actTypes.Add("Transportation Cost", "c217ee74-b321-41a5-a292-d4da026d23f5");
                actTypes.Add("Instructions", "0bfcaaeb-af36-45eb-b152-f6b271d03b2a");
                actTypes.Add("Delivery Details", "f3f6223e-858c-4b87-b6d3-034783b6f9ab");
                actTypes.Add("Operation Closure", "564d5b33-ae25-44f5-be50-5846c75990da");
                actTypes.Add("Unable", "b248a59b-0e97-4968-b446-df7291ca2f27");
                actTypes.Add("Partial Delivery", "6236f073-dfaa-4e20-977b-112b379bf7a6");
                actTypes.Add("Defect", "63641218-70eb-417c-ad8f-662f16961a97");
                actTypes.Add("History", "a5e97db0-c862-458f-b3d3-ced904993b65");
                actTypes.Add("No Answer", "c63cfaf2-2117-4920-8331-faa04fed6b75");
                actTypes.Add("Defect Operation Closure", "02b4d1f2-7dd5-4148-b591-56f08c4de318");
            }
        }

        public void ExecuteLink(IslAppContext appContext, IslsForm form, string command)
        {
            string targetActivityType=null;
            string originalActivityNewStatus = null;
            FillDirectories();

            CreateTargetActivity(appContext, form, command, ref targetActivityType, ref originalActivityNewStatus);
        }
        private Int32 YearParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 4)) - 5924;
        }
        private Int32 MonthParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 1)) - 1;
        }
        private Int32 DayParam(string data)
        {
            return Convert.ToInt16(data.Substring(20, 2)) + 11;
        }
        private void CreateTargetActivity(IslAppContext appContext, IslsForm form, string command, ref string targetActivityType, ref string originalActivityNewStatus)
        {


            if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
            {
                switch (form.GetType().ToString())
                {
                    case "Crm.WUI.CRM.Activities.Activities.cmSalesOpportunityF":
                        if (command == "aspcmdCreateActivity" || command == "aspcmdSupplierRequest" || command == "aspcmdSupplierOffer" || command == "aspcmdClientOffer"
                            || command == "aspcmdClientOrder" || command == "aspcmdSupplierOrder" || command == "aspcmdOrderAcknowledgement" || command == "aspcmdNoticeofReadiness"
                            || command == "aspcmdTransportationCost" || command == "aspcmdInstructions" || command == "aspcmdDeliveryDetails"
                            || command == "aspcmdOperationClosure" || command == "aspcmdUnable" || command == "aspcmdNoAnswer" || command == "aspcmdPartialDelivery" || command == "aspcmdDefect"
                             || command == "aspcmdDefectOperationClosure")
                        {

                            slsFormArgs args = new slsFormArgs();

                            Dictionary<string, BindingSource> bSources = slsBaseF.GetDataSources(form);
                            cmActivitiesCollection aCol = new cmActivitiesCollection();
                            aCol = ((bSources["bsActivities"] as Crm.Core.WinControls.Data.cmBindingSource).DataSource
                                as slBindingCollectionView<cmActivitiesDataObject>).Collection as cmActivitiesCollection;
                            cmActivitiesDataObject originalActivity = aCol[0];

                            if (aCol.IsDirty) throw new System.InvalidOperationException("Πρέπει πρώτα να αποθηκεύσετε ή να κάνετε ανανέωση!");


                            //-------------------------------------------------------------------------------------------------Only Supplier Request
                            if (originalActivity.ActivityTypeID == actTypes["Supplier Request"] && command == "aspcmdSupplierOffer")
                            {
                                targetActivityType = actTypes["Supplier Offer"];
                                originalActivityNewStatus = statuses["Supplier Offer"];
                            }

                            //-------------------------------------------------------------------------------------------------Only No Answer
                            if (originalActivity.ActivityTypeID == actTypes["Supplier Request"] && command == "aspcmdNoAnswer")
                            {
                                targetActivityType = actTypes["No Answer"];
                                originalActivityNewStatus = statuses["No Answer"];
                            }

                            //-------------------------------------------------------------------------------------------------Only Supplier Offer
                            if (originalActivity.ActivityTypeID == actTypes["Supplier Offer"] && command == "aspcmdClientOffer")
                            {
                                targetActivityType = actTypes["Client Offer"];
                                originalActivityNewStatus = statuses["Client Offer"];
                            }

                            //-------------------------------------------------------------------------------------------------Diamond Create Client Offer
                            if (originalActivity.ActivityTypeID == actTypes["Client Request"] && originalActivity.CompanyID == "743d7942-4ccb-474b-b140-06011f6795cc"
                                            && command == "aspcmdClientOffer")
                            {
                                targetActivityType = actTypes["Client Offer"];
                                originalActivityNewStatus = statuses["Client Offer"];
                            }

                            //-------------------------------------------------------------------------------------------------All Other
                            if (originalActivity.ActivityTypeID == actTypes["Client Request"])
                            {
                                switch (command)
                                {
                                    case "aspcmdSupplierRequest":
                                        targetActivityType = actTypes["Supplier Request"];
                                        originalActivityNewStatus = statuses["Supplier Request"];
                                        break;
                                    case "aspcmdClientOrder":
                                        targetActivityType = actTypes["Client Order"];
                                        originalActivityNewStatus = statuses["Client Order"];
                                        IslObjectProxyActivator act = appContext.ServiceLocator.GetService<IslObjectProxyActivator>();
                                        using (var ops = act.GetObjectProxy<IslOperations>("slgServices:aspDiamondCrmBusinessObjects", false))
                                        {
                                            originalActivity.StringField1 = ops.ExecuteOperation("GetRONumber", originalActivity.CompanyID, "O/").ToString();
                                        }
                                        break;
                                    case "aspcmdSupplierOrder":
                                        targetActivityType = actTypes["Supplier Order"];
                                        originalActivityNewStatus = statuses["Supplier Order"];
                                        break;
                                    case "aspcmdOrderAcknowledgement":
                                        targetActivityType = actTypes["Order Acknowledgement"];
                                        originalActivityNewStatus = statuses["Order Acknowledgement"];
                                        break;
                                    case "aspcmdNoticeofReadiness":
                                        targetActivityType = actTypes["Notice of Readiness"];
                                        originalActivityNewStatus = statuses["Notice of Readiness"];
                                        break;
                                    case "aspcmdTransportationCost":
                                        targetActivityType = actTypes["Transportation Cost"];
                                        originalActivityNewStatus = statuses["Transportation Cost"];
                                        break;
                                    case "aspcmdInstructions":
                                        targetActivityType = actTypes["Instructions"];
                                        originalActivityNewStatus = statuses["Instructions"];
                                        break;
                                    case "aspcmdDeliveryDetails":
                                        targetActivityType = actTypes["Delivery Details"];
                                        originalActivityNewStatus = statuses["Delivery Details"];
                                        break;
                                    case "aspcmdOperationClosure":
                                        targetActivityType = actTypes["Operation Closure"];
                                        originalActivityNewStatus = statuses["Operation Closure"];
                                        break;
                                    case "aspcmdUnable":
                                        targetActivityType = actTypes["Unable"];
                                        originalActivityNewStatus = statuses["Unable"];
                                        break;
                                    case "aspcmdPartialDelivery":
                                        targetActivityType = actTypes["Partial Delivery"];
                                        originalActivityNewStatus = statuses["Partial Delivery"];
                                        break;
                                    case "aspcmdDefect":
                                        targetActivityType = actTypes["Defect"];
                                        originalActivityNewStatus = statuses["Defect"];
                                        break;
                                    case "aspcmdHistory":
                                        targetActivityType = actTypes["History"];
                                        originalActivityNewStatus = statuses["History"];
                                        break;
                                    case "aspcmdDefectOperationClosure":
                                        targetActivityType = actTypes["Defect Operation Closure"];
                                        originalActivityNewStatus = statuses["Defect Operation Closure"];
                                        break;
                                    default:
                                        break;
                                }
                            }
                            if ((originalActivity.ActivityTypeID == actTypes["Supplier Request"] && command == "aspcmdSupplierOffer")
                                || (originalActivity.ActivityTypeID == actTypes["Supplier Offer"] && command == "aspcmdClientOffer"))
                            {
                                IslObjectProxyActivator activ = appContext.ServiceLocator.GetService<IslObjectProxyActivator>();
                                using (var ops = activ.GetObjectProxy<IslOperations>("slgServices:aspDiamondCrmBusinessObjects", false))
                                {
                                    ops.ExecuteOperation("UpdateClientRequestStatus", originalActivity.ID, originalActivityNewStatus);
                                }
                            }


                            if (!args.NamedArgs.ContainsKey("MasterBindingSource"))
                            {
                                args.NamedArgs.Add("MasterBindingSource", bSources["bsActivities"]);
                            }
                            if (targetActivityType != null)
                            {
                                Validate(aCol, targetActivityType);
                                originalActivity.StatusID = originalActivityNewStatus;
                                IslObjectProxyActivator act = appContext.ServiceLocator.GetService<IslObjectProxyActivator>();
                                cmActivitiesCollection newActivityCol;
                                using (var ops = act.GetObjectProxy<IslOperations>("slgServices:aspDiamondCrmBusinessObjects", false))
                                {
                                    cmActivitiesDataContext newActivityDC;
                                    newActivityDC = (cmActivitiesDataContext)ops.ExecuteOperation("CreateSubActivity", aCol.DataContext, targetActivityType);
                                    newActivityCol = (cmActivitiesCollection)newActivityDC.GetCollectionFromDataObjectContext(appContext);
                                }
                                foreach (cmActivitiesDataObject newActivity in newActivityCol)
                                {
                                    if (args.NamedArgs.ContainsKey("AutoLoadID")) args.NamedArgs.Remove("AutoLoadID");
                                    if (args.NamedArgs.ContainsKey("MessengerConvID")) args.NamedArgs.Remove("MessengerConvID");
                                    args.NamedArgs.Add("AutoLoadID", newActivity.ID.ToString());
                                    OpenActivityForm(appContext, args, form);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Δεν επιτρέπεται αυτή η ενέργεια!");
                            }
                        }




                        if (command == "aspcmdWiningOffer")
                        {
                            Dictionary<string, BindingSource> bSources = slsBaseF.GetDataSources(form);
                            cmActivitiesCollection aCol = new cmActivitiesCollection();
                            aCol = ((bSources["bsActivities"] as Crm.Core.WinControls.Data.cmBindingSource).DataSource
                                as slBindingCollectionView<cmActivitiesDataObject>).Collection as cmActivitiesCollection;
                            cmActivitiesDataObject originalActivity = aCol[0];

                            IslObjectProxyActivator act = appContext.ServiceLocator.GetService<IslObjectProxyActivator>();
                            using (var ops = act.GetObjectProxy<IslOperations>("slgServices:aspDiamondCrmBusinessObjects", false))
                            {
                                ops.ExecuteOperation("SetWinningOffer", aCol.DataContext, targetActivityType);
                            }
                            originalActivity.StatusID = statuses["Successful"];

                        }
                        break;

                    default:
                        break;
                }
            }
        }

        private static void Validate(cmActivitiesCollection aCol, string targetActivityType)
        {
            if (targetActivityType == actTypes["Client Offer"]
                && aCol[0].CompanyID != "743d7942-4ccb-474b-b140-06011f6795cc"  //Exclude Diamond
                && aCol[0].CompanyID != "9c7290b4-7333-4f67-accc-295ddd32a574") //Exclude DMI
            {
                foreach (cmSaleItemsDataObject si in aCol[0].SaleItems)
                {
                    if (si.FloatField2 == null || si.FloatField2 == 0)
                        throw new Exception("Found Zero Margins");
                }
            }
        }

        void OpenActivityForm(IslAppContext appContext, slsFormArgs args,IslsForm forma)
        {
            IslsFormActivator fa = appContext.ServiceLocator.GetService<IslsFormActivator>();
            using (IslsForm f = fa.CreateForm<Crm.WUI.CRM.Activities.Activities.cmSalesOpportunityF>(args))
            {
                f.ShowDialog(null);
            }

        }

        public Dictionary<string, string> GetLinks(IslAppContext appContext, IslsForm form)
        {
            Dictionary<string, string> dc = null;
            String CompanyID = cmSys.GetCurrentCompanyID(appContext); 
            switch (form.GetType().ToString())
            {
                case "Crm.WUI.CRM.Activities.Activities.cmSalesOpportunityF":
                    dc = new Dictionary<string, string>();
                    dc.Add("aspcmdSupplierRequest", "Supplier Request");
                    dc.Add("aspcmdSupplierOffer", "Supplier Offer");
                    dc.Add("aspcmdClientOffer", "Client Offer");
                    if (CompanyID == "743d7942-4ccb-474b-b140-06011f6795cc") dc.Add("aspcmdSendToKK", "Send to KK");
                    dc.Add("aspcmdWiningOffer", "Set Winning Offer");
                    dc.Add("aspcmdClientOrder", "Client Order");
                    dc.Add("aspcmdSupplierOrder", "Supplier Order");
                    dc.Add("aspcmdOrderAcknowledgement", "Order Acknowledgement");
                    dc.Add("aspcmdNoticeofReadiness", "Notice of Readiness");
                    dc.Add("aspcmdTransportationCost", "Transportation Cost");
                    dc.Add("aspcmdInstructions", "Instructions");
                    dc.Add("aspcmdDeliveryDetails", "Delivery Details");
                    dc.Add("aspcmdOperationClosure", "Operation Closure");
                    dc.Add("aspcmdUnable", "Unable");
                    dc.Add("aspcmdPartialDelivery", "Partial Delivery");
                    dc.Add("aspcmdDefectOperationClosure", "Defect / Operation Closure");
                    dc.Add("aspcmdHistory", "History");
                    dc.Add("aspcmdNoAnswer", "No Answer");
                    break;
                default:
                    break;
            }
            return dc;
        }

        public void Init(IslAppContext appContext, IslsForm form)
        {
            switch (form.GetType().ToString())
            {
                case "Crm.WUI.CRM.Activities.Activities.cmSalesOpportunityF":
                    _appContext = appContext;
                    cmActivitiesCollection cCol = (form as cmActivitiesF).Items;
                    cCol.PreviewPropertyChanged += new EventHandler<SLnet.Base.Utils.slEventArgs<slPreviewPropChangedArgs>>(cCol_PreviewPropertyChanged);
                    break;
                default:
                    break;
            }


        }
        
        public void SyncScreen(IslAppContext appContext, IslsForm form)
        {

        }

        private void cCol_PreviewPropertyChanged(object sender, SLnet.Base.Utils.slEventArgs<slPreviewPropChangedArgs> e)
        {

            //Activity Fields
            if (sender is cmActivitiesDataObject)
            {
                cmActivitiesDataObject acObj = sender as cmActivitiesDataObject;
                cmSaleItemsCollection siCol = acObj.SaleItems as cmSaleItemsCollection;
                IslObjectActivator oa = _appContext.ServiceLocator.GetService<IslObjectActivator>();
                IslObjectProxyActivator act = _appContext.ServiceLocator.GetService<IslObjectProxyActivator>();
                String CompanyID = cmSys.GetCurrentCompanyID(_appContext);              //acObj.CompanyID;//cmSys.GetCurrentCompanyID(_appContext);
                string lookUp3;
                decimal rate = 0;
                object rateObj;
                decimal totalPurchasesValue = 0;
                
                gxCurrencyCollection curDc;

                slDataObjectProviderFetchPath args=new slDataObjectProviderFetchPath();

                if (ceilingFactor == 0 || e.EventData.PropertyName=="asClientCurrency")
                {
                    using (var lakis = oa.CreateObject(gxSys.GetRegName(gxObjRegNameIS.Currency)))
                    {
                        curDc = (gxCurrencyCollection)lakis.ExecuteOperation("Get", args);
                    }
                    foreach (gxCurrencyDataObject item in curDc)
                    {
                        if (acObj.GetDynamicValue("asClientCurrency") != null)
                            if (item.ID.ToString() == acObj.GetDynamicValue("asClientCurrency").ToString())
                                if (item.GetDynamicValue("asCeiling") != null)
                                    ceilingFactor = (decimal)item.GetDynamicValue("asCeiling");
                    }
                }

                
                FillDirectories();
                switch (e.EventData.PropertyName)
                {

                    case "ReferenceNum":
                    case "StandAlone":
                        if (siCol.Count == 0)
                        {
                            switch (CompanyID)
                            {
                                case "8e50fec4-a933-444d-b33f-c762685f2cab":    //Shelde
                                    if (acObj.CompanyID == null) { acObj.CompanyID = CompanyID; }
                                    acObj.AccountID = "a0646ae4-a397-461f-a9d6-edc2abcb59c0";
                                    AddCommissionLine(acObj);
                                    break;
                                case "b5a18dc0-ac35-4637-a79a-961194e7425b":    //Bosung
                                    if (acObj.CompanyID == null) { acObj.CompanyID = CompanyID; }
                                    acObj.AccountID = "5acc2b04-50d8-482e-b428-f046249f85c7";
                                    AddCommissionLine(acObj);
                                    break;
                                case "9c7290b4-7333-4f67-accc-295ddd32a574":    //DMI   
                                    if (acObj.CompanyID == null) { acObj.CompanyID = CompanyID; }
                                    if (acObj.ActivityTypeID == actTypes["Client Request"])
                                        acObj.AccountID = "570ebd22-c51c-42fe-8c91-9e2a4b19bb9d";
                                    using (var ops = act.GetObjectProxy<IslOperations>("slgServices:aspDiamondCrmBusinessObjects", false))
                                    {
                                        if (acObj.GetDynamicValue("asOurRefNum") == null)
                                            acObj.SetDiffDynamicValue("asOurRefNum", ops.ExecuteOperation("GetRONumber", acObj.CompanyID, "E/"));
                                    }
                                    AddCommissionLine(acObj);
                                    break;
                                case "8d934e28-1858-4ebd-95ed-6558feee8511":    //Mare
                                    if (acObj.CompanyID == null) { acObj.CompanyID = CompanyID; }
                                    using (var ops = act.GetObjectProxy<IslOperations>("slgServices:aspDiamondCrmBusinessObjects", false))
                                    {
                                        if (acObj.GetDynamicValue("asOurRefNum") == null)
                                            acObj.SetDiffDynamicValue("asOurRefNum", ops.ExecuteOperation("GetRONumber", acObj.CompanyID, "E/"));
                                    }
                                    break;
                                case "743d7942-4ccb-474b-b140-06011f6795cc":    //Diamond
                                    if (acObj.CompanyID == null) { acObj.CompanyID = CompanyID; }
                                    using (var ops = act.GetObjectProxy<IslOperations>("slgServices:aspDiamondCrmBusinessObjects", false))
                                    {
                                        if (acObj.GetDynamicValue("asOurRefNum") == null)
                                            acObj.SetDiffDynamicValue("asOurRefNum", ops.ExecuteOperation("GetRONumber", acObj.CompanyID, "E/"));
                                    }
                                    if (acObj.AccountID == null && acObj.ActivityTypeID == actTypes["Client Request"])
                                    {
                                        acObj.AccountID = "9d6f1115-960b-4865-89cb-e09aedaaee94";
                                        acObj.CustSite = "b8fd45de-51ad-4379-8d98-176c7c64b517";
                                        acObj.LookupField3 = "b8fd45de-51ad-4379-8d98-176c7c64b517";
                                    }
                                    if (acObj.GetDynamicValue("asSupplierCurrency") == null)
                                        acObj.SetDynamicValue("asSupplierCurrency", "9efe2b51-f986-4071-9ce4-65d42075740b");
                                    if (acObj.GetDynamicValue("asClientCurrency") == null)
                                        acObj.SetDynamicValue("asClientCurrency", "9efe2b51-f986-4071-9ce4-65d42075740b");
                                    acObj.LookupField4 = "9d06c745-dc9c-420a-8fe5-66fbb59545b7";
                                    break;
                                case "d5636571-49cf-43b7-b6a6-3479efc00f60":    //KK
                                    if (acObj.CompanyID == null) { acObj.CompanyID = CompanyID; }
                                    using (var ops = act.GetObjectProxy<IslOperations>("slgServices:aspDiamondCrmBusinessObjects", false))
                                    {
                                        if (acObj.GetDynamicValue("asOurRefNum") == null)
                                            acObj.SetDiffDynamicValue("asOurRefNum", ops.ExecuteOperation("GetRONumber", acObj.CompanyID, "E/"));
                                    }
                                    if (acObj.AccountID == null) acObj.AccountID = "b51b1635-979a-4de5-a0a7-d9bb232d0e93";
                                    if (acObj.GetDynamicValue("asSupplierCurrency") == null)
                                        acObj.SetDynamicValue("asSupplierCurrency", "9efe2b51-f986-4071-9ce4-65d42075740b");
                                    if (acObj.GetDynamicValue("asClientCurrency") == null)
                                        acObj.SetDynamicValue("asClientCurrency", "9efe2b51-f986-4071-9ce4-65d42075740b");
                                    break;
                                default:
                                    break;
                            }
                        }

                        break;


                    case "asSupplierCurrency":
                        using (var obj = oa.CreateObject(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects")))
                        {
                            rateObj = obj.ExecuteOperation("GetRate", acObj.GetDynamicValue("asSupplierCurrency"), acObj.GetDynamicValue("asClientCurrency")) ?? "";
                            decimal.TryParse(rateObj.ToString(), out rate);
                            acObj.FloatField4 = rate;
                        }
                        break;
                    case "asClientCurrency":
                        using (var obj = oa.CreateObject(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects")))
                        {
                            rateObj = obj.ExecuteOperation("GetRate", acObj.GetDynamicValue("asSupplierCurrency"), acObj.GetDynamicValue("asClientCurrency")) ?? "";
                            decimal.TryParse(rateObj.ToString(), out rate);
                            acObj.FloatField4 = rate;
                        }
                        break;
                    case "asMargin":
                        foreach (cmSaleItemsDataObject item in siCol)
                        {
                            item.FloatField2 = Convert.ToDecimal(acObj.GetDynamicValue("asMargin")); // Push Margin to lines 
                        }
                        break;
                    case "FloatField1":
                    case "FloatField3":
                        foreach (cmSaleItemsDataObject item in siCol)
                        {
                            if (item.ItemDescription == "Commission")
                            {
                                item.ItemPrice = acObj.FloatField1 * acObj.FloatField3 / 100;
                            }
                        }
                        break;
                    case "asSupplierRefNum":// Schelde,Bosung SupRef=>OurRef
                        if (acObj.CompanyID == "8e50fec4-a933-444d-b33f-c762685f2cab" || acObj.CompanyID == "b5a18dc0-ac35-4637-a79a-961194e7425b")
                        {
                            acObj.SetDynamicValue("asOurRefNum", acObj.GetDynamicValue("asSupplierRefNum"));
                        }
                        break;

                    case "StringField3"://Not in DMI,Mare,Diamond,KK
                        if (acObj.CompanyID != "9c7290b4-7333-4f67-accc-295ddd32a574" && acObj.CompanyID != "8d934e28-1858-4ebd-95ed-6558feee8511"
                            && acObj.CompanyID != "743d7942-4ccb-474b-b140-06011f6795cc" && acObj.CompanyID != "d5636571-49cf-43b7-b6a6-3479efc00f60")
                        {
                            acObj.StringField1 = acObj.StringField3;
                        }
                        break;
                    case "asOurRefNum"://Mare,Diamond,KK,DMI asOurRefNum=>Subject
                        if (acObj.CompanyID == "8d934e28-1858-4ebd-95ed-6558feee8511" || acObj.CompanyID == "743d7942-4ccb-474b-b140-06011f6795cc"
                            || acObj.CompanyID == "d5636571-49cf-43b7-b6a6-3479efc00f60" || acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")
                        {
                            acObj.Subject = acObj.GetDynamicValue("asOurRefNum").ToString();
                        }
                        break;
                    case "aspTotalCharges":
                        if (acObj.CompanyID == "743d7942-4ccb-474b-b140-06011f6795cc")
                        {
                            foreach (cmSaleItemsDataObject item in siCol)
                            {
                                if (acObj.FloatField5 * item.FloatField5 != 0)
                                    item.FloatField3 = Convert.ToDecimal(acObj.GetDynamicValue("aspTotalCharges")) / acObj.FloatField5 * item.FloatField5;
                                else
                                    item.FloatField3 = 0;
                            }
                        }
                        break;
                    case "CustSite":
                        if (acObj.AccountID == "9d6f1115-960b-4865-89cb-e09aedaaee94")
                            acObj.LookupField3 = acObj.CustSite;

                        break;
                    case "LookupField3":
                        if (acObj.AccountID == "9d6f1115-960b-4865-89cb-e09aedaaee94")
                        {
                            using (var obj = oa.CreateObject(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects")))
                            {
                                lookUp3 = (string)obj.ExecuteOperation("GetLookUp3Description", acObj.LookupField3.ToString()) ?? "";
                            }
                            acObj.StringField7 ="Προσφορά ανταλλακτικών για κινητήρα MITSUBISHI S16R - PTA του "+ lookUp3.Replace("ΔΕΗ Α.Ε. /","");
                        }
                        break;
                    case "asReadyDate":
                        foreach (cmSaleItemsDataObject item in siCol)
                        {
                            item.SetDynamicValue("asReadyDate", Convert.ToDateTime(acObj.GetDynamicValue("asReadyDate"))); // Push Margin to lines 
                        }
                        break;
                    default:
                        break;
                }
            }

            // SaleItems Fields
            if (sender is cmSaleItemsDataObject)
            {

                cmSaleItemsDataObject siObj = sender as cmSaleItemsDataObject;
                cmSaleItemsCollection acCol = (cmSaleItemsCollection)siObj.Owner;
                cmActivitiesDataObject acObj=(cmActivitiesDataObject)acCol.ParentObject;
                decimal totalPurchasesValue = 0;
                decimal realPrice = 0;
                switch (e.EventData.PropertyName)
                {
                    case "ItemID":
                        siObj.FloatField1 = 0;//Client Currency Purchase Price
                        siObj.FloatField3 = 0;//Charges
                        siObj.FloatField4 = 0;//Supplier Currency Purchase Price
                        siObj.FloatField5 = 0;//Line Purchase Value
                        siObj.FloatField6 = 0;//Purchase Discount Percentage
                        siObj.FloatField7 = 0;//Purchase Discount Value
                        if (acObj.GetDynamicValue("asMargin") != null)//Set Line Margin
                        {
                            siObj.FloatField2 = Convert.ToDecimal(acCol.ParentObject.GetDynamicValue("asMargin"));
                        }
                        else 
                        { 
                            siObj.FloatField2 = 0; 
                        }
                        siObj.SetDiffDynamicValue("asReadyDate", acCol.ParentObject.GetDynamicValue("asReadyDate"));

                        break;
                    case "AlterCodeID":
                        siObj.FloatField1 = 0;//Client Currency Purchase Price
                        siObj.FloatField3 = 0;//Charges
                        siObj.FloatField4 = 0;//Supplier Currency Purchase Price
                        siObj.FloatField5 = 0;//Line Purchase Value
                        siObj.FloatField6 = 0;//Purchase Discount Percentage
                        siObj.FloatField7 = 0;//Purchase Discount Value
                        if (acObj.GetDynamicValue("asMargin") != null)//Set Line Margin
                        {
                            siObj.FloatField2 = Convert.ToDecimal(acCol.ParentObject.GetDynamicValue("asMargin"));
                        }
                        else 
                        { 
                            siObj.FloatField2 = 0; 
                        }

                        break;                        
                    case "ItemQty":
                        if (Convert.ToDecimal(siObj.FloatField2) == 100)
                            siObj.ItemPrice = 0;
                        else
                        {
                            realPrice=(Convert.ToDecimal(siObj.FloatField1) / ((100 - Convert.ToDecimal(siObj.FloatField2)) / 100))
                                        + Convert.ToDecimal(siObj.FloatField3);
                            if (ceilingFactor == 0)
                                siObj.ItemPrice = realPrice;
                            else
                                siObj.ItemPrice = Math.Ceiling(realPrice / ceilingFactor) * ceilingFactor;
                        }
                        updating = false;
                        siObj.FloatField7 = (siObj.ItemQty * siObj.FloatField4) * siObj.FloatField6 / 100;//Calc Purch Disc Value
                        siObj.FloatField5 = (siObj.ItemQty * siObj.FloatField4)-siObj.FloatField7; //Recalc Total Purch Value
                        siObj.FloatField1 = (siObj.FloatField5 / siObj.ItemQty) * acObj.FloatField4;//Set CliPurchPrice=SupPurchPRice*Header Rate
                        break;
                    case "FloatField1":// Customer Currency Purchase Price
                        realPrice = (decimal)((((siObj.FloatField5 ?? 0) / siObj.ItemQty) * (acObj.FloatField4 ?? 0)) / ((100 - Convert.ToDecimal(siObj.FloatField2 ?? 0)) / 100))
                                        + Convert.ToDecimal(siObj.FloatField3 ?? 0);
                        if (ceilingFactor == 0)
                            siObj.ItemPrice = realPrice;
                        else
                            siObj.ItemPrice = Math.Ceiling(realPrice / ceilingFactor) * ceilingFactor;
                        break;
                    case "FloatField2":// Line Margin
                        if (!updating)
                        {
                            updating = true;
                            if (Convert.ToDecimal(siObj.FloatField2) == 100)
                                siObj.ItemPrice = 0;
                            else
                            {
                                realPrice = (decimal)((((siObj.FloatField5 ?? 0) / siObj.ItemQty) * (acObj.FloatField4 ?? 0)) / ((100 - Convert.ToDecimal(siObj.FloatField2 ?? 0)) / 100))
                                            + Convert.ToDecimal(siObj.FloatField3 ?? 0);
                                if (ceilingFactor == 0)
                                    siObj.ItemPrice = realPrice;
                                else
                                    siObj.ItemPrice = Math.Ceiling(realPrice / ceilingFactor) * ceilingFactor;
                            }
                            updating = false;
                        }
                        break;
                    case "ItemPrice":
                        if (!updating)
                        {
                            updating = true;
                            if (((siObj.FloatField5 / siObj.ItemQty) * acObj.FloatField4) != 0)
                            {
                                if (siObj.ItemPrice == siObj.FloatField3)
                                {
                                    siObj.FloatField1 = 0;
                                    siObj.FloatField2 = 0;
                                    siObj.FloatField3 = 0;
                                }
                                else 
                                    siObj.FloatField2 = 100 * (1 - (siObj.FloatField1 / (siObj.ItemPrice - siObj.FloatField3)));
                            }
                            updating = false;
                        }

                        break;
                    case "FloatField3"://Charges
                        if (Convert.ToDecimal(siObj.FloatField2) == 100)
                            siObj.ItemPrice = 0;
                        else
                        {
                            realPrice = (decimal)((((siObj.FloatField5 ?? 0) / siObj.ItemQty) * (acObj.FloatField4 ?? 0)) / ((100 - Convert.ToDecimal(siObj.FloatField2 ?? 0)) / 100))
                                        + Convert.ToDecimal(siObj.FloatField3 ?? 0);
                            if (ceilingFactor == 0)
                                siObj.ItemPrice = realPrice;
                            else
                                siObj.ItemPrice = Math.Ceiling(realPrice / ceilingFactor) * ceilingFactor;
                        }
                        break;
                    case "FloatField4"://Purchase Price
                        siObj.FloatField7 = (siObj.ItemQty * siObj.FloatField4) * siObj.FloatField6 / 100;//Calc Purch Disc Value
                        siObj.FloatField5 = (siObj.ItemQty * siObj.FloatField4)-siObj.FloatField7; //Recalc Total Purch Value
                        siObj.FloatField1 = (siObj.FloatField5 / siObj.ItemQty) * acObj.FloatField4;//Set CliPurchPrice=SupPurchPRice*Header Rate
                        break;
                    case "FloatField6":
                        siObj.FloatField7 = (siObj.ItemQty * siObj.FloatField4) * siObj.FloatField6 / 100;//Calc Purch Disc Value
                        siObj.FloatField5 = (siObj.ItemQty * siObj.FloatField4)-siObj.FloatField7; //Recalc Total Purch Value
                        siObj.FloatField1 = (siObj.FloatField5 / siObj.ItemQty) * acObj.FloatField4;//Set CliPurchPrice=SupPurchPRice*Header Rate
                        break;
                    case "FloatField5"://Total Line Purch Value
                        foreach (cmSaleItemsDataObject item in acCol)
                        {
                            totalPurchasesValue += (decimal)item.FloatField5;
                        }
                        if (siObj.LineNum > acCol.Count) { totalPurchasesValue += (decimal)siObj.FloatField5; }
                        acObj.FloatField5 = totalPurchasesValue;//Calc & Set Total Purchases Value
                        break;
                    default:
                        break;
                }
            }
        }



        private static void AddCommissionLine(cmActivitiesDataObject acObj)
        {
            cmSaleItemsCollection acSiCol = acObj.SaleItems as cmSaleItemsCollection;
            cmSaleItemsDataObject acSiObj = acSiCol.New();
            acSiObj.ItemID = "fa23800a-76c7-45d0-b56f-3cba83d93026";
            acSiCol.Add(acSiObj);
        }

        private GridView GetItemLinesGridView(cmActivitiesF form)
        {
            var grpInfo = form.MainLayoutGroup.Items[0] as TabbedControlGroup;
            var tabLines = grpInfo.TabPages[0] as LayoutControlGroup;
            var loItemLines = tabLines.Items.FindByName("loSaleItems") as LayoutControlItem;
            var grcItemLines = loItemLines.Control as cmGridControl;
            return grcItemLines.MainView as GridView;

        }

        // 21.30
        //private GridView GetItemLinesGridView(cmActivitiesF form)
        //{
        //    var grpInfo = form.MainLayoutGroup.Items[4] as TabbedControlGroup;
        //    var tabLines = grpInfo.TabPages[0] as LayoutControlGroup;
        //    var loItemLines = tabLines.Items.FindByName("loSaleItems") as LayoutControlItem;
        //    var grcItemLines = loItemLines.Control as cmGridControl;
        //    return grcItemLines.MainView as GridView;

        //}


    }


}
