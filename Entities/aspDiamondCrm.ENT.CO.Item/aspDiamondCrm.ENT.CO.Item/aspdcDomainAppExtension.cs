﻿using System;
using SLnet.Base.Customization.Interfaces;
using SLnet.Base.Customization;
using SLnet.Base.Interfaces;
using Glx.Data.DataObjects;
using Glx.Core.Base.Trace;


namespace aspDiamondCrm.ENT.CO.Item
{
    [slRegisterCustomDomainAppExtension()]
    public class aspdcDomainAppExtension : IslCustomDomainAppExtension, IslCustomDomainAppObjectExtension
    {


        public void AfterExecuteOperation(ref object res, SLnet.Base.Interfaces.IslAppContext appContext, SLnet.Base.Interfaces.IslOperations container, SLnet.Base.slBaseOperation operation, params object[] args)
        {
            switch (container.ToString())
            {
                case "Glx.ENT.CO.Item.gxItem":
                    switch (operation.ToString())
                    {

                        case "Glx.ENT.CO.Item.gxItem+opServer_PreValidateProcessing":
                                RunCustomValidations(args);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        private Int32 YearParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 4)) - 5924;
        }
        private Int32 MonthParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 1)) - 1;
        }
        private Int32 DayParam(string data)
        {
            return Convert.ToInt16(data.Substring(20, 2)) + 11;
        }

        private void RunCustomValidations(object[] args)
        {


            if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
            {
                gxItemCollection itemCol = (gxItemCollection)args[0];
                gxItemDataObject itemDAO = itemCol[0];
                gxItemCompanyPropCollection icpCol = (gxItemCompanyPropCollection)itemDAO.CompanyProps;
                foreach (gxItemCompanyPropDataObject icp in icpCol)
                {
                    if (icp.CompID == "743d7942-4ccb-474b-b140-06011f6795cc" && icp.BalanceScenarioOverrides.Count == 0
                        && icp.ItemNature == (Glx.Data.DataObjects.gxItemCompanyPropDataObject.ItemNatureEnum)1)
                    {
                        gxStockItemCollection stiCol = (gxStockItemCollection)icp.StockItems;
                        gxItCntrlBalScnOverrDataObject BSODAO = icp.BalanceScenarioOverrides.New();
                        BSODAO.ItemBalanceControl = (Glx.Data.DataObjects.gxItCntrlBalScnOverrDataObject.ItemBalanceControlEnum)1;
                        BSODAO.ItemID = icp.ItemID;

                        BSODAO.IcbsID = "72c51dc7-d501-4f31-aa98-3390b6efcec7";
                        BSODAO.StitID = stiCol[0].ID;
                        BSODAO.ItcpID = icp.ID;
                        icp.BalanceScenarioOverrides.Add(BSODAO);
                    }
                }
            }
        }


        public void BeforeExecuteOperation(SLnet.Base.Interfaces.IslAppContext appContext, SLnet.Base.Interfaces.IslOperations container, SLnet.Base.slBaseOperation operation, params object[] args)
        {
        }


    }
}
