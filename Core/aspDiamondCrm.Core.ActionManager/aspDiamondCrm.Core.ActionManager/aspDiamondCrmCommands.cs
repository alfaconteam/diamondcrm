﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Glx.Core.ActionManager;
using SLnet.Base.Interfaces;
using SLnet.Base.Attributes;
using aspDiamondCrm.Core.Base;

namespace aspDiamondCrm.Core.ActionManager
{

    #region FORM ACTIONS REGISTRATION
    #endregion

    #region LIST ACTIONS REGISTRATION
    #endregion

    #region BROWSER ACTIONS REGISTRATION
    #endregion

    #region REPORT ACTIONS REGISTRATION
    #endregion


    [slRegisterObject(aspDiamondCrmObjRegName.DomainName, aspDiamondCrmObjRegName.AliasName)]
    public class aspDiamondCrmCommands : gxBaseActionsManager
    {

        public aspDiamondCrmCommands(IslAppContext appContext)
            : base(appContext)
        {
        }

        #region FORM ACTIONS DECLARATION
        #endregion

        #region LIST ACTIONS DECLARATION
        #endregion

        #region BROWSER ACTIONS DECLARATION
        #endregion

        #region REPORTS ACTIONS DECLARATION
        #endregion

    }

}
