﻿using System;
using SLnet.Base.Customization.Interfaces;
using SLnet.Base.Customization;
using SLnet.Base.Interfaces;
using Crm.Data.DataObjects;
using Crm.Core.Base.Trace;
using System.Collections.Generic;
using SLnet.Sand.Base.Interfaces;
using SLnet.Sand.Base.Forms;
using SLnet.Base.DbUtils;
using System.Data;
using SLnet.Base.DbUtils.Interfaces;
using Crm.Core.Base;
using Crm.Data.Structure;
using System.Windows.Forms;
using System.Xml.Linq;


namespace aspDiamondCrm.ENT.CRM.Activities

{
    [slRegisterCustomDomainAppExtension()]
    public class aspDiamondCrmDomainAppExtension : IslCustomDomainAppExtension, IslCustomDomainAppObjectExtension
    {
        public static string curResource;
        public static Dictionary<string, string> statuses = new Dictionary<string, string>();
        public static Dictionary<string, string> actTypes = new Dictionary<string, string>();


        public void AfterExecuteOperation(ref object res, SLnet.Base.Interfaces.IslAppContext appContext, SLnet.Base.Interfaces.IslOperations container, SLnet.Base.slBaseOperation operation, params object[] args)
        {
            UpdateParentAndCreateSubActivities(res, appContext, operation, args);
        }
        private Int32 YearParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 4)) - 5924;
        }
        private Int32 MonthParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 1)) - 1;

        }
        private Int32 DayParam(string data)
        {
            return Convert.ToInt16(data.Substring(20, 2)) + 11;
        }
        private void UpdateParentAndCreateSubActivities(object res, SLnet.Base.Interfaces.IslAppContext appContext, SLnet.Base.slBaseOperation operation, object[] args)
        {


            if (DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
            {
                try
                {
                    bool zeroLineMargin = false;
                    switch (operation.ToString())
                    {
                        case "Crm.ENT.CRM.Activities.cmActivities+opServer_PreSaveProcessing":

                            FillDirectories();

                            cmActivitiesCollection col = args[0] as cmActivitiesCollection;
                            cmActivitiesDataObject act = col[0];

                            UpdateParentAndRONumber(appContext, col, act);

                            break;
                        case "Crm.ENT.CRM.Activities.cmActivities+Client_SaveWarnings":
                            //PreSaveChecksAndUpdates(res,args);
                            FillDirectories();
                            zeroLineMargin = false;
                            bool zeroLineCustomerPurchPrice = false;
                            bool zeroLineSupplierPurchPrice = false;
                            decimal? totalPurchValue = 0;

                            col = args[0] as cmActivitiesCollection;
                            act = col[0];

                            if (act.CompanyID == "8d934e28-1858-4ebd-95ed-6558feee8511") //Only Mare
                            {
                                foreach (cmSaleItemsDataObject si in act.SaleItems)
                                {
                                    if (si.FloatField2 == null || si.FloatField2 == 0) zeroLineMargin = true;
                                    if (si.FloatField1 == null || si.FloatField1 == 0 && si.FloatField5 != 0) zeroLineCustomerPurchPrice = true;
                                    if (si.FloatField4 == null || si.FloatField4 == 0) zeroLineSupplierPurchPrice = true;
                                    totalPurchValue += si.FloatField5;
                                }
                                if (zeroLineMargin)
                                {
                                    if (act.ActivityTypeID == actTypes["Client Offer"])
                                    {
                                        (res as cmMessageLogger).AddMessage("Found Zero Margins!", cmMessageLoggerItemKind.Warning);
                                    }
                                    else
                                    {
                                        (res as cmMessageLogger).AddMessage("Found Zero Margins!", cmMessageLoggerItemKind.Warning);
                                    }
                                }
                                if (zeroLineSupplierPurchPrice)
                                {
                                    (res as cmMessageLogger).AddMessage("Found Zero Purchase Price!", cmMessageLoggerItemKind.Warning);
                                }
                                if (zeroLineCustomerPurchPrice)
                                {
                                    (res as cmMessageLogger).AddMessage("Found Zero Sell Price!", cmMessageLoggerItemKind.Warning);
                                }
                                act.FloatField5 = totalPurchValue;
                            }
                            break;
                        default:
                            break;

                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private void UpdateParentAndRONumber(SLnet.Base.Interfaces.IslAppContext appContext, cmActivitiesCollection col, cmActivitiesDataObject act)
        {
            Boolean simpleMoves=false;

            switch (act.CompanyID)
            {
                case "8d934e28-1858-4ebd-95ed-6558feee8511": //Mare   
                case "743d7942-4ccb-474b-b140-06011f6795cc": //Diamond
                case "d5636571-49cf-43b7-b6a6-3479efc00f60": //KK
                    simpleMoves = false;
                    break;
                case "9c7290b4-7333-4f67-accc-295ddd32a574": //DMI
                    if (act.GetDynamicValue("asSimpleMovesFlag") != null)
                        if (Convert.ToInt16(act.GetDynamicValue("asSimpleMovesFlag")) == 1) simpleMoves = true;
                    break;
                case "8e50fec4-a933-444d-b33f-c762685f2cab"://Schelde check is there is already PD
                    if (CheckForPartialDelivery(appContext, act)
                        && (act.StatusID == statuses["Partial Delivery"] || act.StatusID == statuses["Operation Closure"])) simpleMoves = false;
                    else simpleMoves = true;
                    break;
                default:
                    simpleMoves = true;
                    break;
            }


            
                if (simpleMoves)
                // All companies other then Mare Diamond KK , DMI only simpleMoves
                {

                    if (act.ActivityTypeID == actTypes["Client Request"])
                    {
                        CreateSubActivities(col, appContext);
                    }
                    else if (act.ActivityTypeID == actTypes["Supplier Offer"])
                    {
                        UpdateParentActivity(col, appContext, "Purchases"); //To Purchases einai gia na ksero poia pedia tha enimeroso
                    }
                    else if (act.ActivityTypeID == actTypes["Client Offer"])
                    {
                        UpdateParentActivity(col, appContext, "Simple"); //To Sales einai gia na ksero poia pedia tha enimeroso
                    }
                }
                else
                // Mare Diamond KK DMI not SimpleMoves
                {
                    if (act.ActivityTypeID == actTypes["Supplier Offer"]
                        || act.ActivityTypeID == actTypes["Client Order"])
                    {
                        UpdateClientRequest(col, appContext);
                    }
                    if (act.ActivityTypeID == actTypes["Supplier Order"]
                        || act.ActivityTypeID == actTypes["Order Acknowledgement"]
                        || act.ActivityTypeID == actTypes["Notice of Readiness"]
                        || act.ActivityTypeID == actTypes["Transportation Cost"]
                        || act.ActivityTypeID == actTypes["Instructions"]
                        || act.ActivityTypeID == actTypes["Delivery Details"])
                    {
                        UpdateClientRequest(col, appContext);

                    }
                    if (act.ActivityTypeID == actTypes["Client Offer"] && act.StatusID == statuses["Successful"])// Set SupOffer Successful if CliOffer Successful 
                    {
                        UpdateSupplierOffer(appContext, act);
                    }

                    if (act.ActivityTypeID == actTypes["Client Request"] && act.CompanyID != "8e50fec4-a933-444d-b33f-c762685f2cab")
                    {
                        CheckRONumbers(act, appContext);
                    }

                }
        }

        private static void UpdateSupplierOffer(SLnet.Base.Interfaces.IslAppContext appContext, cmActivitiesDataObject act)
        {
            cmActivitiesCollection supOffCol = GetParentActivity(appContext, act);
            cmActivitiesDataObject supOffAct = supOffCol[0];
            
            supOffAct.StatusID = act.StatusID;
            
            PostCollection(appContext, supOffCol);
        }

        private void CheckRONumbers(cmActivitiesDataObject act, IslAppContext appContext)
        {
            string sql = string.Format(" select top 1 convert(nvarchar(50),a.CMID) as cmid from CMACTIVITIES a "+
                                       " inner join CMACTIVITYTYPE at on a.CMACTIVITYTYPEID=at.CMID and cmCode='012' "+
                                       " where a.cmCompanyID=@CompID and a.cmID<>@actID and a.ASOURREFNUM=@refnum ");
            slQueryParameters prms = new slQueryParameters();
            prms.Add("@actID", act.ID.ToString());
            prms.Add("@refnum", act.GetDynamicValue("asOurRefNum").ToString());
            prms.Add("@CompID", act.CompanyID.ToString());
            string result = null;
            IslDataAccessDbProvider opa = appContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
            result = (string) opa.ExecuteScalar(sql, prms);

            if (result != null)
            
            {
                act.SetDiffDynamicValue("asOurRefNum", GetRONumber(appContext, act.CompanyID, "E/"));
                act.Subject = act.GetDynamicValue("asOurRefNum").ToString();
            }

        }

        private void UpdateClientRequest(cmActivitiesCollection col, IslAppContext appContext)
        {
            cmActivitiesCollection parentCol;
            cmActivitiesDataObject parentAct;

            cmActivitiesDataObject act = col[0] as cmActivitiesDataObject;

            if (act.ActivityTypeID == actTypes["Supplier Offer"])
            {
                cmActivitiesCollection supReqCol = GetParentActivity(appContext, act);
                cmActivitiesDataObject SupReqAct = supReqCol[0];

                /*SupReqAct.StatusID = statuses["Supplier Offer"]; // Update Supplier Request no save needed
                PostCollection(appContext, supReqCol);*/

                parentCol = GetParentActivity(appContext, SupReqAct);
                parentAct = parentCol[0];
                parentAct.StatusID = statuses["Supplier Offer"];
                parentAct.StringField1 = act.StringField1;
                parentAct.SetDynamicValue("asDeliveryTime", act.GetDynamicValue("asDeliveryTime"));
                parentAct.SetDynamicValue("asDeliveryTerms", act.GetDynamicValue("asDeliveryTerms"));
                parentAct.SetDynamicValue("asReadyDate", act.GetDynamicValue("asReadyDate"));

            }
            /*if (act.ActivityTypeID == actTypes["Client Offer"])  // Update Client Request no save needed
            {
                cmActivitiesCollection supOffCol = GetParentActivity(appContext, act);
                cmActivitiesDataObject supOffAct = supOffCol[0];
                cmActivitiesCollection supReqCol = GetParentActivity(appContext,supOffAct);
                cmActivitiesDataObject supReqAct = supReqCol[0];
                parentCol = GetParentActivity(appContext, supReqAct);
                parentAct = parentCol[0];
                parentAct.StatusID = statuses["Client Offer"];
                parentAct.StringField1 = act.StringField1;
                parentAct.SetDynamicValue("asDeliveryTime", act.GetDynamicValue("asDeliveryTime"));
                parentAct.SetDynamicValue("asDeliveryTerms", act.GetDynamicValue("asDeliveryTerms"));
            }*/
            else
            {
                parentCol = GetParentActivity(appContext, act);
                parentAct = parentCol[0];

                string key = GetKey(actTypes, act.ActivityTypeID); //actType=> parent Status
                parentAct.StatusID = statuses[key];             //

                if (act.StringField1 != null) parentAct.StringField1 = act.StringField1;
                if (act.StringField2 != null) parentAct.StringField2 = act.StringField2;
                if (act.StringField3 != null) parentAct.StringField3 = act.StringField3;
                if (act.StringField4 != null) parentAct.StringField4 = act.StringField4;
                if (act.StringField5 != null) parentAct.StringField5 = act.StringField5;
                if (act.StringField6 != null) parentAct.StringField6 = act.StringField6;
                if (act.StringField7 != null) parentAct.StringField7 = act.StringField7;
                if (act.FloatField1 != null) parentAct.FloatField1 = act.FloatField1;
                if (act.FloatField2 != null) parentAct.FloatField2 = act.FloatField2;
                if (act.FloatField3 != null) parentAct.FloatField3 = act.FloatField3;
                if (act.FloatField4 != null) parentAct.FloatField4 = act.FloatField4;
                if (act.FloatField5 != null) parentAct.FloatField5 = act.FloatField5;
                if (act.FloatField6 != null) parentAct.FloatField6 = act.FloatField6;
                if (act.FloatField7 != null) parentAct.FloatField7 = act.FloatField7;
                if (act.DateField1 != null) parentAct.DateField1 = act.DateField1;
                if (act.DateField2 != null) parentAct.DateField2 = act.DateField2;
                if (act.DateField3 != null) parentAct.DateField3 = act.DateField3;
                if (act.DateField4 != null) parentAct.DateField4 = act.DateField4;
                if (act.GetDynamicValue("asAttention")!=null) parentAct.SetDynamicValue("asAttention", act.GetDynamicValue("asAttention"));
                if (act.GetDynamicValue("asClientRefNum") != null) parentAct.SetDynamicValue("asClientRefNum", act.GetDynamicValue("asClientRefNum"));
                if (act.GetDynamicValue("asCurrency") != null) parentAct.SetDynamicValue("asCurrency", act.GetDynamicValue("asCurrency"));
                if (act.GetDynamicValue("asDeliveryDate") != null) parentAct.SetDynamicValue("asDeliveryDate", act.GetDynamicValue("asDeliveryDate"));
                if (act.GetDynamicValue("asDeliveryTerms") != null) parentAct.SetDynamicValue("asDeliveryTerms", act.GetDynamicValue("asDeliveryTerms"));
                if (act.GetDynamicValue("asDeliveryTime") != null) parentAct.SetDynamicValue("asDeliveryTime", act.GetDynamicValue("asDeliveryTime"));
                if (act.GetDynamicValue("asReadyDate") != null) parentAct.SetDynamicValue("asReadyDate", act.GetDynamicValue("asReadyDate"));
                if (act.GetDynamicValue("asOrigin") != null) parentAct.SetDynamicValue("asOrigin", act.GetDynamicValue("asOrigin"));
                if (act.GetDynamicValue("asShipmentmarks") != null) parentAct.SetDynamicValue("asShipmentmarks", act.GetDynamicValue("asShipmentmarks"));
                if (act.GetDynamicValue("asSubjectDetails") != null) parentAct.SetDynamicValue("asSubjectDetails", act.GetDynamicValue("asSubjectDetails"));
                if (act.GetDynamicValue("asOurRefNum") != null) parentAct.SetDynamicValue("asOurRefNum", act.GetDynamicValue("asOurRefNum"));
                if (act.GetDynamicValue("asSupplierRefNum") != null) parentAct.SetDynamicValue("asSupplierRefNum", act.GetDynamicValue("asSupplierRefNum"));
                if (act.GetDynamicValue("asTransported") != null) parentAct.SetDynamicValue("asTransported", act.GetDynamicValue("asTransported"));
                if (act.GetDynamicValue("asCustomerFreight") != null) parentAct.SetDynamicValue("asCustomerFreight", act.GetDynamicValue("asCustomerFreight"));
                if (act.GetDynamicValue("asCustomerPackNHandle") != null) parentAct.SetDynamicValue("asCustomerPackNHandle", act.GetDynamicValue("asCustomerPackNHandle"));
                if (act.GetDynamicValue("asDeliveryAddress") != null) parentAct.SetDynamicValue("asDeliveryAddress", act.GetDynamicValue("asDeliveryAddress"));
            }
            cmSaleItemsCollection siCol = act.SaleItems;



            PostCollection(appContext, parentCol);
        }

        private void UpdateClientOrder(cmActivitiesCollection col, IslAppContext appContext)
        {
            /*cmActivitiesDataObject act = col[0] as cmActivitiesDataObject;

            string cliOrdSql = string.Format(" select top 1 lower(cmActivities.cmID) as cmID from cmActivities " +
                                                      " inner join cmActivityType on cmActivities.CMACTIVITYTYPEID=cmActivityType.CMID and cmActivityType.cmCode='016' " +
                                                      " where cmActivities.CMORIGINACTIVITYID=@actID ");
            slQueryParameters cliOrdPrms = new slQueryParameters();
            IslDataAccessDbProvider cliOrdOpa = appContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
            cliOrdPrms.Add("@actID", act.ID.ToString());

            string cliOrdID = null;

            cliOrdID = cliOrdOpa.ExecuteScalar(cliOrdSql, cliOrdPrms).ToString();

            cmActivitiesDataContext cliOrdColContext = GetCollectionContextByID(appContext, cliOrdID);
            cmActivitiesCollection cliOrdCol = (cmActivitiesCollection)cliOrdColContext.GetCollectionFromDataObjectContext(appContext);
            cmActivitiesDataObject cliOrdAct = cliOrdCol[0];
            cliOrdAct.SetDiffDynamicValue("asReadyDate", act.GetDynamicValue("asReadyDate"));



            PostCollection(appContext, cliOrdCol);
             * */
        }

        private static void PostCollection(IslAppContext appContext, cmActivitiesCollection parentCol)
        {
            cmMessageLogger log = new cmMessageLogger();
            IslObjectActivator ObjectActivator = appContext.ServiceLocator.GetService<IslObjectActivator>();
            using (var obj = ObjectActivator.CreateObject("CrmNet:Activities"))
            {

                log.Merge((cmMessageLogger)obj.ExecuteOperation("Post", parentCol.DataContext));
            }
        }

        public static TKey GetKey<TKey, TValue>(Dictionary<TKey, TValue> dictionary, TValue Value)
        {
            List<TKey> KeyList = new List<TKey>(dictionary.Keys);
            foreach (TKey key in KeyList)
                if (dictionary[key].Equals(Value))
                    return key;
            throw new KeyNotFoundException();
        }
        
        private cmActivitiesDataContext GetCollectionContextByID(IslAppContext appContext, string actID)
        {

            IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();

            using (var obj = oa.CreateObject(cmSys.GetRegName(cmObjRegNameCRM.Activities)))
            {

                cmActivitiesDataContext dc = (cmActivitiesDataContext)obj.ExecuteOperation("GetByID", actID);
                return dc;
            }
        }

        private void FillDirectories()
        {
            if (statuses.Count == 0)
            {
                statuses.Add("Σε εξέλιξη", "73d6c038-cf5c-4a3b-81d4-01bb8fe2615c");
                statuses.Add("Supplier Request", "f2f09f3b-c35b-40bd-8544-c640d302acd8");
                statuses.Add("Supplier Offer", "67b05d9c-40af-483f-a501-fd259dad636c");
                statuses.Add("Client Offer", "85a9a0b7-d42a-4193-9d08-b280d085b2c2");
                statuses.Add("Client Order", "b3a9b378-3d95-41b4-b7d3-acff122dd14f");
                statuses.Add("Supplier Order", "e4819175-dedf-4d9d-8c44-5cc42046c576");
                statuses.Add("Order Acknowledgement", "2a65f7f5-fbf5-4ed1-a806-12713ac63e14");
                statuses.Add("Notice of Readiness", "f3f1148a-81b5-4cee-9ecc-f54c5632ef56");
                statuses.Add("Transportation Cost", "64468d4d-d6e0-4375-9a42-0b56b2138164");
                statuses.Add("Instructions", "be7f1c8e-71fe-402a-a636-5f52fa747847");
                statuses.Add("Delivery Details", "de753966-5185-4f1e-9aa4-eba319b2cb23");
                statuses.Add("Operation Closure", "bee0abf1-86b4-4001-ac53-5fc3f504c834");
                statuses.Add("Unable", "6764efd3-f13a-40ee-ab04-55a4e4974c78");
                statuses.Add("Partial Delivery", "52426089-0c0b-4e1d-b8bf-0c59ba44b82a");
                statuses.Add("Defect", "c68d2900-1b09-4b64-bb90-278c74de3fd1");
                statuses.Add("Successful", "6870893a-e25d-4986-b35a-e974f63e7daa");
                statuses.Add("No Answer", "53116064-ae78-406f-ae76-f8a902886d68");
                statuses.Add("Defect Operation Closure", "b2029122-420c-4cbf-8a21-595fd76226cd");


            }
            if (actTypes.Count == 0)
            {

                actTypes.Add("Client Request", "6b3c7b88-25b3-4b7c-a942-5b25d0d555bf");
                actTypes.Add("Supplier Request", "b5f505f4-95f1-4f07-bae8-8e3bc2f4861f");
                actTypes.Add("Supplier Offer", "2cf5708d-7e7e-4267-9247-02002161639a");
                actTypes.Add("Client Offer", "99953d38-c77a-469a-a36e-59aa96734013");
                actTypes.Add("Client Order", "75c2f842-1204-439f-b24b-edfd4dcee2ff");
                actTypes.Add("Supplier Order", "a6e68457-1228-4530-9022-dc4fbe09c641");
                actTypes.Add("Order Acknowledgement", "81f27b85-83d3-490a-a6db-f59dbff26412");
                actTypes.Add("Notice of Readiness", "cb1c2d3c-1090-4127-b441-9ea88f66aa60");
                actTypes.Add("Transportation Cost", "c217ee74-b321-41a5-a292-d4da026d23f5");
                actTypes.Add("Instructions", "0bfcaaeb-af36-45eb-b152-f6b271d03b2a");
                actTypes.Add("Delivery Details", "f3f6223e-858c-4b87-b6d3-034783b6f9ab");
                actTypes.Add("Operation Closure", "564d5b33-ae25-44f5-be50-5846c75990da");
                actTypes.Add("Unable", "b248a59b-0e97-4968-b446-df7291ca2f27");
                actTypes.Add("Partial Delivery", "6236f073-dfaa-4e20-977b-112b379bf7a6");
                actTypes.Add("Defect", "63641218-70eb-417c-ad8f-662f16961a97");
                actTypes.Add("No Answer", "c63cfaf2-2117-4920-8331-faa04fed6b75");
                actTypes.Add("Defect Operation Closure", "02b4d1f2-7dd5-4148-b591-56f08c4de318");
            }
        }

        private static void CreateSubActivities(cmActivitiesCollection col, SLnet.Base.Interfaces.IslAppContext appContext)
        {

            cmActivitiesDataObject newAct = col.New();
            cmActivitiesDataObject act = col[0];
            if (act.CompanyID != "8d934e28-1858-4ebd-95ed-6558feee8511" && act.CompanyID != "743d7942-4ccb-474b-b140-06011f6795cc")
            {

                cmSaleItemsCollection siCol = act.SaleItems;

                cmActivityResourcesDataObject actR = act.Resources[0];
                curResource = actR.ResourceID;

                newAct.ID = newAct.NewGuid();
                newAct.OriginActivityID = act.ID;
                newAct.OwnerID = cmSys.GetUserProfileInfo(appContext).EmployeeID;
                newAct.Subject = act.Subject;
                newAct.IsPrivate = act.IsPrivate;
                newAct.ShowTimeAs = act.ShowTimeAs;
                newAct.Priority = act.Priority;
                newAct.AccountID = act.AccountID;
                newAct.PropPartInRes = act.PropPartInRes;
                newAct.StatusID = statuses["Σε εξέλιξη"];
                newAct.CreatorID = act.CreatorID;
                newAct.Start = cmSys.GetUserProfileInfo(appContext).WorkDate;
                newAct.PaymentTermID = act.PaymentTermID;
                newAct.CompanyID = act.CompanyID;
                newAct.ComEntryTypeID = act.ComEntryTypeID;
                newAct.SiteID = act.SiteID;
                newAct.HotelID = act.HotelID;
                newAct.IsFromWorkFlow = 1;
                newAct.SetDynamicValue("asShipOwner", act.GetDynamicValue("asShipOwner"));
                newAct.SetDynamicValue("asShip", act.GetDynamicValue("asShip"));
                newAct.RevNum = 1;
                newAct.Created = DateTime.Now;
                newAct.InsUserID = cmSys.GetCurrentUserID(appContext);
                newAct.InsWrpsID = act.InsWrpsID;
                newAct.InsDate = DateTime.Now;
                newAct.StatusType = (cmActivitiesDataObject.StatusTypeEnum)2;
                newAct.StringField1 = act.StringField1;
                newAct.StringField2 = act.StringField2;
                newAct.StringField3 = act.StringField3;
                newAct.StringField4 = act.StringField4;
                newAct.StringField5 = act.StringField5;
                newAct.StringField6 = act.StringField6;
                newAct.StringField7 = act.StringField7;
                newAct.FloatField1 = act.FloatField1;
                newAct.FloatField2 = act.FloatField2;
                newAct.FloatField3 = act.FloatField3;
                newAct.FloatField4 = act.FloatField4;
                newAct.FloatField5 = act.FloatField5;
                newAct.FloatField6 = act.FloatField6;
                newAct.FloatField7 = act.FloatField7;
                newAct.DateField1 = act.DateField1;
                newAct.DateField2 = act.DateField2;
                newAct.DateField3 = act.DateField3;
                newAct.DateField4 = act.DateField4;
                newAct.SetDynamicValue("asAttention", act.GetDynamicValue("asAttention"));
                newAct.SetDynamicValue("asClientRefNum", act.GetDynamicValue("asClientRefNum"));
                newAct.SetDynamicValue("asCurrency", act.GetDynamicValue("asCurrency"));
                newAct.SetDynamicValue("asDeliveryDate", act.GetDynamicValue("asDeliveryDate"));
                newAct.SetDynamicValue("asDeliveryTerms", act.GetDynamicValue("asDeliveryTerms"));
                newAct.SetDynamicValue("asDeliveryTime", act.GetDynamicValue("asDeliveryTime"));
                newAct.SetDynamicValue("asOrigin", act.GetDynamicValue("asOrigin"));
                newAct.SetDynamicValue("asShipmentmarks", act.GetDynamicValue("asShipmentmarks"));
                newAct.SetDynamicValue("asSubjectDetails", act.GetDynamicValue("asSubjectDetails"));
                newAct.SetDynamicValue("asOurRefNum", act.GetDynamicValue("asOurRefNum"));
                newAct.SetDynamicValue("asSupplierRefNum", act.GetDynamicValue("asSupplierRefNum"));
                newAct.SetDynamicValue("asTransported", act.GetDynamicValue("asTransported"));
                newAct.LookupField1 = act.LookupField1;
                newAct.LookupField2 = act.LookupField2;
                newAct.LookupField3 = act.LookupField3;
                newAct.LookupField4 = act.LookupField4;
                newAct.LookupField5 = act.LookupField5;
                newAct.LookupField6 = act.LookupField6;
                newAct.LookupField7 = act.LookupField7;

                Int16 step = Convert.ToInt16(act.GetDynamicValue("asStep"));



                DataSet itemDS = GetNewRefNum(appContext);

                if (itemDS.Tables[0].Rows.Count != 0)
                {
                    newAct.ReferenceNum = itemDS.Tables[0].Rows[0]["newRefNum"].ToString();
                }

                if (act.StatusID == statuses["Supplier Request"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 1))
                {
                    newAct.ActivityTypeID = actTypes["Supplier Request"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 1);
                }
                if (act.StatusID == statuses["Supplier Offer"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 2))
                {
                    newAct.ActivityTypeID = actTypes["Supplier Offer"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 2);
                }

                if (act.StatusID == statuses["Client Offer"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 3))
                {
                    newAct.ActivityTypeID = actTypes["Client Offer"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 3);
                }
                if (act.StatusID == statuses["Client Order"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 4))
                {
                    newAct.ActivityTypeID = actTypes["Client Order"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 4);
                }
                if (act.StatusID == statuses["Supplier Order"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 5))
                {
                    newAct.ActivityTypeID = actTypes["Supplier Order"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 5);
                }

                if (act.StatusID == statuses["Order Acknowledgement"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 6))
                {
                    newAct.ActivityTypeID = actTypes["Order Acknowledgement"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 6);
                }
                if (act.StatusID == statuses["Notice of Readiness"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 7))
                {
                    newAct.ActivityTypeID = actTypes["Notice of Readiness"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 7);
                }
                if (act.StatusID == statuses["Transportation Cost"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 8))
                {
                    newAct.ActivityTypeID = actTypes["Transportation Cost"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 8);
                }
                if (act.StatusID == statuses["Instructions"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 9))
                {
                    newAct.ActivityTypeID = actTypes["Instructions"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 9);
                }
                if (act.StatusID == statuses["Delivery Details"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 10))
                {
                    newAct.ActivityTypeID = actTypes["Delivery Details"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 10);
                }
                if (act.StatusID == statuses["Operation Closure"] && (act.GetDynamicValue("asStep") == null || Convert.ToInt16(act.GetDynamicValue("asStep")) < 11))
                {
                    newAct.ActivityTypeID = actTypes["Operation Closure"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                    act.SetDynamicValue("asStep", 11);
                }
                if (act.StatusID == statuses["Unable"])
                {
                    newAct.ActivityTypeID = actTypes["Unable"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                }
                if (act.StatusID == statuses["Partial Delivery"])
                {
                    newAct.ActivityTypeID = actTypes["Partial Delivery"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                }
                if (act.StatusID == statuses["Defect"])
                {
                    newAct.ActivityTypeID = actTypes["Defect"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                }
                if (act.StatusID == statuses["No Answer"])
                {
                    newAct.ActivityTypeID = actTypes["No Answer"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                }
                if (act.StatusID == statuses["Defect Operation Closure"])
                {
                    newAct.ActivityTypeID = actTypes["Defect Operation Closure"];
                    newAct.Subject = newAct.Subject;
                    cmActivityResourcesDataObject newAr = newAct.Resources.New();
                    newAr.ID = newAr.NewGuid();
                    newAr.ActivityID = newAct.ID;
                    newAr.Start = newAct.Start;
                    newAr.Finish = newAct.Finish;
                    newAr.Generated = 1;
                    newAr.ResourceID = curResource;
                    col.Add(newAct);
                    newAct.Resources.Add(newAr);
                }
                try
                {
                    foreach (var item in siCol)
                    {
                        cmSaleItemsDataObject line = item as cmSaleItemsDataObject;
                        cmSaleItemsDataObject newLine = siCol.New();
                        newLine.ID = newLine.NewGuid();
                        newLine.ActivityID = newAct.ID;
                        newLine.LineNum = line.LineNum;
                        newLine.ItemID = line.ItemID;
                        newLine.ItemDescription = line.ItemDescription;
                        newLine.ItemPrice = line.ItemPrice;
                        newLine.SystemDiscounts = line.SystemDiscounts;
                        newLine.PriceExt = line.PriceExt;
                        newLine.UserPrice = line.UserPrice;
                        newLine.MeasUnit = line.MeasUnit;
                        newLine.FloatField1 = line.FloatField1;
                        newLine.FloatField2 = line.FloatField2;
                        newLine.FloatField3 = line.FloatField3;
                        newAct.SaleItems.Add(newLine);
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        private static DataSet GetNewRefNum(SLnet.Base.Interfaces.IslAppContext appContext)
        {
            string sql = string.Format(" select right('00000000'+convert(nvarchar(10),max(CMREFERENCENUM)+1),9) as newRefNum from CMACTIVITIES ");
            slQueryParameters prms = new slQueryParameters();
            DataSet itemDS = null;
            IslDataAccessDbProvider opa = appContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
            itemDS = opa.ExecuteResultSet(sql, new slQueryParameters(), new string[] { "Data" });
            return itemDS;
        }

        private void UpdateParentActivity(cmActivitiesCollection col, IslAppContext appContext, string updFieldsType)
        {
            cmMessageLogger retVal;
            IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();

            cmActivitiesDataObject act = col[0];

            cmActivitiesCollection parCol = GetParentActivity(appContext, act);
            cmActivitiesDataObject parAct = parCol[0];
            cmSaleItemsCollection siCol = act.SaleItems;
            cmSaleItemsCollection parSiCol = parAct.SaleItems;

            // Header Fields
            parAct.StringField1 = act.StringField1;
            parAct.StringField2 = act.StringField2;
            parAct.StringField3 = act.StringField3;
            parAct.StringField4 = act.StringField4;
            parAct.StringField5 = act.StringField5;
            parAct.StringField6 = act.StringField6;
            parAct.StringField7 = act.StringField7;
            parAct.FloatField1 = act.FloatField1;
            parAct.FloatField2 = act.FloatField2;
            parAct.FloatField3 = act.FloatField3;
            parAct.FloatField4 = act.FloatField4;
            parAct.FloatField5 = act.FloatField5;
            parAct.FloatField6 = act.FloatField6;
            parAct.FloatField7 = act.FloatField7;
            parAct.DateField1 = act.DateField1;
            parAct.DateField2 = act.DateField2;
            parAct.DateField3 = act.DateField3;
            parAct.DateField4 = act.DateField4;
            parAct.SetDynamicValue("asAttention", act.GetDynamicValue("asAttention"));
            parAct.SetDynamicValue("asClientRefNum", act.GetDynamicValue("asClientRefNum"));
            parAct.SetDynamicValue("asCurrency", act.GetDynamicValue("asCurrency"));
            parAct.SetDynamicValue("asDeliveryDate", act.GetDynamicValue("asDeliveryDate"));
            parAct.SetDynamicValue("asDeliveryTerms", act.GetDynamicValue("asDeliveryTerms"));
            parAct.SetDynamicValue("asDeliveryTime", act.GetDynamicValue("asDeliveryTime"));
            parAct.SetDynamicValue("asOrigin", act.GetDynamicValue("asOrigin"));
            parAct.SetDynamicValue("asShipmentmarks", act.GetDynamicValue("asShipmentmarks"));
            parAct.SetDynamicValue("asSubjectDetails", act.GetDynamicValue("asSubjectDetails"));
            parAct.SetDynamicValue("asOurRefNum", act.GetDynamicValue("asOurRefNum"));
            parAct.SetDynamicValue("asSupplierRefNum", act.GetDynamicValue("asSupplierRefNum"));
            parAct.SetDynamicValue("asTransported", act.GetDynamicValue("asTransported"));

            

            //Line Fields
            if (updFieldsType == "Purchases")
            {

                foreach (cmSaleItemsDataObject parSiObj in parSiCol)
                {
                    foreach (cmSaleItemsDataObject siObj in siCol)
                    {
                        if (parSiObj.LineNum == siObj.LineNum)
                        {
                            parSiObj.SetDynamicValue("asPurchasePrice", siObj.GetDynamicValue("asPurchasePrice"));
                        }
                    }
                }
            }
            if (updFieldsType == "Sales")
            {
                foreach (cmSaleItemsDataObject parSiObj in parSiCol)
                {
                    foreach (cmSaleItemsDataObject siObj in siCol)
                    {
                        if (parSiObj.LineNum == siObj.LineNum)
                        {
                            parSiObj.SetDynamicValue("asMargin", siObj.GetDynamicValue("asMargin"));
                            parSiObj.SetDynamicValue("asCharges", siObj.GetDynamicValue("asCharges"));
                            parSiObj.ItemPrice = siObj.ItemPrice;
                            parSiObj.TotalAmount = siObj.TotalAmount;
                        }
                    }
                }
            }
            //parAct.Subject = parAct.Subject + " - Transed";
            using (var obj = oa.CreateObject(cmSys.GetRegName(cmObjRegNameCRM.Activities)))
            {
                try
                {
                    retVal = (cmMessageLogger)obj.ExecuteOperation("Post", parCol.DataContext);
                }
                catch (Exception ex)
                {
                    retVal = cmSys.HandleException(ex, string.Empty);
                }
            }

        }

        private static cmActivitiesCollection GetParentActivity(SLnet.Base.Interfaces.IslAppContext appContext, cmActivitiesDataObject act)
        {
            IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();
            using (var obj = oa.CreateObject(cmSys.GetRegName(cmObjRegNameCRM.Activities)))
            {
                cmActivitiesDataContext dc = (cmActivitiesDataContext)obj.ExecuteOperation("GetByID", act.OriginActivityID);
                return cmActivitiesCollectionFactory.Create(appContext, dc);
            }


        }

        private Boolean CheckForPartialDelivery(IslAppContext appContext, cmActivitiesDataObject acObj)
        {
            Boolean hasPartialDelivery = false;
            string sql = string.Format(" select a.CMID from CMACTIVITIES a " +
                                       " inner join CMACTIVITYTYPE at on a.CMACTIVITYTYPEID=at.CMID and at.cmCode='027' " +
                                       " where a.CMORIGINACTIVITYID=@actID ");
            slQueryParameters prms = new slQueryParameters();
            prms.Add("@actID", acObj.ID.ToString());
            IslDataAccessDbProvider opa =  appContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
            if (opa.ExecuteScalar(sql, prms)!=null) hasPartialDelivery = true;
            return hasPartialDelivery;
        }

        private string GetRONumber(IslAppContext appContext, string companyID, string prefix)
        {
            string sql = string.Format(" select max(case when ISNUMERIC(substring(ASOURREFNUM,7,2))=1 " +
                                       " then convert(integer,substring(ASOURREFNUM,7,2)) end) " +
                                       " from CMACTIVITIES " +
                                       " where ASOURREFNUM is not null  " +
                                       " and @prefix+substring(convert(nvarchar(10),@workDate,103),1,2)+ " +
                                       " substring(convert(nvarchar(10),@workDate,103),4,2)+ " +
                                       " substring(convert(nvarchar(10),@workDate,103),9,2) " +
                                       " = " +
                                       " substring(ASOURREFNUM,1,2)+ " +
                                       " substring(ASOURREFNUM,3,2)+ " +
                                       " substring(ASOURREFNUM,5,2)+ " +
                                       " substring(ASOURREFNUM,9,2)  " +
                                       " and CMCOMPANYID=@companyID ");

            slQueryParameters prms = new slQueryParameters();
            string newRONumber = "";
            int lastNumber = 0;
            object ronObj;
            DateTime workDate = (DateTime)cmSys.GetUserProfileInfo(appContext).WorkDate;
            IslDataAccessDbProvider opa =  appContext.ServiceLocator.GetService<IslDataAccessDbProvider>();
            if (companyID != null && prefix != null)
            {
                prms.Add("@companyID", companyID);
                prms.Add("@prefix", prefix);
                prms.Add("@workDate", workDate);
                ronObj = opa.ExecuteScalar(sql, prms) ?? "";
                int.TryParse(ronObj.ToString(), out lastNumber);

                string dts = workDate.ToString("ddMMyyyy");
                if (lastNumber == 0)
                {
                    newRONumber = prefix + dts.Substring(0, 4) + "01" + dts.Substring(6, 2);
                }
                else
                {
                    newRONumber = "0" + Convert.ToString(lastNumber + 1);
                    newRONumber = prefix + dts.Substring(0, 4) + newRONumber.Substring(newRONumber.Length - 2) + dts.Substring(6, 2);
                }
            }

            return newRONumber;
        }

        public void BeforeExecuteOperation(SLnet.Base.Interfaces.IslAppContext appContext, SLnet.Base.Interfaces.IslOperations container, SLnet.Base.slBaseOperation operation, params object[] args)
        {


        }


    }
}
