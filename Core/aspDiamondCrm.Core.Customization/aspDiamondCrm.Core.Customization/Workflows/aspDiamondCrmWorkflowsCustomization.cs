﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SLnet.Base.Customization;
using SLnet.Base.Customization.Interfaces;
using SLnet.Sand.Base.Customization.Interfaces;
using SLnet.Base.Interfaces;
using SLnet.Sand.Base.Customization.Workflows;
using aspDiamondCrm.Core.Base;

namespace aspDiamondCrm.Core.Customization.Workflows
{

    [slRegisterCustomDomainAppExtension()]
    public class aspDiamondCrmWorkflowsCustomization : IslCustomDomainAppExtension, IslsCustomDomainAppWorkflowRegExtension
    {

        private slsWorkflowsCustomizationManager wfManager;

        private void RegisterWorkflow(string alias, string description, string action)
        {
            wfManager.RegisterWorkflow(alias, description + "." + action);
        }

        public void RegisterWorkflows(IslAppContext appContext, slsWorkflowsCustomizationManager workflowManager)
        {
            wfManager = workflowManager;

        }

    }

}
