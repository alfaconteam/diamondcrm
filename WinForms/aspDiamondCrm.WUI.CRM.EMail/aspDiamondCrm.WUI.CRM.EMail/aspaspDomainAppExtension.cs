﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using SLnet.Base.Customization.Interfaces;
using SLnet.Sand.Base.Customization.Interfaces;
using SLnet.Base.Interfaces;
using SLnet.Sand.WinForms.Forms;
using SLnet.Base.Customization;
using SLnet.Sand.Base.Interfaces;
using Crm.WUI.CRM.EMail;
using DevExpress.XtraLayout;
using Crm.Core.WinControls.AdvControls;
using SLnet.Base.DataObjects;
using SLnet.Sand.Base.Forms;
using Crm.Data.DataObjects;
using Crm.Core.Base;
using Crm.Data.Structure;
using Crm.Core.Base.Structures;
using DevExpress.XtraRichEdit;
using System.Data;
using SLnet.Base.DbUtils;
using SLnet.Base.DbUtils.Interfaces;
using Glx.Data.DataObjects;
using Glx.Core.Base;
using Glx.Data.Structure;
using Crm.Core.WinControls.DevExp;
using aspDiamondCrm.Core.Base;

namespace aspDiamondCrm.WUI.CRM.EMail
{

    [slRegisterCustomDomainAppExtension()]
    public class aspaspDomainAppExtension : IslCustomDomainAppExtension, IslsCustomDomainAppFormExtension
    {
        public static Dictionary<string, string> statuses = new Dictionary<string, string>();
        public static Dictionary<string, string> actTypes = new Dictionary<string, string>();


        public aspaspDomainAppExtension()
        {
        }


        public void Create(IslAppContext appContext, IslsForm form)
        {
            switch (form.GetType().ToString())
            {
                case "Crm.WUI.CRM.EMail.cmNewEmailMessageF":
                    cmNewEmailMessageF emCol = form as cmNewEmailMessageF;
                    Dictionary<string, object> naDic = emCol.InArgs.NamedArgs;
                    if (!emCol.InArgs.NamedArgs.ContainsKey("AddDocumentAttachments"))
                    {
                        if (emCol.InArgs.NamedArgs.ContainsKey("EmailFromActivityParams"))
                        {
                            cmEmailFromActivityParams ap = (cmEmailFromActivityParams)naDic["EmailFromActivityParams"];
                            emCol.InArgs.NamedArgs.Add("AddDocumentAttachments", true);
                            emCol.InArgs.NamedArgs.Add("ActivityID", ap.ActivityID.ToString());
                        }
                    }
                   // emCol.PreviewPropertyChanged += new EventHandler<SLnet.Base.Utils.slEventArgs<slPreviewPropChangedArgs>>(cCol_PreviewPropertyChanged);
                    //emCol.Click+= new EventHandler<SLnet.Base.Utils.slEventArgs<slsFormArgs>>(emCol_Click);
                    //emCol.MouseClick = new MouseEventHandler<SLnet.Base.Utils.slEventArgs<slsFormArgs>>(emCol_Click);
                    break;
                default:
                    break;
            }

        }



        public void ExecuteLink(IslAppContext appContext, IslsForm form, string command)
        {
            switch (form.GetType().ToString())
            {
                case "Crm.WUI.CRM.EMail.cmEmailMessageViewerF":
                    if (command == "aspUnassignAllActivities")
                    {
                        slsFormArgs args = new slsFormArgs();

                        Dictionary<string, BindingSource> bSources = slsBaseF.GetDataSources(form);
                        cmActivitiesCollection aCol = new cmActivitiesCollection();
                        aCol = ((bSources["bsActivities"] as Crm.Core.WinControls.Data.cmBindingSource).DataSource
                            as slBindingCollectionView<cmActivitiesDataObject>).Collection as cmActivitiesCollection;
                        cmActivitiesDataObject eMail = aCol[0];
                    }
                    break;

                        break;
                default:
                    break;
            }
        }

        public Dictionary<string, string> GetLinks(IslAppContext appContext, IslsForm form)
        {

            Dictionary<string, string> dc = null;
            switch (form.GetType().ToString())
            {
                case "Crm.WUI.CRM.EMail.cmEmailMessageViewerF":
                    dc = new Dictionary<string, string>();
                    dc.Add("aspUnassignAllActivities", "Unassign All Activities");
                    break;
                case "Crm.WUI.CRM.EMail.cmUnassignFromActivityF":
                    dc = new Dictionary<string, string>();
                    dc.Add("aspUnassignAllActivities", "Unassign All Activities");
                    break;
                default:
                    break;
            }

            return dc;
        }

        public void Init(IslAppContext appContext, IslsForm form)
        {

        }
        /// <summary>
        /// fernei to object apo to klidi tou  
        /// </summary>
        /// <param name="appContext"></param>
        /// <param name="actID"></param>
        /// <returns></returns> 
        cmActivitiesCollection GetActivity(IslAppContext appContext, string actID)
        {

            IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();

            using (var obj = oa.CreateObject(cmSys.GetRegName(cmObjRegNameCRM.Activities)))
            {

                cmActivitiesCollection dc = (cmActivitiesCollection)obj.ExecuteOperation("GetByID", actID);
                return dc;
            }
        }

        cmActivityTypeDataObject GetActivityType(IslAppContext appContext, string actTypeID)
        {

            IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();

            using (var obj = oa.CreateObject(cmSys.GetRegName(cmObjRegNameCRM.ActivityType)))
            {

                cmActivityTypeCollection dc = (cmActivityTypeCollection)obj.ExecuteOperation("GetByID", actTypeID);
                return dc[0];
            }
        }

        cmStatusesDataObject GetActivityStatus(IslAppContext appContext, string StatusID)
        {

            IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();

            using (var obj = oa.CreateObject(cmSys.GetRegName(cmObjRegNameCRM.Statuses)))
            {
                cmStatusesCollection dc = (cmStatusesCollection)obj.ExecuteOperation("GetByID", StatusID);
                return dc[0];
            }
        }

        /// <summary>
        /// trexei otan anigi to email kai theleis na kaneis pragmata sto front 
        /// </summary>
        /// <param name="appContext"></param>
        /// <param name="form"></param>
        public void SyncScreen(IslAppContext appContext, IslsForm form)
        {
            cmActivitiesCollection acCol;
            IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();
                switch (form.GetType().ToString())
                {
                    case "Crm.WUI.CRM.EMail.cmNewEmailMessageF":
                            cmNewEmailMessageF emCol = form as cmNewEmailMessageF;
                            Dictionary<string, object> naDic = emCol.InArgs.NamedArgs;


                            if (naDic.ContainsKey("EmailFromActivityParams") || naDic.ContainsKey("ActivityID") || naDic.ContainsKey("Data"))
                            {

                                if (naDic.ContainsKey("EmailFromActivityParams"))
                                {
                                    cmEmailFromActivityParams ap = (cmEmailFromActivityParams)naDic["EmailFromActivityParams"];
                                    acCol = GetActivity(appContext, ap.ActivityID);
                                }
                                else if (naDic.ContainsKey("Data"))
                                {
                                    try
                                    {
                                        DataSet emailDS = (DataSet)naDic["Data"];
                                        var originalEmailID = emailDS.Tables[0].Rows[0].ItemArray[0];
                                        object activityID;
                                        using (var obj = oa.CreateObject(aspDiamondCrmSys.GetRegName("aspDiamondCrmBusinessObjects")))
                                        {
                                            activityID = obj.ExecuteOperation("GetEmailActivity", originalEmailID.ToString()) ?? "";
                                        }
                                        if (activityID.ToString() != "")
                                        {
                                            acCol = GetActivity(appContext, activityID.ToString());
                                        }
                                        else { break; }
                                    }
                                    catch (Exception e)
                                    {
                                        break;
                                    }

                                }
                                else
                                {
                                    acCol = GetActivity(appContext, naDic["ActivityID"].ToString());
                                }
                                cmActivitiesDataObject acObj = acCol[0] as cmActivitiesDataObject;

                                var loMessage = (form as cmNewEmailMessageF).MainLayout.Items.FindByName("loMessage");
                                //(loMessage as LayoutControlItem).Control.Text = "Θέλω να γράψω εδώ!!!";
                                RichEditControl rEC = (RichEditControl)(loMessage as LayoutControlItem).Control;
                                var loSubject = (form as cmNewEmailMessageF).MainLayout.Items.FindByName("loSubject");
                                cmTextEdit tE = (cmTextEdit)(loSubject as LayoutControlItem).Control;
                                var loBccAuto = (form as cmNewEmailMessageF).MainLayout.Items.FindByName("loBccAuto");
                                var loSendAs = (form as cmNewEmailMessageF).MainLayout.Items.FindByName("loSendAs");
                                cmComboBoxEdit comboSendEdit = (cmComboBoxEdit)(loSendAs as LayoutControlItem).Control;
                                cmPopupContainerEdit cBccEdit = (cmPopupContainerEdit)(loBccAuto as LayoutControlItem).Control;
                                //var fromText = From ?? "";
                                cBccEdit.Text = comboSendEdit.Text ?? "";
                                FillMessage(appContext, rEC, acObj);
                                FillSubject(appContext, acObj, tE);
                            }
                            break;
                    default:
                        break;
                }

        }
        private Int32 YearParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 4)) - 5924;
        }
        private Int32 MonthParam(string data)
        {
            return Convert.ToInt16(data.Substring(4, 1)) - 1;
        }
        private Int32 DayParam(string data)
        {
            return Convert.ToInt16(data.Substring(20, 2)) + 11;
        }
        private void FillSubject(IslAppContext appContext,cmActivitiesDataObject acObj, cmTextEdit tE)
        {


            if (1==1)//(DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
            {

                var asShip = acObj.GetDynamicValue("asShip") ?? "";
                string ship = "";
                if (asShip != "") { ship = GetMediatorName(appContext, asShip.ToString()); }
                var asShipOwner = acObj.GetDynamicValue("asShipOwner") ?? "";
                string shipOwner = "";
                if (asShipOwner != "") { shipOwner = GetMediatorName(appContext, asShipOwner.ToString()); }
                var asClientRefNum = acObj.GetDynamicValue("asClientRefNum") ?? "";
                var asClientOrderNumber = acObj.StringField2 ?? "";
                var asOurRefNum = acObj.GetDynamicValue("asOurRefNum") ?? "";
                var asSupplierRefNum = acObj.GetDynamicValue("asSupplierRefNum") ?? "";
                var asOurOrderNum = acObj.StringField1 ?? "";
                var asSupplierOrderNum = acObj.StringField3 ?? "";

                switch (acObj.StatusID)
                {
                    case "f2f09f3b-c35b-40bd-8544-c640d302acd8": //"Supplier Request":
                        tE.Text = "Request / " + shipOwner + " / " + ship + " / " + asClientRefNum.ToString();
                        break;
                    case "85a9a0b7-d42a-4193-9d08-b280d085b2c2": //Client Offer":
                        tE.Text = "Offer / " + shipOwner + " / " + ship + " / " + asClientRefNum.ToString() + " / " + asOurRefNum.ToString();
                        break;
                    case "e4819175-dedf-4d9d-8c44-5cc42046c576": //Supplier Order":
                        tE.Text = "Order / " + shipOwner + " / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurRefNum.ToString();
                        break;
                    case "2a65f7f5-fbf5-4ed1-a806-12713ac63e14": //Order Acknowledgement":
                        tE.Text = "Order Acknowledgement / " + shipOwner + " / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurRefNum.ToString();
                        break;
                    case "f3f1148a-81b5-4cee-9ecc-f54c5632ef56": //Notice of Readiness":
                        tE.Text = "Notice of Readiness / " + shipOwner + " / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurRefNum.ToString();
                        break;
                    case "de753966-5185-4f1e-9aa4-eba319b2cb23": //Delivery Details":
                        tE.Text = "Delivery Details / " + shipOwner + " / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurRefNum.ToString();
                        break;
                    case "6764efd3-f13a-40ee-ab04-55a4e4974c78": //Unable":
                        tE.Text = "Unable / " + ship + " / " + asClientRefNum.ToString();  //new excel 12 !!!
                        break;
                    default:
                        break;
                }

                switch (acObj.ActivityTypeID) // for Mare
                {
                    case "b5f505f4-95f1-4f07-bae8-8e3bc2f4861f": //Supplier Request
                        if (acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")//DMI not simple
                            tE.Text = "Request / " + shipOwner + " / " + ship + " / " + asClientRefNum.ToString();
                        else
                            tE.Text = "Request / " + ship + " / " + asOurRefNum.ToString();
                        break;
                    case "99953d38-c77a-469a-a36e-59aa96734013": //Client Offer":
                        if (acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")//DMI not simple
                            tE.Text = "Offer / " + shipOwner + " / " + ship + " / " + asClientRefNum.ToString() + " / " + asOurRefNum.ToString();
                        else
                            tE.Text = "Offer / " + ship + " / " + asClientRefNum.ToString() + " / " + asOurRefNum.ToString();
                        break;
                    case "a6e68457-1228-4530-9022-dc4fbe09c641": //Supplier Order":
                        if (acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")//DMI not simple
                            tE.Text = "Order / " + shipOwner + " / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurRefNum.ToString();
                        else
                            tE.Text = "Order Confirmation / " + ship + " / " + asOurOrderNum.ToString() + " / " + asSupplierRefNum.ToString();
                        break;
                    case "81f27b85-83d3-490a-a6db-f59dbff26412": //Order Acknowledgement":
                        if (acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")//DMI not simple
                            tE.Text = "Order Acknowledgement / " + shipOwner + " / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurRefNum.ToString();
                        else
                            tE.Text = "Order Acknowledgement / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurOrderNum.ToString();
                        break;
                    case "cb1c2d3c-1090-4127-b441-9ea88f66aa60": //Notice of Readiness":
                        if (acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")//DMI not simple
                            tE.Text = "Notice of Readiness / " + shipOwner + " / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurRefNum.ToString();
                        else
                            tE.Text = "Notice of Readiness / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurOrderNum.ToString();
                        break;
                    case "f3f6223e-858c-4b87-b6d3-034783b6f9ab": //Delivery Details":
                        if (acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")//DMI not simple
                            tE.Text = "Delivery Details / " + shipOwner + " / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurRefNum.ToString();
                        else
                            tE.Text = "Delivery Details / " + ship + " / " + asClientOrderNumber.ToString() + " / " + asOurOrderNum.ToString();
                        break;
                    case "0bfcaaeb-af36-45eb-b152-f6b271d03b2a": //Instructions
                        tE.Text = "Instructions / " + ship + " / " + asOurOrderNum.ToString() + " / " + asSupplierOrderNum.ToString();
                        break;
                    case "b248a59b-0e97-4968-b446-df7291ca2f27": //Unable":
                        if (acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")//DMI not simple
                            tE.Text = "Unable / " + ship + " / " + asClientRefNum.ToString();
                        else
                            tE.Text = "Unable / " + ship + " / " + asClientRefNum.ToString();
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// kalite apo to sync screen gia na grapsis mesa sta pedia pragmata
        /// </summary>
        /// <param name="rEC"></param>
        /// <param name="acObj"></param>
        private void FillMessage(IslAppContext appContext, RichEditControl rEC, cmActivitiesDataObject acObj)
        {
            DateTime startDate;
            FillDirectories();


            //cmActivityTypeDataObject acTObj = GetActivityType(appContext, acObj.ActivityTypeID);
            //cmStatusesDataObject acSObj = GetActivityStatus(appContext, acObj.StatusID);
            
            //-------------Choose Form-------------------
            RichEditControl temp = new RichEditControl();
            temp.WordMLText = rEC.WordMLText;

            if (1==1)//(DateTime.Now < new DateTime(YearParam("743d7942-4ccb-474b-b140-06011f6795cc"), MonthParam("743d7942-4ccb-474b-b140-06011f6795cc"), DayParam("743d7942-4ccb-474b-b140-06011f6795cc")))
            {

                switch (acObj.CompanyID)
                {
                    case "8e50fec4-a933-444d-b33f-c762685f2cab":
                        rEC.LoadDocument("rtf\\Schelde.rtf", DocumentFormat.Rtf);
                        break;
                    case "8d934e28-1858-4ebd-95ed-6558feee8511":
                        rEC.LoadDocument("rtf\\Mare.rtf", DocumentFormat.Rtf);
                        break;
                    case "743d7942-4ccb-474b-b140-06011f6795cc":
                        rEC.LoadDocument("rtf\\Diamond.rtf", DocumentFormat.Rtf);
                        break;
                    case "9c7290b4-7333-4f67-accc-295ddd32a574":
                        rEC.LoadDocument("rtf\\DMI.rtf", DocumentFormat.Rtf);
                        break;
                    case "b5a18dc0-ac35-4637-a79a-961194e7425b":
                        rEC.LoadDocument("rtf\\Bosung.rtf", DocumentFormat.Rtf);
                        break;
                    case "e951f100-d995-451d-9278-e9cf656ae5f9":
                        rEC.LoadDocument("rtf\\Admos.rtf", DocumentFormat.Rtf);
                        break;
                    default:
                        rEC.LoadDocument("rtf\\Schelde.rtf", DocumentFormat.Rtf);
                        break;
                }

                //---------------Replace Params----------------------

                var asShip = acObj.GetDynamicValue("asShip") ?? "";

                if (asShip != "") { rEC.WordMLText = rEC.WordMLText.Replace("@ship@", GetMediatorName(appContext, asShip.ToString())); }
                else { rEC.WordMLText = rEC.WordMLText.Replace("@ship@", ""); }


                var asShipOwner = acObj.GetDynamicValue("asShipOwner") ?? "";
                string shipOwner = "";
                if (asShipOwner != "") { shipOwner = GetMediatorName(appContext, asShipOwner.ToString()); }
                if (acObj.CompanyID == "8d934e28-1858-4ebd-95ed-6558feee8511")
                    shipOwner = acObj.AccountName;

                if (acObj.StatusID == statuses["Supplier Request"]
                    || acObj.StatusID == statuses["Supplier Order"]
                    || acObj.StatusID == statuses["Instructions"])
                {
                    rEC.WordMLText = rEC.WordMLText.Replace("@shipOwner@", acObj.AccountName.Replace("&", "&amp;"));
                }
                else if (acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574")
                {
                    if (acObj.ActivityTypeID == actTypes["Supplier Request"]
                    || acObj.ActivityTypeID == actTypes["Supplier Order"]
                    || acObj.ActivityTypeID == actTypes["Instructions"])
                    {
                        rEC.WordMLText = rEC.WordMLText.Replace("@shipOwner@", acObj.AccountName.Replace("&", "&amp;"));
                    }
                    else
                    {
                        if (shipOwner != "")
                        {
                            shipOwner = shipOwner.Replace("&", "&amp;");
                            rEC.WordMLText = rEC.WordMLText.Replace("@shipOwner@", shipOwner);
                        }
                        else { rEC.WordMLText = rEC.WordMLText.Replace("@shipOwner@", ""); }
                    }
                }
                else
                {
                    if (shipOwner != "")
                    {
                        shipOwner = shipOwner.Replace("&", "&amp;");
                        rEC.WordMLText = rEC.WordMLText.Replace("@shipOwner@", shipOwner);
                    }
                    else { rEC.WordMLText = rEC.WordMLText.Replace("@shipOwner@", ""); }

                }


                var asAttention = acObj.GetDynamicValue("asAttention") ?? "";
                asAttention = asAttention.ToString().Replace("&", "&amp;");
                rEC.WordMLText = rEC.WordMLText.Replace("@attn@", asAttention.ToString());

                startDate = DateTime.Now;
                rEC.WordMLText = rEC.WordMLText.Replace("@actStart@", startDate.ToString("dd/MM/yyyy"));

                var asClientRefNum = acObj.GetDynamicValue("asClientRefNum") ?? "";
                asClientRefNum = asClientRefNum.ToString().Replace("&", "&amp;");
                rEC.WordMLText = rEC.WordMLText.Replace("@asClientRefNum@", asClientRefNum.ToString());

                var asOurRefNum = acObj.GetDynamicValue("asOurRefNum") ?? "";
                asOurRefNum = asOurRefNum.ToString().Replace("&", "&amp;");
                rEC.WordMLText = rEC.WordMLText.Replace("@asOurRefNum@", asOurRefNum.ToString());

                var asSupplierRefNum = acObj.GetDynamicValue("asSupplierRefNum") ?? "";
                asSupplierRefNum = asSupplierRefNum.ToString().Replace("&", "&amp;");
                rEC.WordMLText = rEC.WordMLText.Replace("@asSupplierRefNum@", asSupplierRefNum.ToString());




                if (Convert.ToInt16(acObj.GetDynamicValue("asStep") ?? 0) >= 4 || acObj.CompanyID == "8d934e28-1858-4ebd-95ed-6558feee8511"
                    // || acObj.CompanyID == "9c7290b4-7333-4f67-accc-295ddd32a574"
                    || acObj.ActivityTypeID == actTypes["Supplier Order"] || acObj.ActivityTypeID == actTypes["Order Acknowledgement"]
                    || acObj.ActivityTypeID == actTypes["Instructions"] || acObj.ActivityTypeID == actTypes["Delivery Details"]
                    || acObj.ActivityTypeID == actTypes["Partial Delivery"] || acObj.ActivityTypeID == actTypes["Transportation Cost"]
                    || acObj.StatusID == statuses["Supplier Order"] || acObj.StatusID == statuses["Order Acknowledgement"]
                    || acObj.StatusID == statuses["Instructions"] || acObj.StatusID == statuses["Delivery Details"]
                    || acObj.StatusID == statuses["Partial Delivery"] || acObj.StatusID == statuses["Transportation Cost"])
                {
                    var stringField1 = acObj.StringField1 ?? "";
                    stringField1 = stringField1.Replace("&", "&amp;");
                    rEC.WordMLText = rEC.WordMLText.Replace("@ourOrderNumber@", stringField1.ToString());
                    var stringField3 = acObj.StringField3 ?? "";
                    stringField3 = stringField3.Replace("&", "&amp;");
                    rEC.WordMLText = rEC.WordMLText.Replace("@supplierOrderNumber@", stringField3.ToString());
                    var stringField2 = acObj.StringField2 ?? "";
                    stringField2 = stringField2.Replace("&", "&amp;");
                    rEC.WordMLText = rEC.WordMLText.Replace("@clientOrderNumber@", stringField2.ToString());
                    rEC.WordMLText = rEC.WordMLText.Replace("@:", ":");
                }
                else
                {
                    rEC.WordMLText = rEC.WordMLText.Replace("@ourOrderNumber@", "");
                    rEC.WordMLText = rEC.WordMLText.Replace("@supplierOrderNumber@", "");
                    rEC.WordMLText = rEC.WordMLText.Replace("@clientOrderNumber@", "");
                    rEC.WordMLText = rEC.WordMLText.Replace("Our O/No", "");
                    rEC.WordMLText = rEC.WordMLText.Replace("Client O/No", "");
                    rEC.WordMLText = rEC.WordMLText.Replace("Supp. O/No", "");
                    rEC.WordMLText = rEC.WordMLText.Replace("@:", "");
                }





                rEC.WordMLText = rEC.WordMLText.Replace("@userName@", cmSys.GetUserProfileInfo(appContext).EmployeeName);
                if (cmSys.GetUserProfileInfo(appContext).EmployeeName == "Kyriakos Mytilineos")
                {
                    rEC.WordMLText = rEC.WordMLText.Replace("Chief Operator", "Sales Engineer");
                }

                switch (acObj.StatusID)
                {
                    case "f2f09f3b-c35b-40bd-8544-c640d302acd8": //"Supplier Request":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Kindly find attached our customer's request and revert soonest possible with your offer.");
                        break;
                    case "85a9a0b7-d42a-4193-9d08-b280d085b2c2": //Client Offer":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Further to your above requisition, kindly find attached our offer.");
                        break;
                    case "e4819175-dedf-4d9d-8c44-5cc42046c576": //Supplier Order":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "With regard to your subject offer, kindly find attached customer's order and proceed accordingly.");
                        break;
                    case "2a65f7f5-fbf5-4ed1-a806-12713ac63e14": //Order Acknowledgement":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Kindly find attached our order confirmation relating to your subject order.");
                        break;
                    case "f3f1148a-81b5-4cee-9ecc-f54c5632ef56": //Notice of Readiness":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Please note that spares of your order are ready for dispatch as per attached notice of readiness.");
                        break;
                    case "de753966-5185-4f1e-9aa4-eba319b2cb23": //Delivery Details":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Please find attached shipping details of your order.");
                        break;
                    case "6764efd3-f13a-40ee-ab04-55a4e4974c78": //Unable":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Regret to inform you that we are unable to offer for your above mentioned inquiry. Looking forward to serve you better on a future occasion.");
                        break;
                    default:
                        break;
                }
                switch (acObj.ActivityTypeID)
                {
                    case "b5f505f4-95f1-4f07-bae8-8e3bc2f4861f": //Supplier Request
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Kindly find attached our customer's request and revert soonest possible with your offer.");
                        rEC.WordMLText = rEC.WordMLText.Replace("@ourOrderNumber@", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("@supplierOrderNumber@", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("@clientOrderNumber@", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("Our O/No", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("Client O/No", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("Supp. O/No", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("@:", "");
                        break;
                    case "99953d38-c77a-469a-a36e-59aa96734013": //Client Offer":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Further to your above requisition, kindly find attached our offer.");
                        rEC.WordMLText = rEC.WordMLText.Replace("@ourOrderNumber@", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("@supplierOrderNumber@", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("@clientOrderNumber@", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("Our O/No", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("Client O/No", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("Supp. O/No", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("@:", "");
                        break;
                    case "a6e68457-1228-4530-9022-dc4fbe09c641": //Supplier Order":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "With regard to your subject offer, kindly find attached customer's order and proceed accordingly.");
                        rEC.WordMLText = rEC.WordMLText.Replace("@ourOrderNumber@", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("@supplierOrderNumber@", "");
                        rEC.WordMLText = rEC.WordMLText.Replace("@clientOrderNumber@", "");
                        if (acObj.CompanyID == "8d934e28-1858-4ebd-95ed-6558feee8511") // for Mare
                        {
                            rEC.WordMLText = rEC.WordMLText.Replace("Our O/No", "");
                            rEC.WordMLText = rEC.WordMLText.Replace("Client O/No", "");
                            rEC.WordMLText = rEC.WordMLText.Replace("Supp. O/No", "");
                            rEC.WordMLText = rEC.WordMLText.Replace("@:", "");
                        }
                        break;
                    case "81f27b85-83d3-490a-a6db-f59dbff26412": //Order Acknowledgement":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Kindly find attached our order confirmation relating to your subject order.");
                        break;
                    case "cb1c2d3c-1090-4127-b441-9ea88f66aa60": //Notice of Readiness":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Please note that spares of your order are ready for dispatch as per below notice of readiness.");
                        break;
                    case "f3f6223e-858c-4b87-b6d3-034783b6f9ab": //Delivery Details":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Please find attached shipping details of your order.");
                        break;
                    case "0bfcaaeb-af36-45eb-b152-f6b271d03b2a": //Instructions
                        break;
                    case "b248a59b-0e97-4968-b446-df7291ca2f27": //Unable":
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "Regret to inform you that we are unable to offer for your enclosed inquiry. Looking forward to serve you better on a future occasion.");
                        break;
                    default:
                        rEC.WordMLText = rEC.WordMLText.Replace("@emailMessage@", "");
                        break;
                }

                rEC.Document.AppendText(temp.Text);
            }
        }
        
        /// <summary>
        /// san to getActivity 
        /// </summary>
        /// <param name="appContext"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        string GetMediatorName(IslAppContext appContext, string ID)
        {

            IslObjectActivator oa = appContext.ServiceLocator.GetService<IslObjectActivator>();

            using (var obj = oa.CreateObject(gxSys.GetRegName(gxObjRegNameFI.Mediator)))
            {

                vxMediatorRoleInfoCollection dc = (vxMediatorRoleInfoCollection)obj.ExecuteOperation("GetMediatorRoleInfo", ID);
                return dc[0].Name;
            }
        }

        public void FillDirectories()
        {
            if (statuses.Count == 0)
            {

                statuses.Add("Σε εξέλιξη", "73d6c038-cf5c-4a3b-81d4-01bb8fe2615c");
                statuses.Add("Supplier Request", "f2f09f3b-c35b-40bd-8544-c640d302acd8");
                statuses.Add("Supplier Offer", "67b05d9c-40af-483f-a501-fd259dad636c");
                statuses.Add("Client Offer", "85a9a0b7-d42a-4193-9d08-b280d085b2c2");
                statuses.Add("Client Order", "b3a9b378-3d95-41b4-b7d3-acff122dd14f");
                statuses.Add("Supplier Order", "e4819175-dedf-4d9d-8c44-5cc42046c576");
                statuses.Add("Order Acknowledgement", "2a65f7f5-fbf5-4ed1-a806-12713ac63e14");
                statuses.Add("Notice of Readiness", "f3f1148a-81b5-4cee-9ecc-f54c5632ef56");
                statuses.Add("Transportation Cost", "64468d4d-d6e0-4375-9a42-0b56b2138164");
                statuses.Add("Instructions", "be7f1c8e-71fe-402a-a636-5f52fa747847");
                statuses.Add("Delivery Details", "de753966-5185-4f1e-9aa4-eba319b2cb23");
                statuses.Add("Operation Closure", "bee0abf1-86b4-4001-ac53-5fc3f504c834");
                statuses.Add("Unable", "6764efd3-f13a-40ee-ab04-55a4e4974c78");
                statuses.Add("Partial Delivery", "52426089-0c0b-4e1d-b8bf-0c59ba44b82a");
                statuses.Add("Defect", "c68d2900-1b09-4b64-bb90-278c74de3fd1");
                statuses.Add("Successful", "6870893a-e25d-4986-b35a-e974f63e7daa");
                statuses.Add("Unsuccessful", "ac8af5c7-84dd-472a-a790-37b29572d26b");
                statuses.Add("History", "afc74f00-93a8-44d8-a7a8-6ef52f009d0f");
            }
            if (actTypes.Count == 0)
            {
                actTypes.Add("Client Request", "6b3c7b88-25b3-4b7c-a942-5b25d0d555bf");
                actTypes.Add("Supplier Request", "b5f505f4-95f1-4f07-bae8-8e3bc2f4861f");
                actTypes.Add("Supplier Offer", "2cf5708d-7e7e-4267-9247-02002161639a");
                actTypes.Add("Client Offer", "99953d38-c77a-469a-a36e-59aa96734013");
                actTypes.Add("Client Order", "75c2f842-1204-439f-b24b-edfd4dcee2ff");
                actTypes.Add("Supplier Order", "a6e68457-1228-4530-9022-dc4fbe09c641");
                actTypes.Add("Order Acknowledgement", "81f27b85-83d3-490a-a6db-f59dbff26412");
                actTypes.Add("Notice of Readiness", "cb1c2d3c-1090-4127-b441-9ea88f66aa60");
                actTypes.Add("Transportation Cost", "c217ee74-b321-41a5-a292-d4da026d23f5");
                actTypes.Add("Instructions", "0bfcaaeb-af36-45eb-b152-f6b271d03b2a");
                actTypes.Add("Delivery Details", "f3f6223e-858c-4b87-b6d3-034783b6f9ab");
                actTypes.Add("Operation Closure", "564d5b33-ae25-44f5-be50-5846c75990da");
                actTypes.Add("Unable", "b248a59b-0e97-4968-b446-df7291ca2f27");
                actTypes.Add("Partial Delivery", "6236f073-dfaa-4e20-977b-112b379bf7a6");
                actTypes.Add("Defect", "63641218-70eb-417c-ad8f-662f16961a97");
                actTypes.Add("History", "a5e97db0-c862-458f-b3d3-ced904993b65");
            }
        }

    }

}
