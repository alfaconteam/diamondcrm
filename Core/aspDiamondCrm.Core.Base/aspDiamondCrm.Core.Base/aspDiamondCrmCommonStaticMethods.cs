﻿using System;
using SLnet.Base.Interfaces;

namespace aspDiamondCrm.Core.Base
{

    public class aspDiamondCrmSys
    {

        private aspDiamondCrmSys()
        {
        }

        /// <summary>
        /// Returns the registration name of a method ex.slgServices:Object.Method
        /// </summary>
        /// <param name="obj">Object name</param>
        /// <param name="method">Method name</param>
        /// <returns>
        /// The method registration name
        /// </returns>
        public static string GetRegName(string obj, string method)
        {
            return aspDiamondCrmObjRegName.DomainName + ":" + obj + "." + method;
        }

        /// <summary>
        /// Returns the registration name of an object ex. slgServices:Object
        /// </summary>
        /// <param name="obj">Object registration name</param>
        /// <returns>
        /// The object registration name
        /// </returns>
        public static string GetRegName(string obj)
        {
            return aspDiamondCrmObjRegName.DomainName + ":" + obj;
        }

        /// <summary>
        /// Returns a Browser Action name ex.slgServices:UI.XX.Object
        /// </summary>
        /// <param name="obj">Object name</param>
        /// <returns>
        /// The Browser Action registration name
        /// </returns>
        public static string GetBrowserAction(string obj)
        {
            return aspDiamondCrmObjRegName.DomainName + ":" + aspDiamondCrmObjRegName.AliasName + ":" + obj;
        }

        /// <summary>
        /// Executes the object's operation 
        /// </summary>
        /// <param name="appContext">AppContext</param>
        /// <param name="objName">Object name</param>
        /// <param name="operation">Operation name</param>
        /// <param name="args">Operation arguments</param>
        /// <returns>
        /// The execution result 
        /// </returns>
        public static object ExecuteOperation(IslAppContext appContext, string objName, string operation, params object[] args)
        {
            IslObjectActivator oActivator = appContext.ServiceLocator.GetService<IslObjectActivator>();
            using (var obj = oActivator.CreateObject(objName))
            {
                return obj.ExecuteOperation(operation, args);
            }
        }

        /// <summary>
        /// Executes the object's server operation 
        /// </summary>
        /// <param name="appContext">AppContext</param>
        /// <param name="objName">Object name</param>
        /// <param name="operation">Operation name</param>
        /// <param name="args">Operation arguments</param>
        /// <returns>
        /// The execution result 
        /// </returns>
        public static object ExecuteServerOperation(IslAppContext appContext, string objName, string operation, params object[] args)
        {
            IslObjectProxyActivator ProxyActivator = appContext.ServiceLocator.GetService<IslObjectProxyActivator>();
            using (var obj = ProxyActivator.GetObjectProxy<IslOperations>(objName, false))
            {
                return obj.ExecuteOperation(operation, args);
            }
        }

    }

}
